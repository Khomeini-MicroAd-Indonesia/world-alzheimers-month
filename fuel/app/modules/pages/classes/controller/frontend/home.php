<?php
namespace Pages;
require_once('twitterapiexchange.php');


class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'home';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {

        $this->set_meta_info($this->_meta_slug);

        $basic_parameter = \Config::get('twitter');
    //var_dump($basic_parameter);
        $url = "https://api.twitter.com/1.1/search/tweets.json";
        $requestMethod = "GET";
        $getField = "?q=#WAM2015";
        $count = 0;

        $twitter = new \TwitterAPIExchange($basic_parameter);
        $string = json_decode($twitter->setGetfield($getField)
            ->buildOauth($url, $requestMethod)
            ->performRequest(), $assoc = TRUE);

        $data_twitter = array();
        foreach ($string['statuses'] as $item)
        {
            $created_at = $item['user']['created_at'];
            $date = implode(' ', array_slice(explode(' ', $created_at), 0, 3));
            $year = implode(' ', array_slice(explode(' ', $created_at), 5, 6));
            $time = implode(' ', array_slice(explode(' ', $created_at), 3, 1));
            $count += 1;

            $la = $item['coordinates']['coordinates'][0];
            $lo = $item['coordinates']['coordinates'][1];

            $data_twitter[] = array(
                'twitter_name'          => $item['user']['name'],
                'twitter_location'      =>$item['user']['location'],
                'twitter_date'          =>$date.' '.$year,
                'twitter_time'          =>$time,
                'twitter_text'          =>$item['text'],
                'twitter_coordinates'   =>$la.", ".$lo
            );

            //return \Response::forge(\View::forge('pages::frontend/home.twig', $this->_data_template, FALSE));
        }
        //var_dump($count); exit;
        $data_twitter['twitter_user_total'] = $count;
        //var_dump($data_twitter['twitter_user_total']); exit;
        //$this->_data_template['rows'] = $data_twitter;
        return \Response::forge(\View::forge('pages::frontend/home.twig', $this->_data_template));

    }
    
    public function action_privacy() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('pages::frontend/privacy_policy.twig', $this->_data_template, FALSE));
    }
    
    
    public function action_add_event() {
        $post_data = \Input::post();
        $post_file = \Input::file();
        $myemail = 'khomeini@microad.co.id';
        
        if(!empty($post_data)){
            $eventname = $post_data['eventname'];
            $email = $post_data['email'];
            $desc = $post_data['desc'];
            $date = $post_data['date'];
            $time = $post_data['time'];
            $country = $post_data['countryf'];
            $city = $post_data['cityf'];
            $location = $post_data['location'];
            $organisation = $post_data['organisation'];
            $weblink = $post_data['weblink'];
            $file = $post_file['uploaded_file'];
            $destination_path = DOCROOT.'media/';
            $success_message = 'Your event has been submitted. Thanks!';
            $status_message = 1;
            $target_path = $destination_path . basename($file['name']);
            move_uploaded_file($file['tmp_name'], $target_path);
            
            
            $email_body = "You have received a new message. ".
            " Here are the details:\n Event Name: $eventname \n ".
            "Email: $email\n ".
            "Description: $desc\n ".
            "Date: $date\n ".
            "Time: $time\n ".
            "Country: $country\n ".
            "City: $city\n ".
            "Location: $location\n ".
            "Organisation: $organisation\n ".
            "Weblink: $weblink\n ";
            
            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
                        ->setUsername($myemail)
                        ->setPassword('K1q2w3e@#');

            //Create the Mailer using your created Transport
            $mailer = \Swift_Mailer::newInstance($transport);
            
            
            //Create a message
            $message = \Swift_Message::newInstance($eventname)
              ->setFrom(array($myemail => 'Event Organizer'))
              ->setTo(array($myemail))
              ->setBody($email_body);
            $message->attach(\Swift_Attachment::fromPath($target_path));
            
            //Send the message
            $mailer->send($message);
            $this->_data_template['success_message'] = $success_message;            
            $this->_data_template['status_message'] = $status_message; 
        }  else {
            
        }
        $this->set_meta_info($this->_meta_slug);
        
        
        return \Response::forge(\View::forge('pages::frontend/add_event.twig', $this->_data_template, FALSE));
    }
    public function action_campaign() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('pages::frontend/campaign.twig', $this->_data_template));

    }
}