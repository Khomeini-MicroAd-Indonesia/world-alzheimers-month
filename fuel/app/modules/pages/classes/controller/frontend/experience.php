<?php
namespace Pages;

class Controller_Frontend_Experience extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'experience';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }
    public function action_experience_one() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('pages::frontend/experience.twig', $this->_data_template, FALSE));
    }
    public function action_experience_two() {
        $this->set_meta_info($this->_meta_slug);
        if($this->_is_fb_connect = TRUE){  
            return \Response::forge(\View::forge('pages::frontend/experience_two.twig', $this->_data_template, FALSE));
        }else{
            return \Response::forge(\View::forge('pages::frontend/experience.twig', $this->_data_template, FALSE));
        }
    }
    public function action_experience_three() {
        $this->set_meta_info($this->_meta_slug);
        if($this->_is_fb_connect = TRUE){
            $photo = \FacebookManagement\Util_Api::getPhoto();
            $this->_data_template['fb_info'] = $photo;
            return \Response::forge(\View::forge('pages::frontend/experience_three.twig', $this->_data_template, FALSE));
        }else{
            return \Response::forge(\View::forge('pages::frontend/experience.twig', $this->_data_template, FALSE));
        }
    }
    public function action_experience_four() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('pages::frontend/experience_four.twig', $this->_data_template, FALSE));
    }
    
    public function action_fb_logout() {
        unset($_SESSION['fb_token']);
        return \Response::forge(\View::forge('pages::frontend/experience.twig', $this->_data_template, FALSE));
    }

    /*new do you remeber me?*/
    private function generate_base64_fb_img ($path) {
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }
    public function action_experience_new_one() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('pages::frontend/experience_new_1.twig', $this->_data_template, FALSE));
    }
    public function action_experience_new_one_alt() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('pages::frontend/experience_new_1_alt.twig', $this->_data_template, FALSE));
    }
    public function action_experience_new_two() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('pages::frontend/experience_new_2.twig', $this->_data_template, FALSE));
    }
    public function action_experience_new_three() {
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['next_step'] = 4;
        if($this->_is_fb_connect = TRUE){
            $photo = \FacebookManagement\Util_Api::getPhoto();
//            $this->_data_template['fb_img_base64'] = $this->generate_base64_fb_img($photo['source']);
            return \Response::forge(\View::forge('pages::frontend/experience_new_3.twig', $this->_data_template, FALSE));
        }else{
            \Response::redirect(\Uri::base().$this->_current_lang.'/do-you-remember-me');
        }
    }
    public function action_experience_new_four() {
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['next_step'] = 5;
        if($this->_is_fb_connect = TRUE){
            $photo = \FacebookManagement\Util_Api::getPhoto();
            $this->_data_template['fb_img_base64'] = $this->generate_base64_fb_img($photo['source']);
            return \Response::forge(\View::forge('pages::frontend/experience_new_4.twig', $this->_data_template, FALSE));
        }else{
            \Response::redirect(\Uri::base().$this->_current_lang.'/do-you-remember-me');
        }
    }
    public function action_experience_new_five() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('pages::frontend/experience_new_5.twig', $this->_data_template, FALSE));
    }
    public function action_experience_new_six() {
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['next_step'] = 7;
        if($this->_is_fb_connect = TRUE){
            $photo = \FacebookManagement\Util_Api::getPhoto();
            $this->_data_template['fb_img_base64'] = $this->generate_base64_fb_img($photo['source']);
            return \Response::forge(\View::forge('pages::frontend/experience_new_6.twig', $this->_data_template, FALSE));
        }else{
            \Response::redirect(\Uri::base().$this->_current_lang.'/do-you-remember-me');
        }
    }
    public function action_experience_new_seven() {
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['next_step'] = 8;
        if($this->_is_fb_connect = TRUE){
            $photo = \FacebookManagement\Util_Api::getPhoto();
            $this->_data_template['fb_img_base64'] = $this->generate_base64_fb_img($photo['source']);
            return \Response::forge(\View::forge('pages::frontend/experience_new_7.twig', $this->_data_template, FALSE));
        }else{
            \Response::redirect(\Uri::base().$this->_current_lang.'/do-you-remember-me');
        }
    }
    public function action_experience_new_eight() {
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['next_step'] = 9;
        return \Response::forge(\View::forge('pages::frontend/experience_new_8.twig', $this->_data_template, FALSE));
    }
    public function action_experience_new_nine() {
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['next_step'] = 10;
        if($this->_is_fb_connect = TRUE){
            $photo = \FacebookManagement\Util_Api::getPhoto();
            $this->_data_template['fb_img_base64'] = $this->generate_base64_fb_img($photo['source']);
            return \Response::forge(\View::forge('pages::frontend/experience_new_9.twig', $this->_data_template, FALSE));
        }else{
            \Response::redirect(\Uri::base().$this->_current_lang.'/do-you-remember-me');
        }
    }
    public function action_experience_new_ten() {
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['next_step'] = 11;
        if($this->_is_fb_connect = TRUE){
            $photo = \FacebookManagement\Util_Api::getPhoto();
            $this->_data_template['fb_img_base64'] = $this->generate_base64_fb_img($photo['source']);
            return \Response::forge(\View::forge('pages::frontend/experience_new_9.twig', $this->_data_template, FALSE));
        }else{
            \Response::redirect(\Uri::base().$this->_current_lang.'/do-you-remember-me');
        }
    }
    public function action_experience_new_eleven() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('pages::frontend/experience_new_11.twig', $this->_data_template, FALSE));
    }
    /*end new do you rember me?*/
}