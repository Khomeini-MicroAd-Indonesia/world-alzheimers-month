<?php
namespace Pages;

class Controller_Frontend_About extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'about';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }
    public function action_dementia() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('pages::frontend/about_dementia.twig', $this->_data_template, FALSE));
    }
    public function action_wam() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('pages::frontend/about_wam.twig', $this->_data_template, FALSE));
    }
}