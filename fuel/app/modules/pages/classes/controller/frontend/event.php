<?php
namespace Pages;

class Controller_Frontend_Event extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'event';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }
    
    public function action_involved() {
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['region_data'] = \Event\Eventutil::get_event_region();
        $this->_data_template['main_event'] = \Event\Eventutil::get_main_event();
        $this->_data_template['fbprofilepic_img'] = \Downloadableassets\Assetutil::get_fb_profile_pic();
        $this->_data_template['fbcover_img'] = \Downloadableassets\Assetutil::get_fb_cover_pic();
        $this->_data_template['printad_img'] = \Downloadableassets\Assetutil::get_printad_pic();
        $this->_data_template['printadtypo_img'] = \Downloadableassets\Assetutil::get_printadtypo_pic();
        $this->_data_template['socmedposter_img'] = \Downloadableassets\Assetutil::get_socmedposter_pic();
        $this->_data_template['tshirtdesign_img'] = \Downloadableassets\Assetutil::get_tshirtdesign_pic();
        return \Response::forge(\View::forge('pages::frontend/get_involved.twig', $this->_data_template, FALSE));
    }
    
}