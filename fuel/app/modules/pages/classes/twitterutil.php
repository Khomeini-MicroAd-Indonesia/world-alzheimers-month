<?php


class Twitterutil{
    
    public static function get_twitter_setting(){
        $settings = Model_Settings::query()
                ->where('setting_type', 'twitter')
                ->get();
        
        if(empty($settings)){
            $data = array();
        }else{
            foreach ($settings as $setting){
                $data[$setting->setting_name] = $setting->setting_value;
            }
        }
        return $data;
    }
    
}
        
        