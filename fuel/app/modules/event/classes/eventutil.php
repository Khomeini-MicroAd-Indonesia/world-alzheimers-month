<?php
namespace Event;

class Eventutil{

    /**
     * get event region
     */
    
    public static function get_event_region()
    {
        $regions = \Event\Model_Regions::query()
                ->where('status', 1)
                ->order_by('name', 'asc')
                ->get();
        
        if(empty($regions)){
            $data = array();
        }else{
            foreach ($regions as $region){
                $data[$region->id] = $region->name;
            }
        }
        return $data;
    }
    
    /**
     * get main event
     */
    
    public static function get_main_event()
    {
        $events = \Event\Model_Events::query()
                ->where('status', 1)
                //->where('city_id', 4)
                ->order_by('post_date', 'desc')
                ->related('cities')
                ->related('event_images')
                ->get();
        
        if(empty($events)){
            $data = array();
        }else{
            foreach ($events as $event){
                if($event->cities->name == 'International HQ'){
                    $data[] = array(
                    'name'          => $event->name,
                    'post_date'     => date('l, d F Y', strtotime($event->post_date)),
                    'event_date'    => date('l d F Y', strtotime($event->event_date)),
                    'meta_desc'     => $event->meta_desc,
                    'phone'         => $event->cities->phone,
                    'email'         => $event->cities->email,
                    'web'           => $event->cities->web,
                    'content'       => $event->content,
                    'image'         => $event->event_images->filename
                    );
                }
             
            }
        }
        return $data;
    }
    
}