<?php
namespace Event;

class Model_Events extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'events';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'events'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'city_id' => array(
			'label' => 'City',
			'validation' => array(
				'required',
			)
		),
                'image_id' => array(
			'label' => 'Image',
			'validation' => array(
				'required',
			)
		),
		'name' => array(
			'label' => 'Event Name',
			'validation' => array(
				'required',
				'max_length' => array(80),
			)
		),
                'content' => array(
                        'label' => 'Event Content',
                        'validation' => array()
                ),
		'meta_title' => array(
			'label' => 'Meta Title',
			'validation' => array(
				'max_length' => array(80),
			)
		),
                'meta_desc' => array(
			'label' => 'Meta Description',
			'validation' => array(
				'max_length' => array(150),
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
                'post_date' => array(
			'label' => 'Post Date',
			'validation' => array(
				'required',
				'valid_date' => array(
					'format' => 'Y-m-d'
				)
			)
		),
                'event_date' => array(
			'label' => 'Event Date',
			'validation' => array(
				'required',
				'valid_date' => array(
					'format' => 'Y-m-d'
				)
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
        protected static $_belongs_to = array(
            'cities' => array(
                'key_from' => 'city_id',
                'model_to' => '\Event\Model_Cities',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            ),
            'event_images' => array(
                'key_from' => 'image_id',
                'model_to' => '\Event\Model_EventImages',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            )
        
        );
        
        private static $_cities;
        
	public function get_status_name() {
            $flag = $this->status;
            return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
        
        public function get_city_name(){
            if(empty(self::$_cities)){
                self::$_cities = Model_Cities::get_as_array();
            }
            $flag = $this->city_id;
            return isset(self::$_cities[$flag]) ? self::$_cities[$flag] : '-';
        }
	
	public function get_form_data_basic($city, $image) {
		return array(
			'attributes' => array(
				'name' => 'frm_event_event',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'City',
						'id' => 'city_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'city_id',
						'value' => $this->city_id,
						'options' => $city,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'City',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Image',
						'id' => 'image_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'image_id',
						'value' => $this->image_id,
						'options' => $image,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'Image',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Event Name',
						'id' => 'event_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'event_name',
						'value' => $this->name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Event Name',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Post Date',
						'id' => 'post_date',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'post_date',
						'value' => $this->post_date,
						'attributes' => array(
							'class' => 'form-control mask-date',
							'placeholder' => 'Post Date',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Event Date',
						'id' => 'event_date',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'event_date',
						'value' => $this->event_date,
						'attributes' => array(
							'class' => 'form-control mask-date',
							'placeholder' => 'Event Date',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Meta Title',
						'id' => 'event_meta_title',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'event_meta_title',
						'value' => $this->meta_title,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Meta Title',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Meta Description',
						'id' => 'event_meta_desc',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'event_meta_desc',
						'value' => $this->meta_desc,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Meta Description',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
                            array(
					'label' => array(
						'label' => 'Event Content',
						'id' => 'event_content',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'event_content',
						'value' => $this->content,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				)
			)
		);
	}
}
