<?php
namespace Event;

class Model_Countries extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'countries';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'events'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
		'region_id' => array(
			'label' => 'Region',
			'validation' => array(
				'required',
			)
		),
		'name' => array(
			'label' => 'Event Country',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
	private static $_regions;
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public function get_region_name() {
		if (empty(self::$_regions)) {
			self::$_regions = Model_Regions::get_as_array();
		}
		$flag = $this->region_id;
		return isset(self::$_regions[$flag]) ? self::$_regions[$flag] : '-';
        }
	
        public static function get_as_array ($filter=array()) {
		$items = self::find('all', $filter);
		if (empty($items)) {
			$data = array();
		} else {
			foreach ($items as $item) {
				$data[$item->id] = $item->name;
			}
		}
		return $data;
	}
        
	public function get_form_data_basic($region) {
		return array(
			'attributes' => array(
				'name' => 'frm_event_country',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'Region',
						'id' => 'region_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'region_id',
						'value' => $this->region_id,
						'options' => $region,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'Region',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Country Name',
						'id' => 'country_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'country_name',
						'value' => $this->name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Country Name',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				
			)
		);
	}
}
