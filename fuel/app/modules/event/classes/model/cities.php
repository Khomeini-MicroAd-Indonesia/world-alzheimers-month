<?php
namespace Event;

class Model_Cities extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'cities';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'events'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
		'country_id' => array(
			'label' => 'Country',
			'validation' => array(
				'required',
			)
		),
		'name' => array(
			'label' => 'City Name',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
		'phone' => array(
			'label' => 'City Phone',
			'validation' => array(
				'required',
				'max_length' => array(125),
			)
		),
                'email' => array(
			'label' => 'City E-mail',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'web' => array(
			'label' => 'City Web',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
		'address' => array(
			'label' => 'City Address',
			'validation' => array(
				'required',
				'max_length' => array(255),
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
        protected static $_has_many = array(
            'events' => array(
                'key_from'          =>  'id',
                'model_to'          =>  '\Event\Model_Events',
                'key_to'            =>  'city_id',
                'cascade_save'      =>  false,
                'cascade_delete'    =>  false
                
            )
        );
        
	private static $_countries;
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public function get_country_name() {
		if (empty(self::$_countries)) {
			self::$_countries = Model_Countries::get_as_array();
		}
		$flag = $this->country_id;
		return isset(self::$_countries[$flag]) ? self::$_countries[$flag] : '-';
	}
        
        public static function get_as_array ($filter=array()) {
		$items = self::find('all', $filter);
		if (empty($items)) {
			$data = array();
		} else {
			foreach ($items as $item) {
				$data[$item->id] = $item->name;
			}
		}
		return $data;
	}
	
	public function get_form_data_basic($country) {
		return array(
			'attributes' => array(
				'name' => 'frm_event_country',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'Country',
						'id' => 'country_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'country_id',
						'value' => $this->country_id,
						'options' => $country,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'Country',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'City Name',
						'id' => 'city_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'city_name',
						'value' => $this->name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'City Name',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'City Phone',
						'id' => 'city_phone',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'city_phone',
						'value' => $this->phone,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'City Phone',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'City E-mail',
						'id' => 'city_email',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'city_email',
						'value' => $this->email,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'City Email',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                            array(
					'label' => array(
						'label' => 'City Web',
						'id' => 'city_web',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'city_web',
						'value' => $this->web,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'City Web',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'City Address',
						'id' => 'city_address',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'city_address',
						'value' => $this->address,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => '',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				)
			)
		);
	}
}
