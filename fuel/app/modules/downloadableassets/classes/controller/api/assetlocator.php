<?php

namespace Downloadableassets;

class Controller_API_Assetlocator extends \Controller_Frontend
{
    
    private $_module_url = '';
    private $_menu_key = 'dowloadableassets';
    
    public function action_getCountry(){
        $countries = \Event\Model_Countries::query()
                ->where('region_id', \Input::get('id'))
                ->where('status', 1)
                ->get();
        
        $data = array();
        
        foreach ($countries as $country){
            $data[] = array(
                'id'    => $country->id,
                'name'  => $country->name
            );
            
        }
        
        return new \Response(json_encode($data),200);
    }
     
    public function action_getCity(){
        $cities = \Event\Model_Cities::query()
                ->where('country_id', \Input::get('id'))
                ->where('status', 1)
                ->get();
        
        $data = array();
        foreach ($cities as $city){
            $data[] = array(
                'id'    => $city->id,
                'name'  => $city->name,
                'phone' => $city->phone,
                'email' => $city->email,
                'web'   => $city->web
            );
        }
        return new \Response(json_encode($data),200);
    }
    
    public function action_getEvent(){
        $events = \Event\Model_Events::query()
                ->where('city_id', \Input::get('id'))
                ->where('status', 1)
                ->related('cities')
                ->get();
                
        $data = array();
        //$post_date;
        foreach ($events as $event){
            
            $data[] = array(
                'name'      => $event->name,
                'post_date' => date('l, d F Y', strtotime($event->post_date)),
                'meta_desc' => $event->meta_desc,
                'phone'     => $event->cities->phone,
                'email'     => $event->cities->email,
                'web'       => $event->cities->web,
                'content'   => $event->content
            );
        }
        return new \Response(json_encode($data),200);
    }
    
}



