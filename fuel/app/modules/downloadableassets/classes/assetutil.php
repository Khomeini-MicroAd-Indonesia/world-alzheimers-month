<?php
namespace Downloadableassets;

class Assetutil{

    /**
     * get asset image
     */
    
    public static function get_fb_profile_pic()
    {
        $profilepics = \Downloadableassets\Model_Fbprofilepics::query()
                ->where('status', 1)
                ->get();
        
        if(empty($profilepics)){
            $data = array();
        }else{
            foreach ($profilepics as $profilepic){
                $data[$profilepic->id] = $profilepic->filename;
            }
        }
        return $data;
        
    }
    
    public static function get_fb_cover_pic()
    {
        $fbcoverpics = \Downloadableassets\Model_Fbcovers::query()
                ->where('status', 1)
                ->get();
        
        if(empty($fbcoverpics)){
            $data = array();
        }else{
            foreach ($fbcoverpics as $fbcoverpic){
                $data[$fbcoverpic->id] = $fbcoverpic->filename;
            }
        }
        return $data;
        
    }
    
    public static function get_printad_pic()
    {
        $printadpics = \Downloadableassets\Model_Printads::query()
                ->where('status', 1)
                ->get();
        
        if(empty($printadpics)){
            $data = array();
        }else{
            foreach ($printadpics as $printadpic){
                $data[$printadpic->id] = $printadpic->filename;
            }
        }
        return $data;
        
    }
    
    public static function get_printadtypo_pic()
    {
        $printadtypopics = \Downloadableassets\Model_Printadtypos::query()
                ->where('status', 1)
                ->get();
        
        if(empty($printadtypopics)){
            $data = array();
        }else{
            foreach ($printadtypopics as $printadtypopic){
                $data[$printadtypopic->id] = $printadtypopic->filename;
            }
        }
        return $data;
        
    }
    
    public static function get_socmedposter_pic()
    {
        $socmedposterpics = \Downloadableassets\Model_Socmedposters::query()
                ->where('status', 1)
                ->get();
        
        if(empty($socmedposterpics)){
            $data = array();
        }else{
            foreach ($socmedposterpics as $socmedposterpic){
                $data[$socmedposterpic->id] = $socmedposterpic->filename;
            }
        }
        return $data;
        
    }
    
    public static function get_tshirtdesign_pic()
    {
        $tshirtdesignpics = \Downloadableassets\Model_Tshirtdesigns::query()
                ->where('status', 1)
                ->get();
        
        if(empty($tshirtdesignpics)){
            $data = array();
        }else{
            foreach ($tshirtdesignpics as $tshirtdesignpic){
                $data[$tshirtdesignpic->id] = $tshirtdesignpic->filename;
            }
        }
        return $data;
        
    }
    
}