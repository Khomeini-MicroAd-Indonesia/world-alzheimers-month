<?php

namespace MediaUploader;

class Controller_EventImage extends \Controller_Backend {
    
    private $_module_url = 'backend/mediauploader/event-image';
    private $_menu_key = 'media_uploader_eventimage';
    
    public function before() {
        parent::before();
        $this->authenticate();
        
        //Check menu permission
        if (!$this->check_menu_permission($this->_menu_key, 'read')) {
            // if not have an access then redirect to error page
            \Response::redirect(\Uri::base().'backend/no-permission');
        }
        
        $this->_data_template['meta_title'] = 'Media Uploader - Event Image';
        $this->_data_template['menu_parent_key'] = 'media_uploader';
        $this->_data_template['menu_current_key'] = 'media_uploader_eventimage';
    }
	
    public function action_index() {
        $this->_data_template['success_message'] = \Session::get_flash('success_message');
        $this->_data_template['error_message'] = \Session::get_flash('error_message');
        $this->_data_template['media_uploader_subtitle'] = 'Event Image';
        $this->_data_template['form_action_url'] = \Uri::base().$this->_module_url.'/upload';
        $this->_data_template['information_text'] = '
            <ul>
                <li>Max filesize is 1 MB</li>
                <li>Optimal Image Dimension is 831px x 315px</li>
            </ul>
        ';
		
        return \Response::forge(\View::forge('backend/jquery_image_uploader.twig', $this->_data_template, FALSE));
        
    }
        
    public function action_upload() {
        \Package::load('jqueryfileupload');
        $options = array(
            'script_url' => \Uri::base().$this->_module_url.'/upload',
            'upload_dir' => DOCROOT.'media/eventimage/',
            'upload_url' => \Uri::base().'media/eventimage/',
            'max_file_size' => 1024 * 1024, // 1 MB
            'image_versions' => array(
            '' => array(
                'auto_orient' => false,
                'max_width' => 831,
                'max_height' => 315,
            ),
                'thumbnail' => array(
                    'crop' => true,
                    'max_width' => 218,
                    'max_height' => 218,
                )
            ),
        );
            $upload_handler = new \JqueryFileUploadHandler($options);
    }
    
} 