<?php
namespace FacebookManagement;

class Model_Participants extends \Orm\Model {
	protected static $_table_name = 'participants';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'events'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
		'fb_suid' => array(
			'label' => 'FB Scoped User ID',
			'validation' => array(
				'required',
			)
		),
		'fb_fullname' => array(
			'label' => 'Fullname',
			'validation' => array(
				'required',
				'max_length' => array(225),
			)
		),
		'fb_link' => array(
			'label' => 'FB Link',
			'validation' => array(
				'required',
				'numeric_max' => array(225),
			)
		),
		'fb_email',
		'gender',
		'created_at',
		'updated_at'
	);
}
