<?php

/* about_dementia.twig */
class __TwigTemplate_6c608dd8c4c71de62d97dd84c882e6756967ab6e7b6d771f5b2b3a6b110085a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_header($context, array $blocks = array())
    {
    }

    // line 5
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 6
        echo "    <h3 class=\"main-title\">";
        echo Lang::get("txt_title_faq");
        echo "</h3>
    <div class=\"channel d-display\">
        <div class=\"row faq-dementia\">
            <div class=\"col-md-2\">
                <div class=\"dementia-1\"></div>
            </div>
            <div class=\"col-md-10\">
                <p class=\"question line\"><span>";
        // line 13
        echo Lang::get("txt_dementia_q1");
        echo "</span></p>
                <div class=\"newspaper\">";
        // line 14
        echo Lang::get("txt_dementia_a1");
        echo "</div>
            </div>
        </div>
        <div class=\"row faq-dementia\">
            <div class=\"col-md-2\">
                <div class=\"dementia-2\"></div>
            </div>
            <div class=\"col-md-10\">
                <p class=\"question line\"><span>";
        // line 22
        echo Lang::get("txt_dementia_q2");
        echo "</span></p>
                <div class=\"newspaper\">";
        // line 23
        echo Lang::get("txt_dementia_a2");
        echo "</div>
            </div>
        </div>
        <div class=\"row faq-dementia\">
            <div class=\"col-md-2\">
                <div class=\"dementia-3\"></div>
            </div>
            <div class=\"col-md-10\">
                <p class=\"question line\"><span>";
        // line 31
        echo Lang::get("txt_dementia_q3");
        echo "</span></p>
                <div class=\"newspaper\">";
        // line 32
        echo Lang::get("txt_dementia_a3");
        echo "</div>
            </div>
        </div>
        <div class=\"row faq-dementia\">
            <div class=\"col-md-2\">
                <div class=\"dementia-4\"></div>
            </div>
            <div class=\"col-md-10\">
                <p class=\"question line\"><span>";
        // line 40
        echo Lang::get("txt_dementia_q4");
        echo "</span></p>
                <div class=\"newspaper\">";
        // line 41
        echo Lang::get("txt_dementia_a4");
        echo "</div>
            </div>
        </div>
        <div class=\"row faq-dementia\">
            <div class=\"col-md-2\">
                <div class=\"dementia-5\"></div>
            </div>
            <div class=\"col-md-10\">
                <p class=\"question line\"><span>";
        // line 49
        echo Lang::get("txt_dementia_q5");
        echo "</span></p>
                <div class=\"newspaper\">";
        // line 50
        echo Lang::get("txt_dementia_a5");
        echo "</div>
            </div>
        </div>
        <div class=\"row faq-dementia\">
            <div class=\"col-md-2\">
                <div class=\"dementia-6\"></div>
            </div>
            <div class=\"col-md-10\">
                <div class=\"question line\">
                    <p><span>";
        // line 59
        echo Lang::get("txt_dementia_q6");
        echo "</span></p>
                </div>
                <div class=\"newspaper\">";
        // line 61
        echo Lang::get("txt_dementia_a6");
        echo "</div>
            </div>
        </div>
    </div>
    <div class=\"channel m-display\">
        <div class=\"panel-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">
            <div class=\"panel panel-default\">
                <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
                    <div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">
                     <h4 class=\"panel-title\">
                         ";
        // line 71
        echo Lang::get("txt_dementia_q1");
        echo " <span class=\"triangle\"></span>
                        </h4>
                </div>
                </a>
                <div id=\"collapseOne\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\">
                    <div class=\"panel-body\">
                        <p>";
        // line 77
        echo Lang::get("txt_dementia_a1a");
        echo "</p>
                        <div class=\"dementia-1\"></div>
                        <p>";
        // line 79
        echo Lang::get("txt_dementia_a1b");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"panel panel-default\">
                <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">
                    <div class=\"panel-heading\" role=\"tab\" id=\"headingTwo\">
                        <h4 class=\"panel-title\">

                            ";
        // line 88
        echo Lang::get("txt_dementia_q2");
        echo "<span class=\"triangle\"></span>

                    </h4>
                </div>
                </a>
                <div id=\"collapseTwo\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo\">
                    <div class=\"panel-body\">
                        <p>";
        // line 95
        echo Lang::get("txt_dementia_a2a");
        echo "</p>
                        <div class=\"dementia-2\"></div>
                        <p>";
        // line 97
        echo Lang::get("txt_dementia_a2b");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"panel panel-default\">
                <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\">
                    <div class=\"panel-heading\" role=\"tab\" id=\"headingThree\">
                    <h4 class=\"panel-title\">
                        ";
        // line 105
        echo Lang::get("txt_dementia_q3");
        echo "<span class=\"triangle\"></span>
                    </h4>
                </div>
                </a>
                <div id=\"collapseThree\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree\">
                    <div class=\"panel-body\">
                        <p>";
        // line 111
        echo Lang::get("txt_dementia_a3a");
        echo "</p>
                        <div class=\"dementia-3\"></div>
                        <p>";
        // line 113
        echo Lang::get("txt_dementia_a3b");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"panel panel-default\">
                <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseFour\" aria-expanded=\"false\" aria-controls=\"collapseFour\">
                    <div class=\"panel-heading\" role=\"tab\" id=\"headingFour\">
                    <h4 class=\"panel-title\">
                        ";
        // line 121
        echo Lang::get("txt_dementia_q4");
        echo "<span class=\"triangle\"></span>
                    </h4>
                </div>
                </a>
                <div id=\"collapseFour\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingFour\">
                    <div class=\"panel-body\">
                        <p>";
        // line 127
        echo Lang::get("txt_dementia_a4a");
        echo "</p>
                        <div class=\"dementia-4\"></div>
                        <p>";
        // line 129
        echo Lang::get("txt_dementia_a4b");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"panel panel-default\">
                <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseFive\" aria-expanded=\"false\" aria-controls=\"collapseFive\">
                    <div class=\"panel-heading\" role=\"tab\" id=\"headingFive\">
                    <h4 class=\"panel-title\">
                        ";
        // line 137
        echo Lang::get("txt_dementia_q5");
        echo "<span class=\"triangle\"></span>
                    </h4>
                </div>
                </a>
                <div id=\"collapseFive\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingFive\">
                    <div class=\"panel-body\">
                        <p>";
        // line 143
        echo Lang::get("txt_dementia_a5a");
        echo "</p>
                        <div class=\"dementia-5\"></div>
                        <p>";
        // line 145
        echo Lang::get("txt_dementia_a5b");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"panel panel-default\">
                <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseSix\" aria-expanded=\"false\" aria-controls=\"collapseSix\">
                    <div class=\"panel-heading\" role=\"tab\" id=\"headingSix\">
                    <h4 class=\"panel-title\">
                        ";
        // line 153
        echo Lang::get("txt_dementia_q6");
        echo "<span class=\"triangle\"></span>
                    </h4>
                </div>
                </a>
                <div id=\"collapseSix\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingSix\">
                    <div class=\"panel-body\">
                        <p>";
        // line 159
        echo Lang::get("txt_dementia_a6a");
        echo "</p>
                        <div class=\"dementia-6\"></div>
                        <p>";
        // line 161
        echo Lang::get("txt_dementia_a6b");
        echo "</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"parent\">
        <div class=\"child\">
            <div class=\"box css3-shadow\"></div>
            <h3 class=\"main-title\">";
        // line 170
        echo Lang::get("txt_title_signs");
        echo "</h3>
            <div id=\"carousel-example-generic\" class=\"carousel slide\" data-ride=\"carousel\">
                <!-- Indicators -->

                <!-- Wrapper for slides -->
                <div class=\"carousel-inner about-dementia\" role=\"listbox\">
                    <div class=\"item active\">
                        <img class=\"img-responsive\" src=\"./assets/img/sign-1.jpg\" alt=\"Dementia Sign\">
                        <div class=\"carousel-caption\">
                            <p class=\"question\">";
        // line 179
        echo Lang::get("txt_subtitle_sign1");
        echo "</p>
                            <p>";
        // line 180
        echo Lang::get("txt_desc_sign1");
        echo "</p>
                            <hr/>
                            <p class=\"footnote\">";
        // line 182
        echo Lang::get("txt_source");
        echo " : <a href=\"http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp\"> http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp</a></p>
                        </div>
                    </div>
                    <div class=\"item\">
                        <img class=\"img-responsive\" src=\"./assets/img/sign-2.jpg\" alt=\"Dementia Sign\">
                        <div class=\"carousel-caption\">
                            <p class=\"question\">";
        // line 188
        echo Lang::get("txt_subtitle_sign2");
        echo "</p>
                            <p>";
        // line 189
        echo Lang::get("txt_desc_sign2");
        echo "</p>
                            <hr/>
                            <p class=\"footnote\">";
        // line 191
        echo Lang::get("txt_source");
        echo " : <a href=\"http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp\"> http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp</a></p>
                        </div>
                    </div>
                    <div class=\"item\">
                        <img class=\"img-responsive\" src=\"./assets/img/sign-3.jpg\" alt=\"Dementia Sign\">
                        <div class=\"carousel-caption\">
                            <p class=\"question\">";
        // line 197
        echo Lang::get("txt_subtitle_sign3");
        echo "</p>
                            <p>";
        // line 198
        echo Lang::get("txt_desc_sign3");
        echo "</p>
                            <hr/>
                            <p class=\"footnote\">";
        // line 200
        echo Lang::get("txt_source");
        echo " : <a href=\"http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp\"> http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp</a></p>
                        </div>
                    </div>
                    <div class=\"item\">
                        <img class=\"img-responsive\" src=\"./assets/img/sign-4.jpg\" alt=\"Dementia Sign\">
                        <div class=\"carousel-caption\">
                            <p class=\"question\">";
        // line 206
        echo Lang::get("txt_subtitle_sign4");
        echo "</p>
                            <p>";
        // line 207
        echo Lang::get("txt_desc_sign4");
        echo "</p>
                            <hr/>
                            <p class=\"footnote\">";
        // line 209
        echo Lang::get("txt_source");
        echo " : <a href=\"http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp\"> http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp</a></p>
                        </div>
                    </div>
                    <div class=\"item\">
                        <img src=\"./assets/img/sign-5.jpg\" alt=\"...\">
                        <div class=\"carousel-caption\">
                            <p class=\"question\">";
        // line 215
        echo Lang::get("txt_subtitle_sign5");
        echo "</p>
                            <p>";
        // line 216
        echo Lang::get("txt_desc_sign5");
        echo "</p>
                            <hr/>
                            <p class=\"footnote\">";
        // line 218
        echo Lang::get("txt_source");
        echo " : <a href=\"http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp\"> http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp</a></p>
                        </div>
                    </div>
                    <div class=\"item\">
                        <img src=\"./assets/img/sign-6.jpg\" alt=\"Dementia Sign\">
                        <div class=\"carousel-caption\">
                            <p class=\"question\">";
        // line 224
        echo Lang::get("txt_subtitle_sign6");
        echo "</p>
                            <p>";
        // line 225
        echo Lang::get("txt_desc_sign6");
        echo "</p>
                            <hr/>
                            <p class=\"footnote\">";
        // line 227
        echo Lang::get("txt_source");
        echo " : <a href=\"http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp\"> http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp</a></p>
                        </div>
                    </div>
                    <div class=\"item\">
                        <img src=\"./assets/img/sign-7.jpg\" alt=\"Dementia Sign\">
                        <div class=\"carousel-caption\">
                            <p class=\"question\">";
        // line 233
        echo Lang::get("txt_subtitle_sign7");
        echo "</p>
                            <p>";
        // line 234
        echo Lang::get("txt_desc_sign7");
        echo "</p>
                            <hr/>
                            <p class=\"footnote\">";
        // line 236
        echo Lang::get("txt_source");
        echo " : <a href=\"http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp\"> http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp</a></p>
                        </div>
                    </div>
                    <div class=\"item\">
                        <img src=\"./assets/img/sign-8.jpg\" alt=\"Dementia Sign\">
                        <div class=\"carousel-caption\">
                            <p class=\"question\">";
        // line 242
        echo Lang::get("txt_subtitle_sign8");
        echo "</p>
                            <p>";
        // line 243
        echo Lang::get("txt_desc_sign8");
        echo "</p>
                            <hr/>
                            <p class=\"footnote\">";
        // line 245
        echo Lang::get("txt_source");
        echo " : <a href=\"http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp\"> http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp</a></p>
                        </div>
                    </div>
                    <div class=\"item\">
                        <img src=\"./assets/img/sign-9.jpg\" alt=\"Dementia Sign\">
                        <div class=\"carousel-caption\">
                            <p class=\"question\">";
        // line 251
        echo Lang::get("txt_subtitle_sign9");
        echo "</p>
                            <p>";
        // line 252
        echo Lang::get("txt_desc_sign9");
        echo "</p>
                            <hr/>
                            <p class=\"footnote\">";
        // line 254
        echo Lang::get("txt_source");
        echo " : <a href=\"http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp\"> http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp</a></p>
                        </div>
                    </div>
                    <div class=\"item\">
                        <img src=\"./assets/img/sign-10.jpg\" alt=\"Dementia Sign\">
                        <div class=\"carousel-caption\">
                            <p class=\"question\">";
        // line 260
        echo Lang::get("txt_subtitle_sign10");
        echo "</p>
                            <p>";
        // line 261
        echo Lang::get("txt_desc_sign10");
        echo "</p>
                            <hr/>
                            <p class=\"footnote\">";
        // line 263
        echo Lang::get("txt_source");
        echo " : <a href=\"http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp\"> http://www.alz.org/alzheimers_disease_10_signs_of_alzheimers.asp</a></p>
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class=\"left carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"prev\">
                    <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Previous</span>
                </a>
                <a class=\"right carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"next\">
                    <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Next</span>
                </a>
            </div>
            <div class=\"box css3-shadow\"></div>
        </div>
    </div>
    <div class=\"parent\">
        <div class=\"child\">
            <h3 class=\"main-title\">";
        // line 283
        echo Lang::get("txt_title_find_help");
        echo "</h3>
            ";
        // line 285
        echo "            <div class=\"newspaper left d-display\" style=\"padding-bottom: 35px;\">";
        echo Lang::get("txt_desc_finding_help");
        echo "</div>
            <a class=\"submit d-display\" href=\"http://www.alz.co.uk/associations\" target=\"_blank\">";
        // line 286
        echo Lang::get("txt_button_find_your_national");
        echo "</a>
            <div class=\"left m-display custom-10-padd\">
                <p>";
        // line 288
        echo Lang::get("txt_desc_finding_help2");
        echo "<p>
                <p>";
        // line 289
        echo Lang::get("txt_desc_finding_help3");
        echo "</p>
                <p class=\"text-center\"> +++ </p>
                <p>";
        // line 291
        echo Lang::get("txt_desc_finding_help4");
        echo " </p>
                ";
        // line 293
        echo "                <a class=\"submit m-display\" href=\"http://www.alz.co.uk/associations\" target=\"_blank\">";
        echo Lang::get("txt_button_find_your_national");
        echo "</a>
            </div>
        </div>
    </div>
";
    }

    // line 299
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 300
        echo "    ";
        echo Asset::js("jquery-1.11.2.min.js");
        echo "
    ";
        // line 301
        echo Asset::js("canvas-menu.js");
        echo "
    ";
        // line 302
        echo Asset::js("bootstrap.min.js");
        echo "
    <script type=\"text/javascript\">
        \$('.carousel').carousel({
//        interval: 1000 * 10
            interval:false
        })
    </script>
";
    }

    public function getTemplateName()
    {
        return "about_dementia.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  551 => 302,  547 => 301,  542 => 300,  539 => 299,  529 => 293,  525 => 291,  520 => 289,  516 => 288,  511 => 286,  506 => 285,  502 => 283,  479 => 263,  474 => 261,  470 => 260,  461 => 254,  456 => 252,  452 => 251,  443 => 245,  438 => 243,  434 => 242,  425 => 236,  420 => 234,  416 => 233,  407 => 227,  402 => 225,  398 => 224,  389 => 218,  384 => 216,  380 => 215,  371 => 209,  366 => 207,  362 => 206,  353 => 200,  348 => 198,  344 => 197,  335 => 191,  330 => 189,  326 => 188,  317 => 182,  312 => 180,  308 => 179,  296 => 170,  284 => 161,  279 => 159,  270 => 153,  259 => 145,  254 => 143,  245 => 137,  234 => 129,  229 => 127,  220 => 121,  209 => 113,  204 => 111,  195 => 105,  184 => 97,  179 => 95,  169 => 88,  157 => 79,  152 => 77,  143 => 71,  130 => 61,  125 => 59,  113 => 50,  109 => 49,  98 => 41,  94 => 40,  83 => 32,  79 => 31,  68 => 23,  64 => 22,  53 => 14,  49 => 13,  38 => 6,  35 => 5,  30 => 3,);
    }
}
