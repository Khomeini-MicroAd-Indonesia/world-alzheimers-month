<?php

/* experience_three.twig */
class __TwigTemplate_50fa18451f7da7b0e0f561a6e851b22b43af7f3697cef7c180a0f6979b93028c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_header($context, array $blocks = array())
    {
    }

    // line 5
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 6
        echo "<h3 class=\"main-title\">Do you remember me?</h3>
<div class=\"row\">
    <div class=\"col-md-6\">
        <div class=\"m-display custom-5-padd\">
            <h3>Instructions</h3>
            <ul style=\"list-style: outside none disc;\">
                <li>Press start to begin the game.</li>
                <li>Name the faces from left to right.</li>
                <li>You must guess all of their names in 2 minutes.</li>
                <li>The difficulties will increase on each photo.</li>
            </ul>
        </div>
        <div class=\"frame-exp d-display\">
            <img class=\"in-frame\" src=\"";
        // line 19
        echo (isset($context["return"]) ? $context["return"] : null);
        echo "\">
        </div>
        <div class=\"frame-exp m-display center\">
            <img class=\"in-frame\" src=\"./assets/img/test-1.jpg\">
        </div>
    </div>
    <div class=\"col-md-6\">
        <div class=\" d-display\">
            <h3>Instructions</h3>
            <ul>
                <li>Press start to begin the game.</li>
                <li>Name the faces from left to right.</li>
                <li>You must guess all of their names in 2 minutes.</li>
                <li>The difficulties will increase on each photo.</li>
            </ul>
        </div>

        <form>
            <p>Time</p>
            <div>
                <span class=\"time-counter\" id=\"time\"></span><span class=\"font-12\">  Minutes</span>
                ";
        // line 43
        echo "            </div>
            <p>Your friend's name : </p>
            <input type=\"text\" class=\"span4 basicTypeahead\" placeholder=\"Your friend's name\" autocomplete=\"off\" data-provide=\"typeahead\"/>
            <a href=\"javascript:\">
                <div class=\"button-submit\"></div>
            </a>
        </form>
    </div>
</div>
<div class=\"row m-display custom-5-padd\">


</div>
";
    }

    // line 58
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 59
        echo "    ";
        echo Asset::js("jquery-1.11.2.min.js");
        echo "
    ";
        // line 60
        echo Asset::js("canvas-menu.js");
        echo "
    <script type=\"text/javascript\">
        function startTimer(duration, display) {
            var start = Date.now(),
                    diff,
                    minutes,
                    seconds;
            function timer() {
                // get the number of seconds that have elapsed since
                // startTimer() was called
                diff = duration - (((Date.now() - start) / 1000) | 0);

                // does the same job as parseInt truncates the float
                minutes = (diff / 60) | 0;
                seconds = (diff % 60) | 0;

                minutes = minutes < 10 ? \"0\" + minutes : minutes;
                seconds = seconds < 10 ? \"0\" + seconds : seconds;

                display.textContent = minutes + \":\" + seconds;

                if (diff <= 0) {
                    // add one second so that the count down starts at the full duration
                    // example 05:00 not 04:59
                    start = Date.now() + 1000;
                }
            };
            // we don't want to wait a full second before the timer starts
            timer();
            setInterval(timer, 1000);
        }

        window.onload = function () {
            var fiveMinutes = 60 * 2,
                    display = document.querySelector('#time');
            startTimer(fiveMinutes, display);
        };
    </script>
";
    }

    public function getTemplateName()
    {
        return "experience_three.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 60,  97 => 59,  94 => 58,  77 => 43,  53 => 19,  38 => 6,  35 => 5,  30 => 3,);
    }
}
