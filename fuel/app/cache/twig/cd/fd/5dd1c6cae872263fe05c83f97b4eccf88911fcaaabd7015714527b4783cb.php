<?php

/* my_profile.twig */
class __TwigTemplate_cdfd5dd1c6cae872263fe05c83f97b4eccf88911fcaaabd7015714527b4783cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 4
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 7
        echo "\t\tDashboard
\t\t<small>My Profile</small>
\t</h1>
\t";
        // line 11
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 12
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li>Dashboard</li>
\t\t<li class=\"active\">My Profile</li>
\t</ol>
</section>
";
    }

    // line 19
    public function block_backend_content($context, array $blocks = array())
    {
        // line 20
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 21
            echo "<div class=\"alert alert-success alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 23
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
</div>
";
        }
        // line 26
        echo "
";
        // line 27
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 28
            echo "<div class=\"alert alert-danger alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 30
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
</div>
";
        }
        // line 33
        echo "
<form name=\"frm_my_profile\" role=\"form\" action=\"\" method=\"post\" class=\"form-horizontal\" accept-charset=\"utf-8\" enctype=\"multipart/form-data\">
\t<div class=\"form-group\">
\t\t<label class=\"col-sm-2 control-label\" for=\"form_fullname\">Fullname</label>
\t\t<div class=\"col-sm-10\">
\t\t\t<input type=\"text\" id=\"form_fullname\" value=\"";
        // line 38
        echo (isset($context["current_admin_fullname"]) ? $context["current_admin_fullname"] : null);
        echo "\" name=\"txtfullname\" required placeholder=\"Fullname\" class=\"form-control\" />
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label class=\"col-sm-2 control-label\" for=\"form_phone\">Phone</label>
\t\t<div class=\"col-sm-10\">
\t\t\t<input type=\"text\" id=\"form_phone\" value=\"";
        // line 44
        echo (isset($context["current_admin_phone"]) ? $context["current_admin_phone"] : null);
        echo "\" name=\"txtphone\" placeholder=\"08XXXXXXXXXX\" class=\"form-control /\">
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label class=\"col-sm-2 control-label\" for=\"form_photo\">Photo (Max. 90 x 90)</label>
\t\t<div class=\"col-sm-10\">
\t\t\t<input type=\"file\" id=\"form_photo\" value=\"\" name=\"txtphoto\" style=\"margin: 5px 0px;\" />
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<div class=\"col-sm-offset-2 col-sm-10\">
\t\t\t<button type=\"submit\" class=\"btn btn-primary\">Save</button>
\t\t</div>
\t</div>
</form>
";
    }

    public function getTemplateName()
    {
        return "my_profile.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 44,  91 => 38,  84 => 33,  78 => 30,  74 => 28,  72 => 27,  69 => 26,  63 => 23,  59 => 21,  57 => 20,  54 => 19,  44 => 12,  41 => 11,  36 => 7,  32 => 4,  29 => 3,);
    }
}
