<?php

/* basic.twig */
class __TwigTemplate_bbbdfd5bdc1be17004cbb334a0de06738f0e31699e43fab58f803b87523d1b0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_css($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("backend_css", $context, $blocks);
        echo "
";
        // line 5
        echo Asset::css("image-picker.css");
        echo "
";
        // line 6
        echo Asset::css("bootstrap-select.min.css");
        echo "
";
    }

    // line 9
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 10
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 13
        echo "\t\t";
        echo (isset($context["content_header"]) ? $context["content_header"] : null);
        echo "
\t\t<small>";
        // line 14
        echo (isset($context["content_subheader"]) ? $context["content_subheader"] : null);
        echo "</small>
\t</h1>
\t";
        // line 17
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 18
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 20
            echo "\t\t<li class=\"";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) ? ("active") : (""));
            echo "\">
\t\t\t";
            // line 21
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "link")) > 0)) {
                echo "<a href=\"";
                echo $this->getAttribute((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "link");
                echo "\">";
            }
            // line 22
            echo "\t\t\t";
            echo $this->getAttribute((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "label");
            echo "
\t\t\t";
            // line 23
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "link")) > 0)) {
                echo "</a>";
            }
            // line 24
            echo "\t\t</li>
\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "\t</ol>
</section>
";
    }

    // line 30
    public function block_backend_content($context, array $blocks = array())
    {
        // line 31
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 32
            echo "<div class=\"alert alert-success alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 34
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
</div>
";
        }
        // line 37
        echo "
";
        // line 38
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 39
            echo "<div class=\"alert alert-danger alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 41
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
</div>
";
        }
        // line 44
        echo "
";
        // line 45
        echo Form::open($this->getAttribute((isset($context["form_data"]) ? $context["form_data"] : null), "attributes"), $this->getAttribute((isset($context["form_data"]) ? $context["form_data"] : null), "hidden"));
        echo "

";
        // line 47
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form_data"]) ? $context["form_data"] : null), "fieldset"));
        foreach ($context['_seq'] as $context["_key"] => $context["fielditem"]) {
            // line 48
            echo Helper_Form::horizontal_form_input((isset($context["fielditem"]) ? $context["fielditem"] : null));
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fielditem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "
<div class=\"form-group\">
\t<div class=\"col-sm-offset-2 col-sm-10\">
\t\t<button type=\"submit\" class=\"btn btn-primary\">Save</button>
\t\t<a href=\"";
        // line 54
        echo (isset($context["cancel_button_link"]) ? $context["cancel_button_link"] : null);
        echo "\">
\t\t\t<button type=\"button\" class=\"btn btn-default\">Cancel</button>
\t\t</a>
\t</div>
</div>

";
        // line 60
        echo Form::close();
        echo "
";
    }

    // line 63
    public function block_backend_js($context, array $blocks = array())
    {
        // line 64
        $this->displayParentBlock("backend_js", $context, $blocks);
        echo "
";
        // line 65
        echo Asset::add_path("assets/ckeditor/", "js");
        echo "
";
        // line 66
        echo Asset::js("ckeditor.js");
        echo "
";
        // line 67
        echo Asset::js("adapters/jquery.js");
        echo "
";
        // line 68
        echo Asset::js("plugins/input-mask/jquery.inputmask.js");
        echo "
";
        // line 69
        echo Asset::js("plugins/input-mask/jquery.inputmask.date.extensions.js");
        echo "
";
        // line 70
        echo Asset::js("plugins/input-mask/jquery.inputmask.extensions.js");
        echo "
";
        // line 71
        echo Asset::js("image-picker.min.js");
        echo "
";
        // line 72
        echo Asset::js("bootstrap-select.min.js");
        echo "
<script type=\"text/javascript\">
\t\$('textarea.ckeditor').ckeditor();
    \$('input.mask-date').inputmask(\"yyyy-mm-dd\", {\"placeholder\": \"yyyy-mm-dd\"});
\t\$('select.image-picker').imagepicker({
\t\thide_select : true,
\t\tshow_label  : true
\t});
\t\$('select.bootstrap-select').selectpicker();
</script>
";
    }

    public function getTemplateName()
    {
        return "basic.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 72,  232 => 71,  228 => 70,  224 => 69,  220 => 68,  216 => 67,  212 => 66,  208 => 65,  204 => 64,  201 => 63,  195 => 60,  186 => 54,  180 => 50,  172 => 48,  168 => 47,  163 => 45,  160 => 44,  154 => 41,  150 => 39,  148 => 38,  145 => 37,  139 => 34,  135 => 32,  133 => 31,  130 => 30,  124 => 26,  109 => 24,  105 => 23,  100 => 22,  94 => 21,  89 => 20,  72 => 19,  68 => 18,  65 => 17,  60 => 14,  55 => 13,  51 => 10,  48 => 9,  42 => 6,  38 => 5,  34 => 4,  31 => 3,);
    }
}
