<?php

/* experience_two.twig */
class __TwigTemplate_2b7a3ead10fb4a025f8e0251ca75edc1ba3d6cd3d73b10b6b67b9c7f3658de7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_header($context, array $blocks = array())
    {
    }

    // line 5
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 6
        echo "<h3 class=\"main-title\">Do you remember me?</h3>
<div class=\"row d-display\">
    <div class=\"col-md-6\">
        <img class=\"img-responsive\" src=\"./assets/img/exp-two-frame.png\">
    </div>
    <div class=\"col-md-6\">
        <h3>Instructions</h3>
        <ul>
            <li>Press start to begin the game.</li>
            <li>Name the faces from left to right.</li>
            <li>You must guess all of their names in 2 minutes.</li>
            <li>The difficulties will increase on each photo.</li>
        </ul>
        <p>Time</p>
        <div class=\"time-counter\">
            <h1>02:00</h1><span>Minutes</span>
        </div>
        <p>Type your friend's name here : </p>
        <img src=\"./assets/img/exp-two-ex-input.png\">
        <a href=\"";
        // line 25
        echo Uri::base();
        echo "do-you-remember-me/step-3\">
        <div class=\"button-start\"></div>
        </a>
    </div>
</div>
<div class=\"row m-display custom-5-padd\">
    <h3>Instructions</h3>
    <ul style=\"list-style: outside none disc;\">
        <li>Press start to begin the game.</li>
        <li>Name the faces from left to right.</li>
        <li>You must guess all of their names in 2 minutes.</li>
        <li>The difficulties will increase on each photo.</li>
    </ul>
   <img class=\"img-responsive\" src=\"./assets/img/mobile/exp-two-frame.png\">
   <p class=\"child\">Time<span class=\"time-counter\">02:00</span></p>
            <p>Type your friend's name here : </p>
            <img src=\"./assets/img/exp-two-ex-input.png\" class=\"img-responsive\">
            <a href=\"";
        // line 42
        echo Uri::base();
        echo "do-you-remember-me/step-3\">
                <div class=\"button-start custom-v-padd\"></div>
            </a>
    </div>
";
    }

    // line 48
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 49
        echo "    ";
        echo Asset::js("jquery-1.11.2.min.js");
        echo "
    ";
        // line 50
        echo Asset::js("canvas-menu.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "experience_two.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 50,  91 => 49,  88 => 48,  79 => 42,  59 => 25,  38 => 6,  35 => 5,  30 => 3,);
    }
}
