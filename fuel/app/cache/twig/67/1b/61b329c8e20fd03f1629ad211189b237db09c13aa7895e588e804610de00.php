<?php

/* pages/views/template_frontend.twig */
class __TwigTemplate_671b61b329c8e20fd03f1629ad211189b237db09c13aa7895e588e804610de00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fb_head_prefix' => array($this, 'block_fb_head_prefix'),
            'fb_meta_data' => array($this, 'block_fb_meta_data'),
            'frontend_css' => array($this, 'block_frontend_css'),
            'fb_js_sdk' => array($this, 'block_fb_js_sdk'),
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"no-js\" lang=\"en\">
<head ";
        // line 3
        $this->displayBlock('fb_head_prefix', $context, $blocks);
        echo ">
    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1, maximum-scale=1,user-scalable=no\">
    <base href=\"";
        // line 5
        echo Uri::base();
        echo "\" />

    ";
        // line 7
        $this->displayBlock('fb_meta_data', $context, $blocks);
        // line 15
        echo "
    <meta charset=\"utf-8\" />

    <title>";
        // line 18
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <link rel=\"icon\" type=\"image/ico\" href=\"";
        // line 19
        echo Uri::base();
        echo "assets/img/icon.png\" />
    <meta name=\"description\" content=\"";
        // line 20
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    
    ";
        // line 22
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 28
        echo "    <script type=\"text/javascript\" src=\"http://fast.fonts.net/jsapi/53758671-72ec-4086-987f-5d3e98d95378.js\"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 35
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
    ";
        // line 40
        $this->displayBlock('fb_js_sdk', $context, $blocks);
        // line 41
        echo "    <div class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\" id=\"slide-nav\" style=\"border-bottom:1px solid #D2D2D2\">
        <div class=\"row logo\" style=\"border-bottom:1px solid #D2D2D2\">
            <div class=\"container\">
                <div class=\"row logo-mobile\">
                    <div class=\"col-xs-6 hidden-xs .col-sm-5cols\">
                        <a href=\"";
        // line 46
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "\"><img src=\"./assets/img/logo-wam-3.png\" style=\"background-color: #ffffff\" alt=\"logo world alzheimer's month\"></a>
                    </div>
                    <div class=\"col-xs-6 hidden-xs .col-sm-5cols\">
                        <a href=\"http://www.alz.co.uk\" target=\"_blank\"><img class=\"right\" src=\"./assets/img/logo-adi.png\" alt=\"logo ADI\"></a>
                    </div>
                    <div class=\"col-xs-6 visible-xs .col-sm-5cols\">
                        <a href=\"";
        // line 52
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "\"><img src=\"./assets/img/mobile/logo-wam.png\" style=\"background-color: #ffffff\" alt=\"logo world alzheimer's month\"></a>
                    </div>
                    <div class=\"col-xs-6 visible-xs .col-sm-5cols\">
                        <a href=\"http://www.alz.co.uk\" target=\"_blank\"><img class=\"right\" src=\"./assets/img/mobile/logo-az.png\" alt=\"logo ADI\"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"container\">
            <div class=\"navbar-header\">
                <a class=\"navbar-toggle\">
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </a>
            </div>
            <div id=\"slidemenu\">
                <ul class=\"nav navbar-nav nav-main\">
                    ";
        // line 70
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["frontend_menus"]) ? $context["frontend_menus"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
            // line 71
            echo "                        <li><a href=\"";
            echo $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "route");
            echo "\">";
            echo Lang::get($this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "lang_code"));
            echo "</a> </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "                    
                </ul>
                <ul class=\"nav navbar-nav lang\">
                    ";
        // line 76
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["lang_item"]) {
            // line 77
            echo "                        <li><a href=\"";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
            echo "\"><span ";
            echo (($this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "active")) ? ("class=\"active\"") : (""));
            echo ">";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "label");
            echo "</span></a></li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lang_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "                </ul>
            </div>
        </div>
    </div>
    ";
        // line 83
        $this->displayBlock('frontend_header', $context, $blocks);
        // line 84
        echo "
    <div class=\"container\">
        ";
        // line 86
        $this->displayBlock('frontend_content', $context, $blocks);
        // line 87
        echo "    </div> <!-- /container -->
    <div class=\"footer d-display\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-9\">
                    ";
        // line 93
        echo "                        <p class=\"font-small line-xheight\">";
        echo Lang::get("txt_footnote_campaign");
        echo "</p>
                    ";
        // line 95
        echo "                    ";
        // line 96
        echo "                        <p class=\"font-bold line-xheight\">ALZHEIMER'S DISEASE INTERNATIONAL (ADI)</p>
                        <p class=\"font-small line-xheight\">";
        // line 97
        echo Lang::get("txt_footnote_supported");
        echo "</p>
                        <a href=\"http://www.nutricia.com/\" target=\"_blank\"><img src=\"./assets/img/nutricia.png\" style=\"width: 218px\" alt=\"logo Nutricia\"></a>
                    ";
        // line 100
        echo "                    ";
        // line 101
        echo "                        <ul class=\"social-media xline-height\">
                            <li ><a class=\"facebook\" href=\"https://www.facebook.com/alzheimersdiseaseinternational\" target=\"_blank\"></a></li>
                            <li ><a class=\"twitter\" href=\"https://twitter.com/alzdisint\" target=\"_blank\"></a></li>
                            <li ><a class=\"youtube\" href=\"https://www.youtube.com/user/alzdisint\" target=\"_blank\"></a></li>
                        </ul>
                    ";
        // line 107
        echo "                    ";
        // line 108
        echo "                        <p class=\"font-small\">";
        echo Lang::get("txt_footnote_copyright");
        echo " &nbsp;| &nbsp;<a href=\"";
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/privacy-policy\">";
        echo Lang::get("txt_footnote_privacy");
        echo "</a>&nbsp;";
        echo "</p>

                    ";
        // line 111
        echo "                </div>
                <div class=\"col-md-3\">
                    <p class=\"font-medium\">";
        // line 113
        echo Lang::get("txt_footnote_contact");
        echo "</p>
                    <p class=\"font-small\">Alzheimer's Disease International <br/> 64 Great Suffolk Street <br/>London <br/>SE1 0BL<br/>UK</p>
                    <p class=\"font-small\">Tel: +44 20 79810880 <br/>Fax: +44 20 79282357</p>
                    <p class=\"font-small\">Email: <a href=\"mailto:info@alz.co.uk\">info@alz.co.uk</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"footer m-display\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-xs-12\">
                    <div class=\"col-md-12\">
                        <p class=\"font-small line-xheight\">";
        // line 126
        echo Lang::get("txt_footnote_campaign");
        echo "</p>
                    </div>
                    <div class=\"col-md-12\">
                        <p class=\"font-bold line-xheight\">ALZHEIMER'S DISEASE INTERNATIONAL (ADI)</p>
                        <p class=\"font-small line-xheight\">";
        // line 130
        echo Lang::get("txt_footnote_supported");
        echo "</p>
                        <a href=\"http://www.nutricia.com/\" target=\"_blank\"><img src=\"./assets/img/nutricia.png\" style=\"width: 218px\" alt=\"logo Nutricia\"></a>
                    </div>
                </div>
                </div>
            <div class=\"row\">
                <div class=\"custom-padd\">
                    <div class=\"col-xs-12\">
                        <p class=\"font-medium\">";
        // line 138
        echo Lang::get("txt_footnote_contact");
        echo "</p>
                        <p class=\"font-small\">Alzheimer's Disease International <br/> 64 Great Sulfolk Street <br/>London <br/>SE1 0BL<br/>UK</p>
                        <p class=\"font-small\">Tel: +44 20 79810880 <br/>Fax: +44 20 79282357</p>
                        <p class=\"font-small\">Email: info@alz.co.uk</p>
                    </div>
                    <div class=\"col-xs-12\">
                        <ul class=\"social-media xline-height\">
                            <li class=\"facebook\"><a href=\"https://www.facebook.com/alzheimersdiseaseinternational\"></a></li>
                            <li class=\"twitter\"><a href=\"https://twitter.com/alzdisint\"></a></li>
                            <li class=\"youtube\"><a href=\"https://www.youtube.com/user/alzdisint\"></a></li>
                        </ul>
                    </div>
                    <div class=\"col-md-12\">
                        <p class=\"font-small line-xxheight\">";
        // line 151
        echo Lang::get("txt_footnote_copyright");
        echo " | <a href=\"";
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "privacy\">";
        echo Lang::get("txt_footnote_privacy");
        echo "</a>
                            ";
        // line 153
        echo "                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    ";
        // line 161
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 169
        echo "</body>
</html>";
    }

    // line 3
    public function block_fb_head_prefix($context, array $blocks = array())
    {
    }

    // line 7
    public function block_fb_meta_data($context, array $blocks = array())
    {
        // line 8
        echo "        ";
        // line 9
        echo "        ";
        // line 10
        echo "        ";
        // line 11
        echo "        ";
        // line 12
        echo "        ";
        // line 13
        echo "        ";
        // line 14
        echo "    ";
    }

    // line 22
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 23
        echo "        ";
        echo Asset::css("normalize.css");
        echo "
        ";
        // line 24
        echo Asset::css("bootstrap.min.css");
        echo "
        ";
        // line 25
        echo Asset::css((("style-" . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . ".css"));
        echo "
        ";
        // line 26
        echo Asset::css("canvas-menu.css");
        echo "
    ";
    }

    // line 40
    public function block_fb_js_sdk($context, array $blocks = array())
    {
    }

    // line 83
    public function block_frontend_header($context, array $blocks = array())
    {
    }

    // line 86
    public function block_frontend_content($context, array $blocks = array())
    {
    }

    // line 161
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 162
        echo "        ";
        echo Asset::js("jquery.latest.min.js");
        echo "
        ";
        // line 163
        echo Asset::js("bootsrap.min.js");
        echo "
        ";
        // line 164
        echo Asset::js("canvas-menu.js");
        echo "
        ";
        // line 165
        echo Asset::js("common.js");
        echo "
        ";
        // line 166
        echo Asset::js("paintbrush.js");
        echo "
        ";
        // line 168
        echo "    ";
    }

    public function getTemplateName()
    {
        return "pages/views/template_frontend.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  379 => 168,  375 => 166,  371 => 165,  367 => 164,  363 => 163,  358 => 162,  355 => 161,  350 => 86,  345 => 83,  340 => 40,  334 => 26,  330 => 25,  326 => 24,  321 => 23,  318 => 22,  314 => 14,  312 => 13,  310 => 12,  308 => 11,  306 => 10,  304 => 9,  302 => 8,  299 => 7,  294 => 3,  289 => 169,  287 => 161,  277 => 153,  268 => 151,  252 => 138,  234 => 126,  218 => 113,  214 => 111,  202 => 108,  200 => 107,  186 => 97,  176 => 93,  169 => 87,  167 => 86,  161 => 83,  142 => 77,  138 => 76,  133 => 73,  122 => 71,  118 => 70,  97 => 52,  88 => 46,  81 => 41,  79 => 40,  71 => 35,  62 => 28,  60 => 22,  55 => 20,  51 => 19,  47 => 18,  42 => 15,  40 => 7,  35 => 5,  26 => 1,  265 => 139,  254 => 131,  250 => 130,  246 => 129,  241 => 130,  239 => 127,  236 => 126,  225 => 122,  209 => 108,  207 => 107,  205 => 106,  203 => 105,  201 => 104,  199 => 103,  197 => 102,  195 => 101,  193 => 101,  191 => 100,  189 => 98,  187 => 97,  185 => 96,  183 => 96,  181 => 95,  179 => 93,  163 => 84,  155 => 79,  151 => 76,  147 => 75,  132 => 63,  127 => 62,  124 => 61,  104 => 45,  100 => 44,  91 => 38,  87 => 37,  78 => 31,  74 => 30,  65 => 24,  61 => 23,  41 => 6,  37 => 5,  33 => 3,  30 => 3,);
    }
}
