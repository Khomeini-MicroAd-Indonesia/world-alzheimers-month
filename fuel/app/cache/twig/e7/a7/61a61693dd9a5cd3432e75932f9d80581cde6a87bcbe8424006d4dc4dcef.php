<?php

/* get_involved.twig */
class __TwigTemplate_e7a761a61693dd9a5cd3432e75932f9d80581cde6a87bcbe8424006d4dc4dcef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_header($context, array $blocks = array())
    {
    }

    // line 5
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 6
        echo "    <div class=\"parent\">
        <div class=\"child\">
            <h3 class=\"main-title\">";
        // line 8
        echo Lang::get("txt_title_spread_word");
        echo "</h3>
            <div class=\"d-display\">
            <p class=\"d-display\">";
        // line 10
        echo Lang::get("txt_desc_spread_word_1");
        echo "</p>
        </div>
            <div class=\"m-display\">
            <p class=\"custom-10-padd\">";
        // line 13
        echo Lang::get("txt_desc_spread_word_1a");
        echo "</p>
            <p class=\"text-center\">+++</p>
            <p class=\"custom-10-padd\">";
        // line 15
        echo Lang::get("txt_desc_spread_word_1b");
        echo "</p>
        </div>
            <div class=\"row d-display\">
            <div class=\"col-md-3 col-xs-6\">
                <div class=\"ad\">
                    <div data-name=\"fb-profile-pic\" class=\"ad-image\">
                        ";
        // line 22
        echo "                        ";
        // line 23
        echo "                            ";
        // line 24
        echo "                        ";
        // line 25
        echo "                        ";
        // line 26
        echo "                        ";
        // line 27
        echo "                        <a href=\"";
        echo Uri::base();
        echo "assets/img/media/download-";
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/WAM-2015-Poster-Web.pdf\" download=\"Poster.pdf\">
                            <img src=\"./assets/img/media/poster-thumb.png\" alt=\"Poster WAM 2015\">
                        </a>
                    </div>
                    <div class=\"ad-title\">
                        <p>Poster <br/>(A3, PDF)</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-3 col-xs-6\">
                <div class=\"ad\">
                    <div id=\"socmed-poster\" class=\"ad-image\">
                        ";
        // line 40
        echo "                        ";
        // line 41
        echo "                            ";
        // line 42
        echo "                        ";
        // line 43
        echo "                        ";
        // line 44
        echo "                        <a href=\"";
        echo Uri::base();
        echo "assets/img/media/download-";
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/WAM-2015-Bulletin-Web.pdf\" download=\"Bulletin.pdf\">
                            <img src=\"./assets/img/media/bulletin-thumb.png\" class=\"img-responsive\" alt=\"Bulletin WAM 2015\">
                        </a>
                    </div>
                    <div class=\"ad-title\">
                        <p>Bulletin <br/>(A4, PDF)</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-3 col-xs-6\">
                <div class=\"ad\">
                    <div id=\"print-ad\" class=\"ad-image\">
                        ";
        // line 57
        echo "                        ";
        // line 58
        echo "                            ";
        // line 59
        echo "                        ";
        // line 60
        echo "                        ";
        // line 61
        echo "                        <a href=\"";
        echo Uri::base();
        echo "assets/img/media/download-";
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/Facebook.png\" download=\"Facebook cover photo.png\">
                            <img src=\"./assets/img/media/facebook-thumb.png\" class=\"img-responsive\">
                        </a>
                    </div>
                    <div class=\"ad-title\">
                        <p>Facebook cover photo<br/>(PNG)</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-3 col-xs-6\">
                <div class=\"ad\">
                    <div id=\"print-ad-typo\" class=\"ad-image\">
                        ";
        // line 74
        echo "                        ";
        // line 75
        echo "                            ";
        // line 76
        echo "                        ";
        // line 77
        echo "                        ";
        // line 78
        echo "                        <a href=\"";
        echo Uri::base();
        echo "assets/img/media/download-";
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/Twitter.png\" download=\"Twitter cover photo.png\">
                            <img src=\"./assets/img/media/twitter-thumb.png\">
                        </a>
                    </div>
                    <div class=\"ad-title\">
                        <p>Twitter cover photo <br/>(PNG)</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-3 col-xs-6\">
                <div class=\"ad\">
                    <div id=\"fb-cover\" class=\"ad-image\">
                        ";
        // line 91
        echo "                        ";
        // line 92
        echo "                            ";
        // line 93
        echo "                        ";
        // line 94
        echo "                        ";
        // line 95
        echo "                        <a href=\"";
        echo Uri::base();
        echo "assets/img/media/download-";
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/WAM-2015-Letter-Template.doc\" download=\"Letter template.doc\">
                            <img src=\"./assets/img/media/letter-thumb.png\">
                        </a>
                    </div>
                    <div class=\"ad-title\">
                        <p>Letter template<br/>(.DOC)</p>
                    </div>
                </div>
            </div>
        </div>
            <div class=\"row m-display\">
            <div class=\"col-xs-1 left-slide\"><a class=\"back-btn off\"><img src=\"./assets/img/row-left.png\"></a></div>
            <div class=\"col-xs-8 vote-result first selectedDiv\">
                <div class=\"ad\">
                    <div data-name=\"fb-profile-pic\" class=\"ad-image\">
                        ";
        // line 111
        echo "                            ";
        // line 112
        echo "                                ";
        // line 113
        echo "                            ";
        // line 114
        echo "                        ";
        // line 115
        echo "                        <a href=\"./assets/img/media/WAM-English-Poster-2015-Web.pdf\" download=\"Poster.pdf\">
                            <img src=\"./assets/img/media/poster-thumb.png\">
                        </a>

                    </div>
                    <div class=\"ad-title\">
                        <p>Poster <br/>(A3, PDF)</p>
                    </div>
                </div>
            </div>
            <div class=\"col-xs-8 vote-result\">
                <div class=\"ad\">
                    <div id=\"socmed-poster\" class=\"ad-image\">
                        ";
        // line 129
        echo "                            ";
        // line 130
        echo "                                ";
        // line 131
        echo "                            ";
        // line 132
        echo "                        ";
        // line 133
        echo "                        <a href=\"./assets/img/media/WAM-2015-Bulletin-English-Web.pdf\" download=\"Bulletin.pdf\">
                            <img src=\"./assets/img/media/bulletin-thumb.png\" class=\"img-responsive\">
                        </a>
                    </div>
                    <div class=\"ad-title\">
                        <p>Bulletin <br/>(A4, PDF)</p>
                    </div>
                </div>
            </div>
            <div class=\"col-xs-8 vote-result\">
                <div class=\"ad\">
                    <div id=\"print-ad\" class=\"ad-image\">
                        ";
        // line 146
        echo "                            ";
        // line 147
        echo "                                ";
        // line 148
        echo "                            ";
        // line 149
        echo "                        ";
        // line 150
        echo "                        <a href=\"./assets/img/media/Facebook.png\" download=\"Facebook cover photo.png\">
                            <img src=\"./assets/img/media/facebook-thumb.png\">
                        </a>
                    </div>
                    <div class=\"ad-title\">
                        <p>Facebook cover photo<br/>(PNG)</p>
                    </div>
                </div>
            </div>
            <div class=\"col-xs-8 vote-result\">
                <div class=\"ad\">
                    <div id=\"print-ad-typo\" class=\"ad-image\">
                        ";
        // line 163
        echo "                            ";
        // line 164
        echo "                                ";
        // line 165
        echo "                            ";
        // line 166
        echo "                        ";
        // line 167
        echo "                        <a href=\"./assets/img/media/Twitter.png\" download=\"Twitter cover photo.png\">
                            <img src=\"./assets/img/media/twitter-thumb.png\">
                        </a>
                    </div>
                    <div class=\"ad-title\">
                        <p>Twitter cover photo<br/>(PNG)</p>
                    </div>
                </div>
            </div>
            <div class=\"col-xs-8 vote-result last\">
                <div class=\"ad\">
                    <div id=\"fb-cover\" class=\"ad-image\">
                        ";
        // line 180
        echo "                            ";
        // line 181
        echo "                                ";
        // line 182
        echo "                            ";
        // line 183
        echo "                        ";
        // line 184
        echo "                        <a href=\"./assets/img/media/World-Alzheimer-Month-Letter-Template.doc\" download=\"Letter template.doc\">
                            <img src=\"./assets/img/media/letter-thumb.png\">
                        </a>
                    </div>
                    <div class=\"ad-title\">
                        <p>Letter template<br/>(.DOC)</p>
                    </div>
                </div>
            </div>
            <div class=\"col-xs-1 right-slide\"><a class=\"next-btn\"><img src=\"./assets/img/row-right.png\"></a></div>
        </div>
        </div>
         
    </div>
        <a name=\"find-an-event\"></a>              
        <div class=\"channel delay\">

        <div class=\"channel delay custom-10-padd\">
        <div class=\"box css3-shadow\"></div>
    
       
        <h3 class=\"main-title\">";
        // line 205
        echo Lang::get("txt_title_find_event");
        echo "</h3>
        
        <div class=\"row show-event\" style=\"margin-top: -80px; margin-bottom: -80px;\">
            ";
        // line 208
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["main_event"]) ? $context["main_event"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
            // line 209
            echo "            <div class=\"col-md-6 event\">
                <h3 class=\"sub-title text-orange\">";
            // line 210
            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "name");
            echo "</h3>
                ";
            // line 211
            echo "<span class=\"text-orange\">";
            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "event_date");
            echo "</span>
                <hr/>
                <img class=\"event-gallery img-responsive\" src=\"";
            // line 213
            echo Uri::base();
            echo "media/eventimage/";
            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "image");
            echo "\"><br/>
                <p class=\"content\">";
            // line 214
            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "content");
            echo "</p>
                <p class=\"address\">
                <b>Tel:</b> <span>";
            // line 216
            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "phone");
            echo "</span><br/>
                <b>Email:</b> <span><a href=\"mailto:info@alz.co.uk?Subject=Enquiry\">";
            // line 217
            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "email");
            echo "</a></span><br/>
                <b>Web:</b> <span class=\"text-orange\"><a href=\"http://www.alz.co.uk\">";
            // line 218
            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "web");
            echo "</a></span>
                </p><br/>
            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 222
        echo "
        </div>
        <h3 class=\"sub-title\">";
        // line 224
        echo Lang::get("txt_subtitle_find_event");
        echo "</h3>
        
        <p class=\"newspaper left\">";
        // line 226
        echo Lang::get("txt_desc_find_event");
        echo " <a href=\"http://www.alz.co.uk/associations\" target=\"_blank\">";
        echo Lang::get("txt_here");
        echo "</a>. ";
        echo Lang::get("txt_desc_find_event_2");
        echo "</p>
        
        
        
        
        <br/>
        <form >
            <label>
                <select class=\"form-control region\" id=\"region\" onchange=\"showCountry(this.value);\">
                    <option value=\"\">";
        // line 235
        echo Lang::get("txt_region");
        echo "</option>
                    ";
        // line 236
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["region_data"]) ? $context["region_data"] : null));
        foreach ($context['_seq'] as $context["id"] => $context["name"]) {
            // line 237
            echo "                        <option value=\"";
            echo (isset($context["id"]) ? $context["id"] : null);
            echo "\">";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "</option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['name'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 239
        echo "                </select>
            </label>
            <br/>
            <label>
                <select class=\"form-control country\" id=\"country\" onchange=\"showCity(this.value);\" disabled>
                    <option value=\"\">";
        // line 244
        echo Lang::get("txt_country");
        echo "</option>
                </select>
            </label>
            <br/>
            <label>
                <select class=\"form-control city\" id=\"city\" onchange=\"showEvent(this.value);\" disabled>
                    <option value=\"\">";
        // line 250
        echo Lang::get("txt_city");
        echo "</option>
                </select>
            </label>
        </form>

        <div id=\"event-row\" class=\"row show-event\" style=\"display: none;\">
            
        </div>
                
               
        <div class=\"text-center\">

            <a href=\"";
        // line 262
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/add-event\" class=\"submit\">";
        echo Lang::get("txt_add_event");
        echo "</a>
        </div>
        <div class=\"modal fade\" id=\"modal-addevent\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modal-addevent\" aria-hidden=\"true\">
            <div class=\"modal-dialog\">
                <div class=\"modal-content\" id=\"modal-content\">
                    <div id=\"form-content\" class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                        <h4 class=\"modal-title\">";
        // line 269
        echo Lang::get("txt_add_event");
        echo "</h4>
                    </div>
                    ";
        // line 311
        echo "                                <input id=\"submit\" type=\"button\" class=\"submit\" value=\"Submit\" onclick=\"submitEvent()\">
                            </div>
                    </form>
                    </div>
                </div>
            </div>

    </div>
    <div class=\"parent\">
        <div class=\"child\">
            <div class=\"box css3-shadow\"></div>
            <h3 class=\"main-title\">";
        // line 322
        echo Lang::get("txt_title_support_us");
        echo "</h3>
            <div>
                <p class=\"sub-support-title\">";
        // line 324
        echo Lang::get("txt_desc_support_us");
        echo "</p>
                <p class=\"sub-support-title\"> ";
        // line 325
        echo Lang::get("txt_desc_support_us_2");
        echo "</p>
                <h3 class=\"sub-support-title\">";
        // line 326
        echo Lang::get("txt_desc_support_3");
        echo "</h3>
                <button onClick=\"window.open('http://www.alz.co.uk/donate');\" class=\"submit\">";
        // line 327
        echo Lang::get("txt_donate");
        echo "</button>
            </div>
        </div>
    </div>
";
    }

    // line 333
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 334
        echo "
    ";
        // line 335
        echo Asset::js("jquery-1.11.1.min.js");
        echo "
    ";
        // line 336
        echo Asset::js("jquery.chained.min.js");
        echo "
    ";
        // line 337
        echo Asset::js("canvas-menu.js");
        echo "
    ";
        // line 338
        echo Asset::js("bootstrap.min.js");
        echo "
    
 <script type=\"text/javascript\" charset=\"utf-8\">
     \$(\".back-btn\").click(function(){debugger;
         var prevElement=\$('.selectedDiv').prev();
         prevElement.show();
         \$(\".selectedDiv\").hide();
         \$(\".selectedDiv\").removeClass(\"selectedDiv\");
         prevElement.addClass(\"selectedDiv\");

         if(\$('.first').css('display')==\"block\"){
             \$(\".back-btn\").addClass(\"off\");
         }
         else{
             \$(\".next-btn\").removeClass(\"off\");
         }
     });

     \$(\".next-btn\").click(function(){debugger;
         var nextElement= \$('.selectedDiv').next();
         nextElement.show();
         \$(\".selectedDiv\").hide();
         \$(\".selectedDiv\").removeClass(\"selectedDiv\");
         nextElement.addClass(\"selectedDiv\");
         if(\$('.last').css('display')==\"block\"){
             \$(\".next-btn\").addClass(\"off\");
         }
         else{
             \$(\".back-btn\").removeClass(\"off\");
         }
     });
     
function showCountry(str) {
    \$.ajax({ 
        type: 'POST', 
        url: \"";
        // line 373
        echo Uri::base();
        echo "get-country?id=\"+str, 
        data: {}, 
        success: function(data){ 
            
            var reset = '<option value=\"0\"> Please Select City</option>';
            var temp = '<option value=\"0\"> Please Select Country</option>';
            var json = JSON.parse(data);
            \$.each(json, function(i, item){
            
                //console.log(item.name);
                temp += '<option value=\"'+ item.id +'\">'+ item.name +'</option>';                    
            });
        
            document.getElementById(\"country\").disabled = false;
            \$('#country').html(temp);
            \$('#city').html(reset);
        
        } 
    }); 
   
}

function showCity(str) {
  
    \$.ajax({ 
        type: 'POST', 
        url: \"";
        // line 399
        echo Uri::base();
        echo "get-city?id=\"+str, 
        data: {}, 
        success: function(data){ 
        
            var temp = '<option value=\"0\"> Please Select </option>';
            var json = JSON.parse(data);
            \$.each(json, function(i, item){
    
                console.log(item.name);
                temp += '<option value=\"'+ item.id +'\">'+ item.name +'</option>';                    
            });
        
            document.getElementById(\"city\").disabled = false;
            \$('#city').html(temp);     
        } 
    }); 
   
}

function showEvent(str){
    
    \$.ajax({ 
        type: 'POST', 
        url: \"";
        // line 422
        echo Uri::base();
        echo "get-event?id=\"+str, 
        data: {}, 
        success: function(data){ 
        
            var temp = '';
            var json = JSON.parse(data);
            \$.each(json, function(i, item){
                console.log(item.name);
            temp += '<div class=\"col-md-6 event\">\\n\\
                     <h3 class=\"sub-title text-orange\">'+ item.name +'</h3>\\n\\
                     <p class=\"tanggal text-orange\">'+ item.event_date  +'</p>\\n\\
                     <hr/>\\n\\
                     <p class=\"meta-desc\">'+ item.meta_desc +'</p>\\n\\
                     <img class=\"event-gallery img-responsive\" src=\"";
        // line 435
        echo Uri::base();
        echo "media/eventimage/'+ item.image +'\"><br/><br/>\\n\\
                     <p class=\"address\">'+ item.content +'</p>\\n\\
                     <b>Tel:</b> <span> '+ item.phone +'</span><br/>\\n\\
                     <b>Email:</b> <a href=\"mailto:'+item.email+'\"><span> '+ item.email +'</span></a><br/>\\n\\
                     <b>Web:</b> <span class=\"text-orange\"> <a href=\"'+item.web+'\" target=\"_blank\"> '+ item.web +'</a></span></p><br/>\\n\\
                     <a class=\"more-info\" style=\"border: 1px solid #fcdddc;\" href=\"'+ item.web +'\">More Info</a>\\n\\
                     </div>';                    
            });
        
            document.getElementById(\"event-row\").style.display = \"block\";
            \$('#event-row').html(temp);     
        } 
    }); 
    
}

function previewImage(input) {
    var preview = document.getElementById('preview');
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            preview.setAttribute('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    } else {
        preview.setAttribute('src', 'placeholder.png');
    }
}


function submitEvent(){

    var eventname = document.getElementById('eventname').value;
    var email = document.getElementById('email').value;
    var desc = document.getElementById('desc').value;
    var date = document.getElementById('date').value;
    var time = document.getElementById('time').value;
    var country = document.getElementById('countryf').value;
    var city = document.getElementById('cityf').value;
    var location = document.getElementById('location').value;
    var organisation = document.getElementById('organisation').value;
    var weblink = document.getElementById('weblink').value;
    //var img = document.getElementById('upload');
    //var file = img.files[0];
     //alert(file);
    
    //var preview = document.getElementById(\"preview\");
    //preview.src = file.getAsDataURL();
    
    \$.ajax({
        type: \"POST\",
        url: \"";
        // line 486
        echo Uri::base();
        echo "get-submit-event\", // 
        data: { 
            'eventname':eventname,
            'email':email,
            'desc':desc,
            'date':date,
            'time':time,
            'country':country,
            'city':city,
            'location':location,
            'organisation':organisation,
            'weblink':weblink
            //'img': file
           
        },
        success: function(msg){
            \$(\"#thanks\").html(msg);
            \$(\"#modal-content\").modal('hide');\t
        }";
        // line 508
        echo "    });
}

</script>
 

";
    }

    public function getTemplateName()
    {
        return "get_involved.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  708 => 508,  687 => 486,  633 => 435,  617 => 422,  591 => 399,  562 => 373,  524 => 338,  520 => 337,  516 => 336,  512 => 335,  509 => 334,  506 => 333,  497 => 327,  493 => 326,  489 => 325,  485 => 324,  480 => 322,  467 => 311,  462 => 269,  449 => 262,  434 => 250,  425 => 244,  418 => 239,  407 => 237,  403 => 236,  399 => 235,  383 => 226,  378 => 224,  374 => 222,  364 => 218,  360 => 217,  356 => 216,  351 => 214,  345 => 213,  339 => 211,  335 => 210,  332 => 209,  328 => 208,  322 => 205,  299 => 184,  297 => 183,  295 => 182,  293 => 181,  291 => 180,  277 => 167,  275 => 166,  273 => 165,  271 => 164,  269 => 163,  255 => 150,  253 => 149,  251 => 148,  249 => 147,  247 => 146,  233 => 133,  231 => 132,  229 => 131,  227 => 130,  225 => 129,  210 => 115,  208 => 114,  206 => 113,  204 => 112,  202 => 111,  181 => 95,  179 => 94,  177 => 93,  175 => 92,  173 => 91,  155 => 78,  153 => 77,  151 => 76,  149 => 75,  147 => 74,  129 => 61,  127 => 60,  125 => 59,  123 => 58,  121 => 57,  103 => 44,  101 => 43,  99 => 42,  97 => 41,  95 => 40,  77 => 27,  75 => 26,  73 => 25,  71 => 24,  69 => 23,  67 => 22,  58 => 15,  53 => 13,  47 => 10,  42 => 8,  38 => 6,  35 => 5,  30 => 3,);
    }
}
