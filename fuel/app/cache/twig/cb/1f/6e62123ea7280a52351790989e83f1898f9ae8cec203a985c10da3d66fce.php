<?php

/* setting.twig */
class __TwigTemplate_cb1f6e62123ea7280a52351790989e83f1898f9ae8cec203a985c10da3d66fce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 4
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 7
        echo "\t\tFacebook Management
\t\t<small>Setting</small>
\t</h1>
\t";
        // line 11
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 12
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li>Facebook Management</li>
\t\t<li class=\"active\">Setting</li>
\t</ol>
</section>
";
    }

    // line 19
    public function block_backend_content($context, array $blocks = array())
    {
        // line 20
        echo "
";
        // line 21
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 22
            echo "<div class=\"alert alert-success alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 24
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
</div>
";
        }
        // line 27
        echo "
";
        // line 28
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 29
            echo "<div class=\"alert alert-danger alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 31
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
</div>
";
        }
        // line 34
        echo "
\t<form class=\"form-horizontal\" accept-charset=\"utf-8\" method=\"post\" action=\"\" role=\"form\" name=\"frm_basic_setting\">
\t\t";
        // line 36
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["setting_data"]) ? $context["setting_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["setting_item"]) {
            // line 37
            echo "\t\t<div class=\"form-group\">
\t\t\t<label for=\"form_";
            // line 38
            echo $this->getAttribute((isset($context["setting_item"]) ? $context["setting_item"] : null), "name");
            echo "\" class=\"col-sm-2 control-label\">";
            echo $this->getAttribute((isset($context["setting_item"]) ? $context["setting_item"] : null), "label");
            echo "</label>
\t\t\t<div class=\"col-sm-10\">
\t\t\t\t";
            // line 40
            $context["form_attr"] = array("class" => "form-control", "placeholder" => $this->getAttribute((isset($context["setting_item"]) ? $context["setting_item"] : null), "label"), "required" => "");
            // line 41
            echo "\t\t\t\t";
            echo Form::input($this->getAttribute((isset($context["setting_item"]) ? $context["setting_item"] : null), "name"), $this->getAttribute((isset($context["setting_item"]) ? $context["setting_item"] : null), "value"), (isset($context["form_attr"]) ? $context["form_attr"] : null));
            echo "
\t\t\t</div>
\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['setting_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "\t\t<div class=\"form-group\">
\t\t\t<div class=\"col-sm-offset-2 col-sm-10\">
\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\">Save</button>
\t\t\t</div>
\t\t</div>
\t</form>
";
    }

    public function getTemplateName()
    {
        return "setting.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 45,  107 => 41,  105 => 40,  98 => 38,  95 => 37,  91 => 36,  87 => 34,  81 => 31,  77 => 29,  75 => 28,  72 => 27,  66 => 24,  62 => 22,  60 => 21,  57 => 20,  54 => 19,  44 => 12,  41 => 11,  36 => 7,  32 => 4,  29 => 3,);
    }
}
