<?php

/* participant.twig */
class __TwigTemplate_9cdd04cb97f3da49aae1f2684bb877c0e62b31e843a5d8845f14cb621100af59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_css($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("backend_css", $context, $blocks);
        echo "
\t<!-- dataTables css -->
\t";
        // line 6
        echo Asset::css("datatables/dataTables.bootstrap.css");
        echo "
";
    }

    // line 9
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 10
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 13
        echo "\t\tFacebook Management
\t\t<small>Participant</small>
\t</h1>
\t";
        // line 17
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 18
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li>Facebook Management</li>
\t\t<li class=\"active\">Participant</li>
\t</ol>
</section>
";
    }

    // line 25
    public function block_backend_content($context, array $blocks = array())
    {
        // line 26
        echo "
";
        // line 27
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 28
            echo "<div class=\"alert alert-success alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 30
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
</div>
";
        }
        // line 33
        echo "
";
        // line 34
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 35
            echo "<div class=\"alert alert-danger alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 37
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
</div>
";
        }
        // line 40
        echo "
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<div class=\"box\">
\t\t\t\t<div class=\"box-body table-responsive\">
\t\t\t\t\t<table id=\"table-level\" class=\"table table-bordered table-striped\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>FB SUID</th>
\t\t\t\t\t\t\t<th>FullName</th>
\t\t\t\t\t\t\t<th>Link</th>
\t\t\t\t\t\t\t<th>Email</th>
\t\t\t\t\t\t\t<th>Gender</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 56
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["participant_list"]) ? $context["participant_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["participant"]) {
            // line 57
            echo "\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td>";
            // line 58
            echo $this->getAttribute((isset($context["participant"]) ? $context["participant"] : null), "fb_suid");
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 59
            echo $this->getAttribute((isset($context["participant"]) ? $context["participant"] : null), "fb_fullname");
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 60
            echo $this->getAttribute((isset($context["participant"]) ? $context["participant"] : null), "fb_link");
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 61
            echo $this->getAttribute((isset($context["participant"]) ? $context["participant"] : null), "fb_email");
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 62
            echo $this->getAttribute((isset($context["participant"]) ? $context["participant"] : null), "gender");
            echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participant'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div><!-- /.box-body -->
\t\t\t</div><!-- /.box -->
\t\t</div>
\t</div>
";
    }

    // line 73
    public function block_backend_js($context, array $blocks = array())
    {
        // line 74
        echo "\t";
        $this->displayParentBlock("backend_js", $context, $blocks);
        echo "
\t<!-- DATA TABES SCRIPT -->
\t";
        // line 76
        echo Asset::js("plugins/datatables/jquery.dataTables.js");
        echo "
\t";
        // line 77
        echo Asset::js("plugins/datatables/dataTables.bootstrap.js");
        echo "
\t<!-- page script -->
\t<script type=\"text/javascript\">
\t\t\$(function() {
\t\t\t\$(\"#table-level\").dataTable();
\t\t} );
\t</script>
";
    }

    public function getTemplateName()
    {
        return "participant.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 77,  173 => 76,  167 => 74,  164 => 73,  154 => 65,  145 => 62,  141 => 61,  137 => 60,  133 => 59,  129 => 58,  126 => 57,  122 => 56,  104 => 40,  98 => 37,  94 => 35,  92 => 34,  89 => 33,  83 => 30,  79 => 28,  77 => 27,  74 => 26,  71 => 25,  61 => 18,  58 => 17,  53 => 13,  49 => 10,  46 => 9,  40 => 6,  34 => 4,  31 => 3,);
    }
}
