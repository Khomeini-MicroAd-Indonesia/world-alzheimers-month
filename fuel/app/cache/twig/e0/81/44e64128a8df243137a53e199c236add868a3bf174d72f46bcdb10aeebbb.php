<?php

/* home.twig */
class __TwigTemplate_e08144e64128a8df243137a53e199c236add868a3bf174d72f46bcdb10aeebbb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_frontend_header($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"header m-display\">
        <div class=\"header-text\">
            <h3 class=\"main-title\">";
        // line 5
        echo Lang::get("txt_main_menu_banner");
        echo "</h3>
            <p class=\"main-text\">";
        // line 6
        echo Lang::get("txt_main_text_banner");
        echo "</p>
        </div>
    </div>
    <div id=\"carousel-example-generic\" class=\"carousel slide d-display\" data-ride=\"carousel\">
        <!-- Indicators -->
        <ol class=\"carousel-indicators\">
            <li data-target=\"#carousel-example-generic\" data-slide-to=\"0\" class=\"active\"></li>
            <li data-target=\"#carousel-example-generic\" data-slide-to=\"1\"></li>
            <li data-target=\"#carousel-example-generic\" data-slide-to=\"2\"></li>
            <li data-target=\"#carousel-example-generic\" data-slide-to=\"3\"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class=\"carousel-inner home-banner-parent\" role=\"listbox\">
            <div class=\"item active\">
                <img src=\"./assets/img/panjang1.jpg\" class=\"img-responsive\" alt=\"Banner World Alzheimer's Month\">
                <div class=\"carousel-caption home-banner\">
                    <h3 class=\"main-title\">";
        // line 23
        echo Lang::get("txt_main_menu_banner");
        echo "</h3>
                    <p class=\"main-text\">";
        // line 24
        echo Lang::get("txt_main_text_banner");
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <img src=\"./assets/img/panjang3.jpg\" class=\"img-responsive\" alt=\"Banner World Alzheimer's Month\">
                <div class=\"carousel-caption home-banner\">
                    <h3 class=\"main-title\">";
        // line 30
        echo Lang::get("txt_main_menu_banner");
        echo "</h3>
                    <p class=\"main-text\">";
        // line 31
        echo Lang::get("txt_main_text_banner");
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <img src=\"./assets/img/panjang4.jpg\" class=\"img-responsive\" alt=\"Banner World Alzheimer's Month\">
                <div class=\"carousel-caption home-banner\">
                    <h3 class=\"main-title\">";
        // line 37
        echo Lang::get("txt_main_menu_banner");
        echo "</h3>
                    <p class=\"main-text\">";
        // line 38
        echo Lang::get("txt_main_text_banner");
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <img src=\"./assets/img/panjang5.jpg\" class=\"img-responsive\" alt=\"Banner World Alzheimer's Month\">
                <div class=\"carousel-caption home-banner\">
                    <h3 class=\"main-title\">";
        // line 44
        echo Lang::get("txt_main_menu_banner");
        echo "</h3>
                    <p class=\"main-text\">";
        // line 45
        echo Lang::get("txt_main_text_banner");
        echo "</p>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class=\"left carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"prev\">
            <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Previous</span>
        </a>
        <a class=\"right carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"next\">
            <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Next</span>
        </a>
    </div>
";
    }

    // line 61
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 62
        echo "    ";
        echo Asset::css("default.css");
        echo "
    ";
        // line 63
        echo Asset::css("demo.css");
        echo "
    <style type=\"text/css\">
        .mapNav {
            display:none;
        }
        .europe_map {
            width:1019px;
            height:546px;
        }
    </style>
    <div class=\"parent\">
        <div class=\"child\">
            <div class=\"video-container\">
                <iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/w_siyLJTFjo\" frameborder=\"0\" allowfullscreen></iframe>
            </div>
            <h3 class=\"main-title\">";
        // line 78
        echo Lang::get("txt_main_title_impact");
        echo "</h3>
            <img class=\"d-display img-responsive center\" src=\"./assets/img/infographic-alzheimer-epidemic-";
        // line 79
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo ".jpg\" alt=\"Infographic World Alzheimer's Month\">
            <img class=\"m-display img-responsive center\" src=\"./assets/img/mobile/infographic-";
        // line 80
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo ".jpg\" alt=\"Infographic World Alzheimer's Month\">
        </div>
    </div>
    <div class=\"box css3-shadow\"></div>
    <div class=\"text-center distance-bottom\">
        <a class=\"submit\" href=\"";
        // line 85
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/get-involved#find-an-event\">";
        echo Lang::get("txt_title_find_event");
        echo "</a>
    </div>
    <div class=\"channel text-center\" style=\"position: relative\">
        <div class=\"text-center\"></div>
        <div class=\"box css3-shadow\"></div>
        <h3 class=\"font-bold line-xxheight\">
            <input id=\"total-user\" type=\"hidden\" />
            <div id=\"user-counter\"></div>
        </h3>
        <br/>
        ";
        // line 96
        echo "            ";
        // line 97
        echo "                ";
        // line 98
        echo "                ";
        // line 99
        echo "                    ";
        // line 100
        echo "                        ";
        // line 101
        echo "                            ";
        // line 102
        echo "                            ";
        // line 103
        echo "                            ";
        // line 104
        echo "                            ";
        // line 105
        echo "                            ";
        // line 106
        echo "                        ";
        // line 107
        echo "                    ";
        // line 108
        echo "                ";
        // line 109
        echo "            ";
        // line 110
        echo "        ";
        // line 111
        echo "
        <div style=\"\">
            <a class=\"twitter-timeline\"  href=\"https://twitter.com/AlzDisInt\" data-widget-id=\"602658822306471936\">Tweets by @AlzDisInt</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script>
        </div>
    </div>
    </div>
    </div>
    <div class=\"parent\">
        <div class=\"text-center distance-bottom d-display\">
            <button id=\"share-app-twitter\" class=\"submit twitter-submit\"><span class=\"twitter-bird\"></span>Tweet</button>
        </div>
        <div class=\"box css3-shadow\"></div>
        <div class=\"text-center distance-bottom\">
            <a class=\"submit\" href=\"";
        // line 125
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/get-involved\">";
        echo Lang::get("txt_download");
        echo "</a>
        </div>
    </div>
";
    }

    // line 129
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 130
        echo "    ";
        // line 131
        echo "    ";
        echo Asset::js("jquery-1.11.2.min.js");
        echo "
    ";
        // line 132
        echo Asset::js("canvas-menu.js");
        echo "
    ";
        // line 133
        echo Asset::js("bootstrap.min.js");
        echo "
    ";
        // line 134
        echo Asset::js("mobilymap.js");
        echo "
    <script type=\"text/javascript\">

        window.onload = function () {

            var numbers = new Array();
            var board = document.getElementById('total-user').value;
            var myDiv = document.getElementById('user-counter');
            var text = document.createTextNode(\"";
        // line 142
        echo Lang::get("txt_title_people_tweeting");
        echo "\");

            numbers = board.split('');
            var count = numbers.length;
            //alert('jumlah: '+numbers[0]);

            for(var i=0; i<count; i++){
                var newSpan = document.createElement('span');
                newSpan.setAttribute('class', 'flip-display');
                var t = document.createTextNode(numbers[i]);
                newSpan.appendChild(t);
                myDiv.appendChild(newSpan);
            }

            myDiv.appendChild(text);
        };

        \$(function(){

//            \$('.europe_map').mobilymap({
//                position: 'center',
//                popupClass: 'bubble',
//                markerClass: 'point',
//                popup: true,
//                cookies: false,
//                caption: false,
//                setCenter: true,
//                navigation: true,
//                navSpeed: 1000,
//                navBtnClass: 'navBtn',
//                outsideButtons: '.map_buttons a',
//                onMarkerClick: function(){},
//                onPopupClose: function(){},
//                onMapLoad: function(){}
//            });

        });
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"https:://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");
        window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src=\"https://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,\"script\",\"twitter-wjs\"));
        var tweetUrlBuilder = function(o){
            return [
                'https://twitter.com/intent/tweet?tw_p=tweetbutton',
                '&url=', encodeURI(o.url),
                '&via=', o.via,
                '&text=', o.text
            ].join('');
        };

        \$(document).on('click', '#share-app-twitter', function(e) {
            //We tell our browser not to follow that link
            e.preventDefault();
            //We get the URL of the link
            var loc = 'https://twitter.com/intent/tweet?original_referer=http%3A%2F%2Fathena.microad.co.id%2Fworld-alzheimers-month%2Fdo-you-remember-me%2Fstep-4&text=September is World Alzheimer%27s Month %23WAM2015. Join %40AlzDisInt and help raise dementia awareness around the world http%3A%2F%2Fworldalzmonth%2Eorg&tw_p=tweetbutton&url=worldalzmonth.org';
            //We trigger a new window with the Twitter dialog, in the middle of the page
            window.open(loc, 'twitterwindow', 'height=450, width=550, top='+(\$(window).height()/2 - 225) +', left='+\$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        });
        \$('.carousel').carousel({
            interval: 1000 * 5
//            interval: false
        })
    </script>
";
    }

    public function getTemplateName()
    {
        return "home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 142,  257 => 134,  253 => 133,  249 => 132,  244 => 131,  242 => 130,  239 => 129,  228 => 125,  212 => 111,  210 => 110,  208 => 109,  206 => 108,  204 => 107,  202 => 106,  200 => 105,  198 => 104,  196 => 103,  194 => 102,  192 => 101,  190 => 100,  188 => 99,  186 => 98,  184 => 97,  182 => 96,  166 => 85,  158 => 80,  154 => 79,  150 => 78,  132 => 63,  127 => 62,  124 => 61,  104 => 45,  100 => 44,  91 => 38,  87 => 37,  78 => 31,  74 => 30,  65 => 24,  61 => 23,  41 => 6,  37 => 5,  33 => 3,  30 => 2,);
    }
}
