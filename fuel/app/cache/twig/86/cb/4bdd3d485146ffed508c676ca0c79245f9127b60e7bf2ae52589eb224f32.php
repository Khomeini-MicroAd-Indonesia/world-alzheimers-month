<?php

/* welcome.twig */
class __TwigTemplate_86cb4bdd3d485146ffed508c676ca0c79245f9127b60e7bf2ae52589eb224f32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 4
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 7
        echo "\t\tDashboard
\t\t<small>Welcome</small>
\t</h1>
\t";
        // line 11
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 12
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li class=\"active\">Dashboard</li>
\t</ol>
</section>
";
    }

    // line 18
    public function block_backend_content($context, array $blocks = array())
    {
        // line 19
        echo "\t<h4>WELCOME</h4>
\t<div>
\t\tThis is Admin Page For Page Management - Ganbate!
\t</div>
";
    }

    public function getTemplateName()
    {
        return "welcome.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 19,  53 => 18,  44 => 12,  41 => 11,  36 => 7,  32 => 4,  29 => 3,);
    }
}
