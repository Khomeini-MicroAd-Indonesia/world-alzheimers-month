<?php

/* experience.twig */
class __TwigTemplate_3bdc8e5b52ca7e7298a1d2fdabac789d72d0564aa258ce80dc6262b2f661c3e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_header($context, array $blocks = array())
    {
    }

    // line 5
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 6
        echo "
<div class=\"parent\" style=\"min-height: 850px;\">
    <div class=\"child custom-5-padd\">
        <h3 class=\"main-title custom-v-padd\">";
        // line 9
        echo Lang::get("txt_title_do_you");
        echo "</h3>
        <img class=\"img-responsive d-display custom-v-padd\" src=\"./assets/img/experience-one.jpg\">
        <img class=\"img-responsive m-display custom-v-padd\" src=\"./assets/img/mobile/experience-one.jpg\">
        <p class=\"d-display\">";
        // line 12
        echo Lang::get("txt_desc_do_you");
        echo "</p>
        <p class=\"m-display custom-v-padd\">";
        // line 13
        echo Lang::get("txt_desc_do_you");
        echo "</p>
        <div class=\"facebook-login custom-v-padd\">
            ";
        // line 15
        if ((twig_length_filter($this->env, (isset($context["fb_login_url"]) ? $context["fb_login_url"] : null)) > 0)) {
            // line 16
            echo "                <a href=\"";
            echo (isset($context["fb_login_url"]) ? $context["fb_login_url"] : null);
            echo "\"><img src=\"";
            echo Uri::base();
            echo "/assets/css/images/login-with-facebook.png\"/></a>
            ";
        }
        // line 18
        echo "        </div>
    </div>
    </div>
</div>
";
    }

    // line 24
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 25
        echo Asset::js("jquery-1.11.2.min.js");
        echo "
";
        // line 26
        echo Asset::js("canvas-menu.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "experience.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 26,  79 => 25,  76 => 24,  68 => 18,  60 => 16,  58 => 15,  53 => 13,  49 => 12,  43 => 9,  38 => 6,  35 => 5,  30 => 3,);
    }
}
