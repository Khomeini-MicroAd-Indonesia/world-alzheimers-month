<?php

/* about_wam.twig */
class __TwigTemplate_aecac345c2ba155e157b104ac646d8c2c3882167c9d7a1306357a87f0300ae7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_header($context, array $blocks = array())
    {
    }

    // line 5
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 6
        echo "    <h3 class=\"main-title\">";
        echo Lang::get("txt_title_facts");
        echo "</h3>
    <div class=\"channel d-display\">
        <div class=\"row\">
            <div class=\"col-md-6\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-2\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 15
        echo Lang::get("txt_desc_fact_2");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-3\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 25
        echo Lang::get("txt_desc_fact_3");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-4\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 35
        echo Lang::get("txt_desc_fact_4");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-6\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 45
        echo Lang::get("txt_desc_fact_6");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-5\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 55
        echo Lang::get("txt_desc_fact_5");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-1\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 65
        echo Lang::get("txt_desc_fact_1a");
        echo "</p>
                        <p class=\"table-bordered\">";
        // line 66
        echo Lang::get("txt_desc_fact_1b");
        echo "</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"channel m-display\">
        <div class=\"row\">
            <div class=\"col-md-6\">
                <div class=\"row fact\">
                    <div class=\"col-xs-3\">
                        <span class=\"fact-1\"></span>
                    </div>
                    <div class=\"col-xs-9 fact-detail custom-10-padd\">
                        <p>";
        // line 80
        echo Lang::get("txt_desc_fact_1a");
        echo "</p>
                        <p>";
        // line 81
        echo Lang::get("txt_desc_fact_1b");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row fact\">
                    <div class=\"col-xs-9 fact-detail custom-10-padd\">
                        <p>";
        // line 88
        echo Lang::get("txt_desc_fact_2");
        echo "</p>
                    </div>
                    <div class=\"col-xs-3\">
                        <span class=\"fact-2\"></span>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row fact\">
                    <div class=\"col-xs-3\">
                        <span class=\"fact-3\"></span>
                    </div>
                    <div class=\"col-xs-9 fact-detail custom-10-padd\">
                        <p>";
        // line 101
        echo Lang::get("txt_desc_fact_3");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row fact\">
                    <div class=\"col-xs-9 fact-detail custom-10-padd\">
                        <p>";
        // line 108
        echo Lang::get("txt_desc_fact_4");
        echo "</p>
                    </div>
                    <div class=\"col-xs-3\">
                        <span class=\"fact-4\"></span>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row fact\">
                    <div class=\"col-xs-3\">
                        <span class=\"fact-5\"></span>
                    </div>
                    <div class=\"col-xs-9 fact-detail custom-10-padd\">
                        <p>";
        // line 121
        echo Lang::get("txt_desc_fact_5");
        echo "</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"parent\">
        <div class=\"child\">
            <div class=\"box css3-shadow\"></div>
            <h3 class=\"main-title\">";
        // line 130
        echo Lang::get("txt_title_remember_me");
        echo "</h3>
            <img src=\"./assets/img/World Alzheimer's Month map white.jpg\" class=\"img-responsive\">
            <div class=\"d-display\">
                <p class=\"left\">";
        // line 133
        echo Lang::get("txt_desc_remember_me_1");
        echo "</p>
                <p class=\"left\">";
        // line 134
        echo Lang::get("txt_desc_remember_me_2");
        echo " <a href=\"http://www.alz.co.uk/world-alzheimers-month/previous-campaigns\" target=\"_blank\">";
        echo Lang::get("txt_here");
        echo "</a> </p>
            </div>
            <div class=\"m-display\">
                <p class=\"left custom-10-padd\">";
        // line 137
        echo Lang::get("txt_desc_remember_me_1");
        echo "</p>
                <p class=\"left custom-10-padd\">";
        // line 138
        echo Lang::get("txt_desc_remember_me_2");
        echo " <a href=\"http://www.alz.co.uk/world-alzheimers-month/previous-campaigns\" target=\"_blank\">";
        echo Lang::get("txt_here");
        echo "</a> </p>
            </div>
        </div>
    </div>
    <div class=\"parent\">
        <div class=\"child\">
            <div class=\"box css3-shadow\"></div>
            <h3 class=\"main-title\">";
        // line 145
        echo Lang::get("txt_title_about_wam");
        echo "</h3>
            <div class=\"row d-display\">
                <div class=\"col-md-4\">
                    <img src=\"./assets/img/ADI-logo-NEW.png\" class=\"img-responsive\">
                </div>
                <div class=\"col-md-8 vertical-line\" >
                    <p class=\"left\">";
        // line 151
        echo Lang::get("txt_desc_about_wam_1");
        echo "</p>
                    <p class=\"left\">";
        // line 152
        echo Lang::get("txt_desc_about_wam_2");
        echo "</p>
                    <p class=\"left\">";
        // line 153
        echo Lang::get("txt_desc_about_wam_3");
        echo "</p>
                </div>
            </div>
            <div class=\"row m-display custom-10-padd\">

                    <p class=\"left\">";
        // line 158
        echo Lang::get("txt_desc_about_wam_1");
        echo "</p>
                    <p class=\"left\">";
        // line 159
        echo Lang::get("txt_desc_about_wam_2");
        echo "</p>
                <span>
                    <img src=\"./assets/img/ADI-logo-NEW.png\" class=\"custom-v img-responsive-costom\">
                </span>
                    <p class=\"left\">";
        // line 163
        echo Lang::get("txt_desc_about_wam_3");
        echo "</p>

            </div>
        </div>
    </div>
";
    }

    // line 170
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 171
        echo "    ";
        echo Asset::js("jquery-1.11.2.min.js");
        echo "
    ";
        // line 172
        echo Asset::js("canvas-menu.js");
        echo "
    <script type=\"text/javascript\">
        \$('.carousel').carousel({
//        interval: 2000
        })
    </script>
";
    }

    public function getTemplateName()
    {
        return "about_wam.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  293 => 172,  288 => 171,  285 => 170,  275 => 163,  268 => 159,  264 => 158,  256 => 153,  252 => 152,  248 => 151,  239 => 145,  227 => 138,  223 => 137,  215 => 134,  211 => 133,  205 => 130,  193 => 121,  177 => 108,  167 => 101,  151 => 88,  141 => 81,  137 => 80,  120 => 66,  116 => 65,  103 => 55,  90 => 45,  77 => 35,  64 => 25,  51 => 15,  38 => 6,  35 => 5,  30 => 3,);
    }
}
