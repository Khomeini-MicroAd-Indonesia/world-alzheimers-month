<?php

/* add_event.twig */
class __TwigTemplate_c778d68337539d8777f8ddc6c3e7e344ab30375e4a9c1cd1b5129a2f1ed7f45a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_header($context, array $blocks = array())
    {
    }

    // line 5
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 6
        echo "
 
<div class=\"parent\" style=\"min-height: 850px;\">
    <div class=\"text-center\">
        <h3>";
        // line 10
        echo Lang::get("txt_add_event");
        echo "</h3>
    </div>
    <div id=\"thanks\" style=\"display: none;\"><span class=\"alert alert-success\">";
        // line 12
        echo (isset($context["success_message"]) ? $context["success_message"] : null);
        echo "</span><br><br></div> 
    <form role=\"form\" id=\"modal-addevent-form\" class=\"contact\" enctype=\"multipart/form-data\" action=\"\" name=\"event-form\" method=\"POST\">
        <div class=\"form-group\">
            <input type=\"text\" class=\"form-control input-lg\" placeholder=\"";
        // line 15
        echo Lang::get("txt_event_name");
        echo "\" name=\"eventname\">
        </div>
        <div class=\"form-group\">
            <input type=\"email\" class=\"form-control input-lg\" placeholder=\"";
        // line 18
        echo Lang::get("txt_email");
        echo "\" name=\"email\">
        </div>
        <div class=\"form-group\">
            <textarea name=\"desc\" class=\"form-control\" placeholder=\"";
        // line 21
        echo Lang::get("txt_event_desc");
        echo "\"></textarea>
        </div>
        <div class=\"form-group\">
            <input type=\"datetime\" class=\"form-control input-lg\" placeholder=\"";
        // line 24
        echo Lang::get("txt_date");
        echo "\" name=\"date\">
        </div>
        <div class=\"form-group\">
            <input type=\"datetime\" class=\"form-control input-lg\" placeholder=\"";
        // line 27
        echo Lang::get("txt_time");
        echo "\" name=\"time\">
        </div>
        <div class=\"form-group\">
            <input type=\"text\" class=\"form-control input-lg\" placeholder=\"";
        // line 30
        echo Lang::get("txt_country");
        echo "\" name=\"countryf\">
        </div>
        <div class=\"form-group\">
            <input type=\"text\" class=\"form-control input-lg\" placeholder=\"";
        // line 33
        echo Lang::get("txt_city");
        echo "\" name=\"cityf\">
        </div>
        <div class=\"form-group\">
            <input type=\"text\" class=\"form-control input-lg\" placeholder=\"";
        // line 36
        echo Lang::get("txt_location");
        echo "\" name=\"location\">
        </div>
        <div class=\"form-group\">
            <input type=\"text\" class=\"form-control input-lg\" placeholder=\"";
        // line 39
        echo Lang::get("txt_organization");
        echo "\" name=\"organisation\">
        </div>
        <div class=\"form-group\">
            <input type=\"text\" class=\"form-control input-lg\" placeholder=\"";
        // line 42
        echo Lang::get("txt_web_link");
        echo "\" name=\"weblink\">
        </div>
        <div class=\"form-group\">
            <input type=\"file\" id=\"uploaded_file\" name=\"uploaded_file\" class=\"form-control input-lg\" onchange=\"previewImage(this);\">
        </div>
        <div class=\"form-group\">
            <img src=\"assets/img/frame.png\" alt=\"Image preview\" id=\"preview\">
        </div>
        <input id=\"status\" type=\"hidden\" value=\"";
        // line 50
        echo (isset($context["status_message"]) ? $context["status_message"] : null);
        echo "\">
        <div class=\"modal-footer\">
            <input id=\"submit\" type=\"submit\" class=\"submit\" value=\"Submit\">
        </div>    
    </form>
    

</div>
";
    }

    // line 60
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 61
        echo Asset::js("jquery-1.11.2.min.js");
        echo "
";
        // line 62
        echo Asset::js("canvas-menu.js");
        echo "

<script>
    function previewImage(input) {
        
        var preview = document.getElementById('preview');
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
          preview.setAttribute('src', e.target.result);
         }
         reader.readAsDataURL(input.files[0]);
        }else {
         preview.setAttribute('src', 'placeholder.png');
        }
        
    }
    
   (function(){
       
       var status = document.getElementById('status').value;
       //alert(status);
       if(status == 1){
           document.getElementById('thanks').style.display = \"block\";
       }
   })(); 
</script>
";
    }

    public function getTemplateName()
    {
        return "add_event.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 62,  136 => 61,  133 => 60,  120 => 50,  109 => 42,  103 => 39,  97 => 36,  91 => 33,  85 => 30,  79 => 27,  73 => 24,  67 => 21,  61 => 18,  55 => 15,  49 => 12,  44 => 10,  38 => 6,  35 => 5,  30 => 3,);
    }
}
