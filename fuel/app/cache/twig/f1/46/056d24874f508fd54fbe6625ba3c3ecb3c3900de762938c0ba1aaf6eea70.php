<?php

/* experience_new_1.twig */
class __TwigTemplate_f146056d24874f508fd54fbe6625ba3c3ecb3c3900de762938c0ba1aaf6eea70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_header($context, array $blocks = array())
    {
    }

    // line 5
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 6
        echo "    <div class=\"parent\" style=\"min-height: 850px;\">
        <div class=\"col-md-7\">
            <img src=\"./assets/img/experience-one-new-banner.jpg\" class=\"img-responsive m-center\">
        </div>
        <div class=\"col-md-5 m-text-center\">
            <h3 class=\"main-title-left m-text-center\">";
        // line 11
        echo Lang::get("txt_title_do_you");
        echo "</h3>
            <p class=\"font-13\">";
        // line 12
        echo Lang::get("txt_experience_new_1_content_1");
        echo " </p>
            <p class=\"font-13\">";
        // line 13
        echo Lang::get("txt_experience_new_1_content_2");
        echo "</p>
            <p class=\"font-13\">";
        // line 14
        echo Lang::get("txt_experience_new_1_content_3");
        echo "</p>
            <p class=\"text-warning font-18\">Login with Facebook to get started.</p>
            <p class=\"m-display\">";
        // line 16
        echo Lang::get("txt_desc_do_you_login");
        echo "</p>
            <div class=\"facebook-login custom-v-padd m-text-center\">
                ";
        // line 18
        if ((twig_length_filter($this->env, (isset($context["fb_login_url"]) ? $context["fb_login_url"] : null)) > 0)) {
            // line 19
            echo "                    <a href=\"";
            echo (isset($context["fb_login_url"]) ? $context["fb_login_url"] : null);
            echo "\"><img src=\"";
            echo Uri::base();
            echo "/assets/css/images/login-with-facebook.png\"/></a>
                ";
        }
        // line 21
        echo "            </div>
        </div>
    </div>
";
    }

    // line 26
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 27
        echo "    ";
        echo Asset::js("jquery-1.11.2.min.js");
        echo "
    ";
        // line 28
        echo Asset::js("canvas-menu.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "experience_new_1.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 28,  87 => 27,  84 => 26,  77 => 21,  69 => 19,  67 => 18,  62 => 16,  57 => 14,  53 => 13,  49 => 12,  45 => 11,  38 => 6,  35 => 5,  30 => 3,);
    }
}
