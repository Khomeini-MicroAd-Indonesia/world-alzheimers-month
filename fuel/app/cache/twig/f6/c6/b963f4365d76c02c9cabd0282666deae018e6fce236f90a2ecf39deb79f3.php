<?php

/* change_password.twig */
class __TwigTemplate_f6c6b963f4365d76c02c9cabd0282666deae018e6fce236f90a2ecf39deb79f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 4
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 7
        echo "\t\tDashboard
\t\t<small>Change Password</small>
\t</h1>
\t";
        // line 11
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 12
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li>Dashboard</li>
\t\t<li class=\"active\">Change Password</li>
\t</ol>
</section>
";
    }

    // line 19
    public function block_backend_content($context, array $blocks = array())
    {
        // line 20
        echo "\t<div class=\"form-box\" id=\"login-box\">
\t\t";
        // line 21
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 22
            echo "\t\t<div class=\"alert alert-success alert-dismissable\">
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t\t\t";
            // line 24
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
\t\t</div>
\t\t";
        }
        // line 27
        echo "
\t\t";
        // line 28
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 29
            echo "\t\t<div class=\"alert alert-danger alert-dismissable\">
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t\t\t";
            // line 31
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
\t\t</div>
\t\t";
        }
        // line 34
        echo "\t\t
\t\t<form action=\"\" method=\"post\">
\t\t\t<div class=\"body bg-gray\">
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<input type=\"password\" name=\"password_old\" class=\"form-control\" placeholder=\"Old Password\" required />
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password\" required />
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<input type=\"password\" name=\"password_confirm\" class=\"form-control\" placeholder=\"Confirm Password\" required />
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div>
\t\t\t\t<button type=\"submit\" class=\"btn bg-olive btn-block\">Change Password</button>
\t\t\t</div>
\t\t</form>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "change_password.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 34,  81 => 31,  77 => 29,  75 => 28,  72 => 27,  66 => 24,  62 => 22,  60 => 21,  57 => 20,  54 => 19,  44 => 12,  41 => 11,  36 => 7,  32 => 4,  29 => 3,);
    }
}
