<?php

/* pages/views/template_frontend.twig */
class __TwigTemplate_09e23d5ce9639a262430b19f634484af38bb3255bca567f2d8ab01cb35067f2e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fb_head_prefix' => array($this, 'block_fb_head_prefix'),
            'fb_meta_data' => array($this, 'block_fb_meta_data'),
            'frontend_css' => array($this, 'block_frontend_css'),
            'fb_js_sdk' => array($this, 'block_fb_js_sdk'),
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"no-js\" lang=\"en\">
<head ";
        // line 3
        $this->displayBlock('fb_head_prefix', $context, $blocks);
        echo ">
    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1, maximum-scale=1,user-scalable=no\">
    <base href=\"";
        // line 5
        echo Uri::base();
        echo "\" />

    ";
        // line 7
        $this->displayBlock('fb_meta_data', $context, $blocks);
        // line 8
        echo "
    <meta charset=\"utf-8\" />

    <title>";
        // line 11
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <link rel=\"icon\" type=\"image/ico\" href=\"";
        // line 12
        echo Uri::base();
        echo "assets/img/icon.png\" />
    <meta name=\"description\" content=\"";
        // line 13
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    
    ";
        // line 15
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 21
        echo "    <script type=\"text/javascript\" src=\"http://fast.fonts.net/jsapi/53758671-72ec-4086-987f-5d3e98d95378.js\"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 28
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
    ";
        // line 33
        $this->displayBlock('fb_js_sdk', $context, $blocks);
        // line 34
        echo "    <div class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\" id=\"slide-nav\" style=\"border-bottom:1px solid #D2D2D2\">
        <div class=\"row logo\" style=\"border-bottom:1px solid #D2D2D2\">
            <div class=\"container\">
                <div class=\"row logo-mobile\">
                    <div class=\"col-xs-6 hidden-xs .col-sm-5cols\">
                        <a href=\"";
        // line 39
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "\"><img src=\"./assets/img/logo-wam-3.png\" style=\"background-color: #ffffff\" alt=\"logo world alzheimer's month\"></a>
                    </div>
                    <div class=\"col-xs-6 hidden-xs .col-sm-5cols\">
                        <a href=\"http://www.alz.co.uk\" target=\"_blank\"><img class=\"right\" src=\"./assets/img/logo-adi.png\" alt=\"logo ADI\"></a>
                    </div>
                    <div class=\"col-xs-6 visible-xs .col-sm-5cols\">
                        <a href=\"";
        // line 45
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "\"><img src=\"./assets/img/mobile/logo-wam.png\" style=\"background-color: #ffffff\" alt=\"logo world alzheimer's month\"></a>
                    </div>
                    <div class=\"col-xs-6 visible-xs .col-sm-5cols\">
                        <a href=\"http://www.alz.co.uk\" target=\"_blank\"><img class=\"right\" src=\"./assets/img/mobile/logo-az.png\" alt=\"logo ADI\"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"container\">
            <div class=\"navbar-header\">
                <a class=\"navbar-toggle\">
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </a>
            </div>
            <div id=\"slidemenu\">
                <ul class=\"nav navbar-nav nav-main\">
                    ";
        // line 63
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["frontend_menus"]) ? $context["frontend_menus"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
            // line 64
            echo "                        <li><a href=\"";
            echo $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "route");
            echo "\">";
            echo Lang::get($this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "lang_code"));
            echo "</a> </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                    
                </ul>
                <ul class=\"nav navbar-nav lang\">
                    ";
        // line 69
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["lang_item"]) {
            // line 70
            echo "                        <li><a href=\"";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
            echo "\"><span ";
            echo (($this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "active")) ? ("class=\"active\"") : (""));
            echo ">";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "label");
            echo "</span></a></li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lang_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "                </ul>
            </div>
        </div>
    </div>
    ";
        // line 76
        $this->displayBlock('frontend_header', $context, $blocks);
        // line 77
        echo "
    <div class=\"container\">
        ";
        // line 79
        $this->displayBlock('frontend_content', $context, $blocks);
        // line 80
        echo "    </div> <!-- /container -->
    <div class=\"footer d-display\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-9\">
                    ";
        // line 86
        echo "                        <p class=\"font-small line-xheight\">";
        echo Lang::get("txt_footnote_campaign");
        echo "</p>
                    ";
        // line 88
        echo "                    ";
        // line 89
        echo "                        <p class=\"font-bold line-xheight\">ALZHEIMER'S DISEASE INTERNATIONAL (ADI)</p>
                        <p class=\"font-small line-xheight\">";
        // line 90
        echo Lang::get("txt_footnote_supported");
        echo "</p>
                        <a href=\"http://www.nutricia.com/\" target=\"_blank\"><img src=\"./assets/img/nutricia.png\" style=\"width: 218px\" alt=\"logo Nutricia\"></a>
                    ";
        // line 93
        echo "                    ";
        // line 94
        echo "                        <ul class=\"social-media xline-height\">
                            <li ><a class=\"facebook\" href=\"https://www.facebook.com/alzheimersdiseaseinternational\" target=\"_blank\"></a></li>
                            <li ><a class=\"twitter\" href=\"https://twitter.com/alzdisint\" target=\"_blank\"></a></li>
                            <li ><a class=\"youtube\" href=\"https://www.youtube.com/user/alzdisint\" target=\"_blank\"></a></li>
                        </ul>
                    ";
        // line 100
        echo "                    ";
        // line 101
        echo "                        <p class=\"font-small\">";
        echo Lang::get("txt_footnote_copyright");
        echo " &nbsp;| &nbsp;<a href=\"";
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/privacy-policy\">";
        echo Lang::get("txt_footnote_privacy");
        echo "</a>&nbsp;";
        echo "</p>

                    ";
        // line 104
        echo "                </div>
                <div class=\"col-md-3\">
                    <p class=\"font-medium\">";
        // line 106
        echo Lang::get("txt_footnote_contact");
        echo "</p>
                    <p class=\"font-small\">Alzheimer's Disease International <br/> 64 Great Suffolk Street <br/>London <br/>SE1 0BL<br/>UK</p>
                    <p class=\"font-small\">Tel: +44 20 79810880 <br/>Fax: +44 20 79282357</p>
                    <p class=\"font-small\">Email: <a href=\"mailto:info@alz.co.uk\">info@alz.co.uk</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"footer m-display\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-xs-12\">
                    <div class=\"col-md-12\">
                        <p class=\"font-small line-xheight\">";
        // line 119
        echo Lang::get("txt_footnote_campaign");
        echo "</p>
                    </div>
                    <div class=\"col-md-12\">
                        <p class=\"font-bold line-xheight\">ALZHEIMER'S DISEASE INTERNATIONAL (ADI)</p>
                        <p class=\"font-small line-xheight\">";
        // line 123
        echo Lang::get("txt_footnote_supported");
        echo "</p>
                        <a href=\"http://www.nutricia.com/\" target=\"_blank\"><img src=\"./assets/img/nutricia.png\" style=\"width: 218px\" alt=\"logo Nutricia\"></a>
                    </div>
                </div>
                </div>
            <div class=\"row\">
                <div class=\"custom-padd\">
                    <div class=\"col-xs-12\">
                        <p class=\"font-medium\">";
        // line 131
        echo Lang::get("txt_footnote_contact");
        echo "</p>
                        <p class=\"font-small\">Alzheimer's Disease International <br/> 64 Great Sulfolk Street <br/>London <br/>SE1 0BL<br/>UK</p>
                        <p class=\"font-small\">Tel: +44 20 79810880 <br/>Fax: +44 20 79282357</p>
                        <p class=\"font-small\">Email: info@alz.co.uk</p>
                    </div>
                    <div class=\"col-xs-12\">
                        <ul class=\"social-media xline-height\">
                            <li class=\"facebook\"><a href=\"https://www.facebook.com/alzheimersdiseaseinternational\"></a></li>
                            <li class=\"twitter\"><a href=\"https://twitter.com/alzdisint\"></a></li>
                            <li class=\"youtube\"><a href=\"https://www.youtube.com/user/alzdisint\"></a></li>
                        </ul>
                    </div>
                    <div class=\"col-md-12\">
                        <p class=\"font-small line-xxheight\">";
        // line 144
        echo Lang::get("txt_footnote_copyright");
        echo " | <a href=\"";
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "privacy\">";
        echo Lang::get("txt_footnote_privacy");
        echo "</a>
                            ";
        // line 146
        echo "                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    ";
        // line 154
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 160
        echo "</body>
</html>";
    }

    // line 3
    public function block_fb_head_prefix($context, array $blocks = array())
    {
    }

    // line 7
    public function block_fb_meta_data($context, array $blocks = array())
    {
    }

    // line 15
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 16
        echo "        ";
        echo Asset::css("normalize.css");
        echo "
        ";
        // line 17
        echo Asset::css("bootstrap.min.css");
        echo "
        ";
        // line 18
        echo Asset::css((("style-" . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . ".css"));
        echo "
        ";
        // line 19
        echo Asset::css("canvas-menu.css");
        echo "
    ";
    }

    // line 33
    public function block_fb_js_sdk($context, array $blocks = array())
    {
    }

    // line 76
    public function block_frontend_header($context, array $blocks = array())
    {
    }

    // line 79
    public function block_frontend_content($context, array $blocks = array())
    {
    }

    // line 154
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 155
        echo "        ";
        echo Asset::js("jquery.latest.min.js");
        echo "
        ";
        // line 156
        echo Asset::js("bootsrap.min.js");
        echo "
        ";
        // line 157
        echo Asset::js("canvas-menu.js");
        echo "
        ";
        // line 159
        echo "    ";
    }

    public function getTemplateName()
    {
        return "pages/views/template_frontend.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  357 => 159,  353 => 157,  349 => 156,  344 => 155,  341 => 154,  336 => 79,  331 => 76,  326 => 33,  320 => 19,  316 => 18,  312 => 17,  307 => 16,  304 => 15,  299 => 7,  294 => 3,  289 => 160,  287 => 154,  277 => 146,  268 => 144,  252 => 131,  234 => 119,  218 => 106,  214 => 104,  202 => 101,  200 => 100,  186 => 90,  176 => 86,  169 => 80,  167 => 79,  161 => 76,  142 => 70,  138 => 69,  133 => 66,  122 => 64,  118 => 63,  97 => 45,  88 => 39,  81 => 34,  79 => 33,  71 => 28,  62 => 21,  60 => 15,  55 => 13,  51 => 12,  47 => 11,  42 => 8,  40 => 7,  35 => 5,  26 => 1,  265 => 139,  254 => 131,  250 => 130,  246 => 129,  241 => 123,  239 => 127,  236 => 126,  225 => 122,  209 => 108,  207 => 107,  205 => 106,  203 => 105,  201 => 104,  199 => 103,  197 => 102,  195 => 101,  193 => 94,  191 => 93,  189 => 98,  187 => 97,  185 => 96,  183 => 89,  181 => 88,  179 => 93,  163 => 77,  155 => 72,  151 => 76,  147 => 75,  132 => 63,  127 => 62,  124 => 61,  104 => 45,  100 => 44,  91 => 38,  87 => 37,  78 => 31,  74 => 30,  65 => 24,  61 => 23,  41 => 6,  37 => 5,  33 => 3,  30 => 3,);
    }
}
