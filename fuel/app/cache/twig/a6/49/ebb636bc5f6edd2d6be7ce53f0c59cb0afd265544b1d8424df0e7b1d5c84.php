<?php

/* about_wam.twig */
class __TwigTemplate_a649ebb636bc5f6edd2d6be7ce53f0c59cb0afd265544b1d8424df0e7b1d5c84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_header' => array($this, 'block_frontend_header'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_header($context, array $blocks = array())
    {
    }

    // line 5
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 6
        echo "    <h3 class=\"main-title\">";
        echo Lang::get("txt_title_facts");
        echo "</h3>
    <div class=\"video-container\">
        <iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/V26IMR_cqi4?feature=player_detailpage\" frameborder=\"0\" allowfullscreen></iframe>
    </div>
    <div class=\"channel d-display\">
        <div class=\"row\">
            <div class=\"col-md-6\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-2\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 18
        echo Lang::get("txt_desc_fact_2a");
        echo " <a href=\"";
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/get-involved#find-an-event\">";
        echo Lang::get("txt_desc_fact_find_event");
        echo "</a> ";
        echo Lang::get("txt_desc_fact_2b");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6 box-wam\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-5\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 28
        echo Lang::get("txt_desc_fact_5");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-1\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 38
        echo Lang::get("txt_desc_fact_1a");
        echo "<br/>";
        echo Lang::get("txt_desc_fact_1b");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-6\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 48
        echo Lang::get("txt_desc_fact_6");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-3\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 58
        echo Lang::get("txt_desc_fact_3");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row\">
                    <div class=\"col-md-2\">
                        <span class=\"fact-4\"></span>
                    </div>
                    <div class=\"col-md-10\">
                        <p class=\"table-bordered\">";
        // line 68
        echo Lang::get("txt_desc_fact_4");
        echo "</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"channel m-display\">
        <div class=\"row\">
            <div class=\"col-md-6\">
                <div class=\"row fact\">
                    <div class=\"col-xs-3\">
                        <span class=\"fact-1\"></span>
                    </div>
                    <div class=\"col-xs-9 fact-detail custom-10-padd\">
                        <p>";
        // line 82
        echo Lang::get("txt_desc_fact_1a");
        echo "</p>
                        <p>";
        // line 83
        echo Lang::get("txt_desc_fact_1b");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row fact\">
                    <div class=\"col-xs-9 fact-detail custom-10-padd\">
                        <p>";
        // line 90
        echo Lang::get("txt_desc_fact_2a");
        echo " <a href=\"";
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/get-involved#find-an-event\">";
        echo Lang::get("txt_desc_fact_find_event");
        echo "</a> ";
        echo Lang::get("txt_desc_fact_2b");
        echo "</p>
                    </div>
                    <div class=\"col-xs-3\">
                        <span class=\"fact-2\"></span>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row fact\">
                    <div class=\"col-xs-3\">
                        <span class=\"fact-3\"></span>
                    </div>
                    <div class=\"col-xs-9 fact-detail custom-10-padd\">
                        <p>";
        // line 103
        echo Lang::get("txt_desc_fact_3");
        echo "</p>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row fact\">
                    <div class=\"col-xs-9 fact-detail custom-10-padd\">
                        <p>";
        // line 110
        echo Lang::get("txt_desc_fact_4");
        echo "</p>
                    </div>
                    <div class=\"col-xs-3\">
                        <span class=\"fact-4\"></span>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"row fact\">
                    <div class=\"col-xs-3\">
                        <span class=\"fact-5\"></span>
                    </div>
                    <div class=\"col-xs-9 fact-detail custom-10-padd\">
                        <p>";
        // line 123
        echo Lang::get("txt_desc_fact_5");
        echo "</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class=\"parent\">
        <div class=\"child\">
            <div class=\"box css3-shadow\"></div>
            <h3 class=\"main-title\">";
        // line 133
        echo Lang::get("txt_title_remember_me");
        echo "</h3>
            <img src=\"./assets/img/World Alzheimer's Month map white.jpg\" class=\"img-responsive\" alt=\"World Alzheimer's Month Map\">
            <div class=\"d-display\">
                <p class=\"left\">";
        // line 136
        echo Lang::get("txt_desc_remember_me_1");
        echo "</p>
                <p class=\"left\">";
        // line 137
        echo Lang::get("txt_desc_remember_me_2");
        echo " <a href=\"http://www.alz.co.uk/world-alzheimers-month/previous-campaigns\" target=\"_blank\">";
        echo Lang::get("txt_here");
        echo "</a> </p>
            </div>
            <div class=\"m-display\">
                <p class=\"left custom-10-padd\">";
        // line 140
        echo Lang::get("txt_desc_remember_me_1");
        echo "</p>
                <p class=\"left custom-10-padd\">";
        // line 141
        echo Lang::get("txt_desc_remember_me_2");
        echo " <a href=\"http://www.alz.co.uk/world-alzheimers-month/previous-campaigns\" target=\"_blank\">";
        echo Lang::get("txt_here");
        echo "</a> </p>
            </div>
        </div>
    </div>
    <div class=\"parent\">
        <div class=\"child\">
            <div class=\"box css3-shadow\"></div>
            <h3 class=\"main-title\">";
        // line 148
        echo Lang::get("txt_title_about_wam");
        echo "</h3>
            <div class=\"row d-display\">
                <div class=\"col-md-4\">
                    <img src=\"./assets/img/ADI-logo-NEW.png\" class=\"img-responsive\" alt=\"Logo ADI\">
                </div>
                <div class=\"col-md-8 vertical-line\" >
                    <p class=\"left\">";
        // line 154
        echo Lang::get("txt_desc_about_wam_1");
        echo "</p>
                    <p class=\"left\">";
        // line 155
        echo Lang::get("txt_desc_about_wam_2");
        echo "</p>
                    <p class=\"left\">";
        // line 156
        echo Lang::get("txt_desc_about_wam_3");
        echo "</p>
                </div>
            </div>
            <div class=\"row m-display custom-10-padd\">

                    <p class=\"left\">";
        // line 161
        echo Lang::get("txt_desc_about_wam_1");
        echo "</p>
                    <p class=\"left\">";
        // line 162
        echo Lang::get("txt_desc_about_wam_2");
        echo "</p>
                    <p class=\"left\">";
        // line 163
        echo Lang::get("txt_desc_about_wam_3");
        echo "</p>
                <span>
                    <img src=\"./assets/img/ADI-logo-NEW.png\" class=\"custom-v img-responsive-costom\" alt=\"Logo ADI\">
                </span>

            </div>
        </div>
    </div>
";
    }

    // line 173
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 174
        echo "    ";
        echo Asset::js("jquery-1.11.2.min.js");
        echo "
    ";
        // line 175
        echo Asset::js("canvas-menu.js");
        echo "
    <script type=\"text/javascript\">
        \$('.carousel').carousel({
            interval: 1000 * 5
        })
    </script>
";
    }

    public function getTemplateName()
    {
        return "about_wam.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  309 => 175,  304 => 174,  301 => 173,  288 => 163,  284 => 162,  280 => 161,  272 => 156,  268 => 155,  264 => 154,  255 => 148,  243 => 141,  239 => 140,  231 => 137,  227 => 136,  221 => 133,  208 => 123,  192 => 110,  182 => 103,  159 => 90,  149 => 83,  145 => 82,  128 => 68,  115 => 58,  102 => 48,  87 => 38,  74 => 28,  54 => 18,  38 => 6,  35 => 5,  30 => 3,);
    }
}
