<?php

return array(
    'cms_name' => 'WAM',
    'app_lang' => 'de',
	'max_lock_count' => 5,
	'cms_session_name' => array(
		'admin_id' => 'WAM'
	),
	'admin_default_password' => 'wam',
	'max_email_sent_daily' => 500,
	'max_email_sent_per_task' => 10,
	
	'menus' => array(
		'dashboard' => array(
			'label' => 'Dashboard',
			'route' => 'backend',
			'icon_class' => 'fa fa-dashboard',
			'permission' => false,
		),
                'admin_management' => array(
			'label' => 'Admin Management',
			'icon_class' => 'fa fa-columns',
			'permission' => false,
			'submenus' => array(
				'admin_user' => array(
					'label' => 'Admin User',
					'route' => 'backend/admin-user',
					'icon_class' => 'fa fa-users',
					'permission' => true,
				),
				'admin_role_permission' => array(
					'label' => 'Admin Role Permission',
					'route' => 'backend/admin-role-permission',
					'icon_class' => 'fa fa-shield',
					'permission' => true,
				),
                                'admin_setting' => array(
                                'label' => 'Basic Setting',
                                'route' => 'backend/setting',
                                'icon_class' => 'fa fa-gear',
                                'permission' => true,
                                ),
                        
                                )
		),
                'event' => array(
                    'label' => 'Event Management',
                    'icon_class' => 'fa fa-picture-o',
                    'permission' => false,
                    'submenus' => array(
                        'event_organizer' => array(
                            'label' => 'Organize',
                            'route' => 'backend/event',
                            'icon_class' => 'fa fa-calendar',
                            'permission' => true,
                        ),
                        'event_image' => array(
                            'label' => 'Image',
                            'route' => 'backend/event/image',
                            'permission' => true,
                         ),
                        'event_region' => array(
                            'label' => 'Region',
                            'route' => 'backend/event/region',
                            'permission' => true,
                        ),
                        'event_country' => array(
                            'label' => 'Country',
                            'route' => 'backend/event/country',
                            'permission' => true,
                        ),
                        'event_city' => array(
                            'label' => 'City',
                            'route' => 'backend/event/city',
                            'permission' => true,
                        ),
                    )
                    
                        
                ),
                'admin_fb_management' => array(
			'label' => 'Facebook Management',
			'icon_class' => 'fa fa-facebook',
			'permission' => false,
			'submenus' => array(
				'admin_fb_setting' => array(
					'label' => 'Facebook Setting',
					'route' => 'backend/fb-setting',
					'icon_class' => 'fa fa-gear',
					'permission' => true,
				),
				
			)
		),
                'media_uploader' => array(
			'label' => 'Media Uploader',
			'icon_class' => 'fa fa-upload',
			'permission' => false,
			'submenus' => array(
                            'media_uploader_event' => array(
                            'label' => 'Event Image',
                            'route' => 'backend/mediauploader/event-image',
                            'icon_class' => 'fa fa-upload',
                            'permission' => true,
                        ),
                            'media_uploader_fbcover' => array(
                            'label' => 'FB Cover',
                            'route' => 'backend/mediauploader/fb-cover',
                            'icon_class' => 'fa fa-upload',
                            'permission' => true,
                        ),
                            
                            'media_uploader_fbprofilepic' => array(
                            'label' => 'FB Profile Picture',
                            'route' => 'backend/mediauploader/fb-profile-pic',
                            'icon_class' => 'fa fa-upload',
                            'permission' => true,
                        ),
                            
                            'media_uploader_printad' => array(
                            'label' => 'Print Ad',
                            'route' => 'backend/mediauploader/print-ad',
                            'icon_class' => 'fa fa-upload',
                            'permission' => true,
                        ),
                            'media_uploader_printadtypo' => array(
                            'label' => 'Print Ad -Typo',
                            'route' => 'backend/mediauploader/print-ad-typo',
                            'icon_class' => 'fa fa-upload',
                            'permission' => true,
                        ),
                            
                            'media_uploader_socmedposter' => array(
                            'label' => 'Socmed Image Poster',
                            'route' => 'backend/mediauploader/socmed-poster',
                            'icon_class' => 'fa fa-upload',
                            'permission' => true,
                        ),
                            
                            'media_uploader_tshirtdesign' => array(
                            'label' => 'T-shirt Design',
                            'route' => 'backend/mediauploader/tshirt-design',
                            'icon_class' => 'fa fa-upload',
                            'permission' => true,
                        ),
                            
                            
                    )
		),
		'pages' => array(
			'label' => 'Pages',
			'route' => 'backend/pages',
			'icon_class' => 'fa fa-sitemap',
			'permission' => false,
		),
		'printed_ad' => array(
			'label' => 'Printed Ad',
			'icon_class' => 'fa fa-camera',
			'permission' => false,
                        'submenus' => array(
                            'print_ad' => array(
                                'label' => 'Print Ad',
                                'route' => 'backend/print-ad',
                                'icon_class' => 'fa fa-file-image-o',
                                'permission' => true,
                            ),
                            'print_ad_typo' => array(
                                'label' => 'Print Ad Typo',
                                'route' => 'backend/print-ad-typo',
                                'icon_class' => 'fa fa-font',
                                'permission' => true,
                            ),
                        )
		),
                'social_asset' => array(
			'label' => 'Social Media Assets',
			'icon_class' => 'fa fa-archive',
			'permission' => false,
                        'submenus' => array(
                            'fb_cover' => array(
                                'label' => 'FB Cover',
                                'route' => 'backend/fb-cover',
                                'permission' => true,
                            ),
                            'fb_profile_pic' => array(
                                'label' => 'FB Profile Pic',
                                'route' => 'backend/fb-profile-pic',
                                'permission' => true,
                            ),
                            'socmed_poster' => array(
                                'label' => 'Socmed Image Poster',
                                'route' => 'backend/socmed-poster',
                                'permission' => true,
                            ),
                        )
		),
                'tshirt_design' => array(
			'label' => 'T-shirt Design',
			'icon_class' => 'fa fa-puzzle-piece',
			'permission' => false,
			'route' => 'backend/tshirt-design'
		),
                'admin_twitter_management' => array(
			'label' => 'Twitter Management',
			'icon_class' => 'fa fa-twitter',
			'permission' => false,
			'submenus' => array(
				'admin_twitter_setting' => array(
					'label' => 'Twitter Setting',
					'route' => 'backend/twitter-setting',
					'icon_class' => 'fa fa-gear',
					'permission' => true,
				),
				
			)
		),
		
            )

);
