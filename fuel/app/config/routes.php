<?php
return array(

    '_404_'   => 'welcome/404',    // The main 404 route

    //frontend
    '_root_'  => 'pages/frontend/home',  // The default route

    //new do you remember me?
    ':lang_code/do-you-remember-me' => 'pages/frontend/experience/experience_new_one',
    ':lang_code/do-you-remember-me/step-2' => 'pages/frontend/experience/experience_new_two',
    ':lang_code/do-you-remember-me/step-3' => 'pages/frontend/experience/experience_new_three',
    ':lang_code/do-you-remember-me/step-4' => 'pages/frontend/experience/experience_new_four',
    ':lang_code/do-you-remember-me/step-5' => 'pages/frontend/experience/experience_new_five',
    ':lang_code/do-you-remember-me/step-6' => 'pages/frontend/experience/experience_new_six',
    ':lang_code/do-you-remember-me/step-7' => 'pages/frontend/experience/experience_new_seven',
    ':lang_code/do-you-remember-me/step-8' => 'pages/frontend/experience/experience_new_eight',
    ':lang_code/do-you-remember-me/step-9' => 'pages/frontend/experience/experience_new_nine',
    ':lang_code/do-you-remember-me/step-10' => 'pages/frontend/experience/experience_new_ten',
    ':lang_code/do-you-remember-me/step-11' => 'pages/frontend/experience/experience_new_eleven',
    //end new do you remember me?
    ':lang_code/campaign-report-2015'         => 'pages/frontend/home/campaign',

    ':lang_code/about-dementia'               => 'pages/frontend/about/dementia',
    ':lang_code/about-world-alzheimers-month' => 'pages/frontend/about/wam',
    ':lang_code/get-involved'                 => 'pages/frontend/event/involved',
    'get-country'                             => 'event/api/eventlocator/getcountry',
    'get-city'                                => 'event/api/eventlocator/getcity',
    'get-event'                               => 'event/api/eventlocator/getevent',
    ':lang_code/home'                         => 'pages/frontend/home',
    ':lang_code/privacy-policy'               => 'pages/frontend/home/privacy',
    ':lang_code/add-event'                    => 'pages/frontend/home/add_event',

    // Installation
    'backend/installation' => 'backend/install/setup',

    // Backend routes
    'backend' => 'backend/dashboard', // The default route for backend
    'backend/sign-in' => 'backend/dashboard/sign_in',
    'backend/sign-out' => 'backend/dashboard/sign_out',
    'backend/change-password' => 'backend/dashboard/change_current_password',
    'backend/my-profile' => 'backend/dashboard/my_profile_form',
    'backend/no-permission' => 'error/no_permission/backend',

    // Admin Management //
    // Admin User
    'backend/admin-user' => 'adminmanagement/adminuser',
    'backend/admin-user/add' => 'adminmanagement/adminuser/form/0',
    'backend/admin-user/edit/(:num)' => 'adminmanagement/adminuser/form/$1',
    'backend/admin-user/delete/(:num)' => 'adminmanagement/adminuser/delete/$1',
    'backend/admin-user/reset-password/(:num)' => 'adminmanagement/adminuser/reset_password/$1',
    
    // Admin Role Permission
    'backend/admin-role-permission' => 'adminmanagement/adminrolepermission',
    'backend/admin-role-permission/add' => 'adminmanagement/adminrolepermission/form/0',
    'backend/admin-role-permission/edit/(:num)' => 'adminmanagement/adminrolepermission/form/$1',
    'backend/admin-role-permission/delete/(:num)' => 'adminmanagement/adminrolepermission/delete/$1',
    'backend/admin-role-permission/assign-admin/(:num)' => 'adminmanagement/adminrolepermission/assign_admin/$1',
    'backend/admin-role-permission/do-assign-admin/(:num)/(:num)' => 'adminmanagement/adminrolepermission/do_assign_admin/$1/$2/1',
    'backend/admin-role-permission/do-unassign-admin/(:num)/(:num)' => 'adminmanagement/adminrolepermission/do_assign_admin/$1/$2/0',
    'backend/admin-role-permission/set-permission/(:num)' => 'adminmanagement/adminrolepermission/set_permission/$1',
    
    // Basic Setting //
    'backend/setting' => 'adminmanagement/setting',

    // Facebook Management //
    //'backend/fb-participant' => 'facebookmanagement/participant',
    'backend/fb-setting' => 'facebookmanagement/setting',
    
    // Twitter Management //
    'backend/twitter-setting' => 'twittermanagement/setting',
	
    // Pages //
    'backend/pages' => 'pages/backend',
    'backend/pages/add' => 'pages/backend/form/0',
    'backend/pages/edit/(:num)' => 'pages/backend/form/$1',
    'backend/pages/delete/(:num)' => 'pages/backend/delete/$1',
	
    // Media Uploader //
    // FB Profile Picture
    'backend/mediauploader/event-image' => 'mediauploader/eventimage',
    'backend/mediauploader/event-image/upload' => 'mediauploader/eventimage/upload',
    
    // FB Profile Picture
    'backend/mediauploader/fb-profile-pic' => 'mediauploader/fbprofilepic',
    'backend/mediauploader/fb-profile-pic/upload' => 'mediauploader/fbprofilepic/upload',
    
    // Social Media Image Poster
    'backend/mediauploader/socmed-poster' => 'mediauploader/socmedposter',
    'backend/mediauploader/socmed-poster/upload' => 'mediauploader/socmedposter/upload',
    
    // PrintAd
    'backend/mediauploader/print-ad' => 'mediauploader/printad',
    'backend/mediauploader/print-ad/upload' => 'mediauploader/printad/upload',
    
    // PrintAd -Typographic
    'backend/mediauploader/print-ad-typo' => 'mediauploader/printadtypo',
    'backend/mediauploader/print-ad-typo/upload' => 'mediauploader/printadtypo/upload',
    
    // T-shirt Design
    'backend/mediauploader/tshirt-design' => 'mediauploader/tshirtdesign',
    'backend/mediauploader/tshirt-design/upload' => 'mediauploader/tshirtdesign/upload',
    
    // FB Cover
    'backend/mediauploader/fb-cover' => 'mediauploader/fbcover',
    'backend/mediauploader/fb-cover/upload' => 'mediauploader/fbcover/upload',
    
    // Event //
    // List
    'backend/event' => 'event/backend',
    'backend/event/add' => 'event/backend/form/0',
    'backend/event/edit/(:num)' => 'event/backend/form/$1',
    'backend/event/delete/(:num)' => 'event/backend/delete/$1',
    //'backend/event/rearrange/(:num)' => 'event/backend/rearrange/$1',
    
    // Region
    'backend/event/region' => 'event/backend/region',
    'backend/event/region/add' => 'event/backend/region/form/0',
    'backend/event/region/edit/(:num)' => 'event/backend/region/form/$1',
    //'backend/event/region/rearrange/(:num)' => 'event/backend/region/rearrange/$1',
    'backend/event/region/delete/(:num)' => 'event/backend/region/delete/$1',
    
    // Country
    'backend/event/country' => 'event/backend/country',
    'backend/event/country/add' => 'event/backend/country/form/0',
    'backend/event/country/edit/(:num)' => 'event/backend/country/form/$1',
    //'backend/event/country/rearrange/(:num)' => 'event/backend/region/rearrange/$1',
    'backend/event/country/delete/(:num)' => 'event/backend/country/delete/$1',    
    
    // City
    'backend/event/city' => 'event/backend/city',
    'backend/event/city/add' => 'event/backend/city/form/0',
    'backend/event/city/edit/(:num)' => 'event/backend/city/form/$1',
    //'backend/event/city/rearrange/(:num)' => 'event/backend/region/rearrange/$1',
    'backend/event/city/delete/(:num)' => 'event/backend/city/delete/$1',
    
    // Image
    'backend/event/image' => 'event/backend/image',
    'backend/event/image/add' => 'event/backend/image/form/0',
    'backend/event/image/edit/(:num)' => 'event/backend/image/form/$1',
    'backend/event/image/delete/(:num)' => 'event/backend/image/delete/$1',
    
    // Downloadable Assets //
    // Facebook Cover
    'backend/fb-cover' => 'downloadableassets/backend/fbcover',
    'backend/fb-cover/add' => 'downloadableassets/backend/fbcover/form/0',
    'backend/fb-cover/edit/(:num)' => 'downloadableassets/backend/fbcover/form/$1',
    'backend/fb-cover/rearrange/(:num)' => 'downloadableassets/backend/fbcover/rearrange/$1',
    'backend/fb-cover/delete/(:num)' => 'downloadableassets/backend/fbcover/delete/$1',
    
    // Facebook Profile Picture
    'backend/fb-profile-pic' => 'downloadableassets/backend/fbprofilepic',
    'backend/fb-profile-pic/add' => 'downloadableassets/backend/fbprofilepic/form/0',
    'backend/fb-profile-pic/edit/(:num)' => 'downloadableassets/backend/fbprofilepic/form/$1',
    'backend/fb-profile-pic/rearrange/(:num)' => 'downloadableassets/backend/fbprofilepic/rearrange/$1',
    'backend/fb-profile-pic/delete/(:num)' => 'downloadableassets/backend/fbprofilepic/delete/$1',
    
    // Print Ad
    'backend/print-ad' => 'downloadableassets/backend/printad',
    'backend/print-ad/add' => 'downloadableassets/backend/printad/form/0',
    'backend/print-ad/edit/(:num)' => 'downloadableassets/backend/printad/form/$1',
    'backend/print-ad/rearrange/(:num)' => 'downloadableassets/backend/printad/rearrange/$1',
    'backend/print-ad/delete/(:num)' => 'downloadableassets/backend/printad/delete/$1',
    
    // Print Ad -Typo
    'backend/print-ad-typo' => 'downloadableassets/backend/printadtypo',
    'backend/print-ad-typo/add' => 'downloadableassets/backend/printadtypo/form/0',
    'backend/print-ad-typo/edit/(:num)' => 'downloadableassets/backend/printadtypo/form/$1',
    'backend/print-ad-typo/rearrange/(:num)' => 'downloadableassets/backend/printadtypo/rearrange/$1',
    'backend/print-ad-typo/delete/(:num)' => 'downloadableassets/backend/printadtypo/delete/$1',
    
    // Social Media Image Poster
    'backend/socmed-poster' => 'downloadableassets/backend/socmedposter',
    'backend/socmed-poster/add' => 'downloadableassets/backend/socmedposter/form/0',
    'backend/socmed-poster/edit/(:num)' => 'downloadableassets/backend/socmedposter/form/$1',
    'backend/socmed-poster/rearrange/(:num)' => 'downloadableassets/backend/socmedposter/rearrange/$1',
    'backend/socmed-poster/delete/(:num)' => 'downloadableassets/backend/socmedposter/delete/$1',
    
    // T-shirt Design
    'backend/tshirt-design' => 'downloadableassets/backend/tshirtdesign',
    'backend/tshirt-design/add' => 'downloadableassets/backend/tshirtdesign/form/0',
    'backend/tshirt-design/edit/(:num)' => 'downloadableassets/backend/tshirtdesign/form/$1',
    'backend/tshirt-design/rearrange/(:num)' => 'downloadableassets/backend/tshirtdesign/rearrange/$1',
    'backend/tshirt-design/delete/(:num)' => 'downloadableassets/backend/tshirtdesign/delete/$1',
);