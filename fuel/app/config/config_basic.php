<?php
// can be more or less
return array(
	'app_name' => 'World Alzheimer\'s Month',
	'google_analytic_code' => '',
	'account_facebook' => 'https://www.facebook.com/alzheimersdiseaseinternational',
	'account_twitter' => 'https://twitter.com/alzdisint',
        'account_youtube' => 'https://www.youtube.com/user/alzdisint',
        'website_adi' => 'http://www.alz.co.uk',
        'map_adi' => 'https://www.google.com/maps/place/Alzheimers+Disease+International/@51.503071,-0.101451,17z/data=!3m1!4b1!4m2!3m1!1s0x0:0xafcd77dc363582f2'
);
