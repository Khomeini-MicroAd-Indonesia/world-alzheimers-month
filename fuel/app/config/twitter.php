<?php

return array(
    // twitter api consumer configurations 
    'oauth_access_token'        => '',
    'oauth_access_token_secret' => '',
    'consumer_key'              => '',
    'consumer_secret'           => '',
);