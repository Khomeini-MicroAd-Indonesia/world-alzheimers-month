<?php

class Util_Email {
    
	/*
		Set email list value
		
		@param Fuelphp Core Email instance $email
		@param string $type
		@param string/array $value
		@return Fuelphp Core Email instance
		
		Example for $type
			$type = "from";
				OR
			$type = "to";
				OR
			$type = "reply_to";
		
		Example for $value
			$value = 'name@email.com';
				OR
			$value = array(
				'email' => 'name@email.com',
				'name' => 'Name'
			);
				OR
			$value = array('name@email.com','name@email.com');
				OR
			$value = array(
				array(
					'email' => 'name@email.com',
					'name' => 'Name'
				),
				array(
					'email' => 'name@email.com',
					'name' => 'Name'
				)
			);
	*/
	private static function _set_email_list($email, $type, $value) {
		if (is_string($value)) {
			$email->$type($value);
		} else if(isset($value->email)) {
			if (isset($value->name)) {
				$email->$type($value->email, $value->name);
			} else {
				$email->$type($value->email);
			}
		} else if(is_array($value)) {
			foreach ($value as $temp) {
				if (is_string($temp)) {
					$email->$type($temp);
				} else if(isset($temp->email)) {
					if (isset($temp->name)) {
						$email->$type($temp->email, $temp->name);
					} else {
						$email->$type($temp->email);
					}
				}
			}
		}
		return $email;
	}
	
	/*
		Process email queue task
		
		@return booelan
	*/
	public static function process() {
		$max_daily = \Config::get('config_cms.max_email_sent_daily');
		$max_per_task = \Config::get('config_cms.max_email_sent_per_task');
		// Get Count of email which sent today
		$today_sent_cnt = \Model_MailQueues::query()
			->where('updated_at', '>=', date('Y-m-d').' 00:00:00')
			->where('updated_at', '<=', date('Y-m-d').' 23:59:59')
			->where('email_sent', 1)
			->count();
		// Give the limit of total of daily email
		if (($today_sent_cnt + $max_per_task) > $max_daily) {
			$max_per_task = $max_daily - $today_sent_cnt;
		}
		// Process when the queue of email more than 0
		if ($max_per_task > 0) {
			\Log::debug('Start task to send email');
			$mq = \Model_MailQueues::in_queues()
				->limit($max_per_task)
				->get();
			foreach ($mq as $m) {
				// Log each queue
				\Log::debug('Start send email (Queue ID = '.$m->id.')');
				
				// Init email
				$email = \Email::forge();
				// Set Email From
				$email = self::_set_email_list($email, 'from', json_decode($m->email_from));
				// Set Email To
				$email = self::_set_email_list($email, 'to', json_decode($m->email_to));
				// Set Email Reply To
				if (!empty($m->email_reply_to)) {
					$email = self::_set_email_list($email, 'reply_to', json_decode($m->email_reply_to));
				}
				// Set Subject
				$email->subject($m->email_subject);
				// Set Email Body
				$body = '';
				if (!empty($m->email_view)) { /* HTML BODY */
					$email_data = (is_array(json_decode($m->email_data, TRUE))) ? json_decode($m->email_data, TRUE) : array();
					try {
						$body = \View::forge($m->email_view, $email_data);
						$email->html_body($body);
					} catch(\Exception $ex) {
						\Log::error($ex->getMessage());
					}
				}
				if (!empty($m->email_body)) { /* TEXT BODY */
					if (empty($body)) {
						$body = $m->email_body;
						$email->body($body);
					}
				}
				// Sending an Email
				try {
					$email->send();
					self::unqueue_send_email($m->id);
					\Log::debug('Success send email (Queue ID = '.$m->id.')');
					sleep(120);
					return true;
				} catch(\EmailValidationFailedException $ex) {
					\Log::error($ex->getMessage().' (Queue ID = '.$m->id.')');
					return false;
				} catch(\EmailSendingFailedException $ex) {
					\Log::error($ex->getMessage().' (Queue ID = '.$m->id.')');
					return false;
				} catch(\Exception $ex) {
					\Log::error($ex->getMessage().' (Queue ID = '.$m->id.') ('.json_encode($email).') ');
					return false;
				}
			}
			\Log::debug('End task to send email');
		}
    }
	
	/*
		Queue the email into list
		
		@param array $data
		@return void
		
		Example for $data
		$data = array(
			'email_from' => '',
			'email_to' => '',
			'email_subject' => '',
			
			'email_reply_to' => '', // Optional
			
			// Optional
			'email_body' => '',
				OR (to use html body)
			'email_data' => '',
			'email_view' => '',
		);
	*/
    public static function queue_send_email($data) {
		$mq = \Model_MailQueues::forge(array(
			'email_from' => json_encode($data['email_from']),
			'email_to' => json_encode($data['email_to']),
			'email_subject' => $data['email_subject'],
		));
		if (isset($data['email_reply_to'])) {
			$mq->email_reply_to = json_encode($data['email_reply_to']);
		}
		if (isset($data['email_body'])) {
			$mq->email_body = $data['email_body'];
		}
		if (isset($data['email_data'])) {
			$mq->email_data = json_encode($data['email_data']);
		}
		if (isset($data['email_view'])) {
			$mq->email_view = $data['email_view'];
		}
		try {
			$mq->save();
		} catch (\Exception $ex) {
			\Log::error($ex->getMessage());
		}
	}
	
	/*
		Unqueue the email from list after process the task
		
		@param numeric $id
		@return void
	*/
    private static function unqueue_send_email($id) {
		$mq = \Model_MailQueues::find($id);
		$mq->email_sent = 1;
		try {
			$mq->save();
		} catch (\Exception $ex) {
			\Log::error($ex->getMessage());
		}
	}
}