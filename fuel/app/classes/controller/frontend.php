<?php

use FacebookManagement\Util_Api;

class Controller_Frontend extends Controller_Mama
{
	protected $_is_fb_connect = false;
	protected $_data_template = array(
		'meta_title' => '',
		'meta_desc' => '',
		'frontend_menus' => array(),
		'menu_parent_key' => '',
		'menu_current_key' => '',
	);
	protected $_check_lang = true;
	protected $_valid_lang = array(
		'eng' => array(
			'active' => false,
			'label' => 'ENG',
			'link' => ''
		),
		'ina' => array(
			'active' => false,
			'label' => 'INA',
			'link' => ''
		),
		'esp' => array(
			'active' => false,
			'label' => 'ESP',
			'link' => ''
		)
	);
	protected $_current_lang = 'eng';
	
	public function before() {
		parent::before();
		
		$_change_lang_url = '/home';
		if ($this->_check_lang) {
			$_is_lang_valid = true;
			$temp_lang = $this->param('lang_code');
			if (empty($temp_lang)) {
				$_is_lang_valid = false;
			} else {
				if (isset($this->_valid_lang[$temp_lang])) {
					$this->_current_lang = $temp_lang;
					$this->_valid_lang[$temp_lang]['active'] = true;
				} else {
					$_is_lang_valid = false;
					\Log::error('Lang code '.$temp_lang.' is Invalid!');
				}
			}
			if (!$_is_lang_valid) {
				\Response::redirect(\Uri::base().$this->_current_lang.'/home');
			}
			$temp = '';
			foreach (\Uri::segments() as $key => $value) {
				if ($key > 0) {
					$temp .= '/'.$value;
				} else {
					if (!isset($this->_valid_lang[$value])) {
						break;
					}
				}
			}
			if (!empty($temp)) {
				$_change_lang_url = $temp;
                unset($temp);
			}
		}
		foreach ($this->_valid_lang as $key => $value) {
                    $this->_valid_lang[$key]['link'] = \Uri::base().$key.$_change_lang_url;
		}
		$this->_data_template['valid_lang'] = $this->_valid_lang;
		$this->_data_template['current_lang'] = $this->_current_lang;
		
		\Config::set('language', $this->_current_lang);
		\Lang::load($this->_current_lang);
		
		$this->fetch_config_data('facebook');
		$this->fetch_config_data('twitter');
		
		session_start();
		Util_Api::initSession(['scope' => 'email, user_photos']);
		$this->_is_fb_connect = Util_Api::getLogin();
                $this->_data_template['fb_login_url'] = Util_Api::getLoginUrl();

		$this->_data_template['frontend_menus'] = array(
			'home' => array(
				'lang_code' => 'mn_home',
				'label' => 'Home',
				'route' => \Uri::base().$this->_current_lang.'/home',
				'menu_id' => 'menu-',
				'menu_class' => 'menu-item',
			),
//			'do_you_remember_me' => array(
//				'lang_code' => 'mn_do_you_remember_me',
//				'label' => 'Do You Remember Me?',
//				'route' => \Uri::base().$this->_current_lang.'/do-you-remember-me',
//				'menu_id' => 'menu-',
//				'menu_class' => 'menu-item',
//			),

            'campaign_report_2015' => array(
                'lang_code' => 'mn_campaign_report_2015',
                'label' => 'Campaign Report 2015',
                'route' => \Uri::base().$this->_current_lang.'/campaign-report-2015',
                'menu_id' => 'menu-',
                'menu_class' => 'menu-item',
            ),
			'about_world_alz_month' => array(
				'lang_code' => 'mn_about_wam',
				'label' => 'About World Alzheimer&#39;s Month',
				'route' => \Uri::base().$this->_current_lang.'/about-world-alzheimers-month',
				'menu_id' => 'menu-',
				'menu_class' => 'menu-item',
			),
			'about_dementia' => array(
				'lang_code' => 'mn_about_dementia',
				'label' => 'About Dementia',
				'route' => \Uri::base().$this->_current_lang.'/about-dementia',
				'menu_id' => 'menu-',
				'menu_class' => 'menu-item',
			),
			'get_involved' => array(
				'lang_code' => 'mn_get_involved',
				'label' => 'Get Involved',
				'route' => \Uri::base().$this->_current_lang.'/get-involved',
				'menu_id' => 'menu-',
				'menu_class' => 'menu-item',
			),
		);
	}
	
	protected function set_meta_info($current_route) {
		$this->_data_template['meta_title'] = \Config::get('config_basic.app_name');
		$model_page = \Pages\Model_Pages::query()
			->where('url_path', $current_route)
			->where('status', 1)
			->get_one();
		if (!empty($model_page)) {
			$this->_data_template['page_title'] = $model_page->title;
            $this->_data_template['page_content'] = $model_page->content;
			if (strlen($model_page->meta_title) > 0) {
				$this->_data_template['meta_title'] .= ' - '.$model_page->meta_title;
			}
			$this->_data_template['meta_desc'] = $model_page->meta_desc;
		}
	}
}
