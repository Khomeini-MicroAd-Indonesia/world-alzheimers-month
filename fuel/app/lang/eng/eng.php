<?php
/**
 * Created by PhpStorm.
 * User: Albert
 * Date: 5/18/15
 * Time: 11:04 AM
 */
return array(
/*------------------------------default--------------------------------*/
    'txt_here' => 'here',
    'txt_click' => 'Click',
    'txt_donate' => 'Donate',
    'txt_time' => 'Time',
    'txt_download' => 'Download our materials',
    'txt_button_find_your_national' => 'Find your national Alzheimer association',
/*---------------------------template---------------------------------*/
    'txt_footnote_campaign' => 'A campaign by',
    'txt_footnote_supported' => 'Proudly supported by',
    'txt_footnote_copyright' => 'Copyright &copy; World Alzheimer&#39;s Month 2015',
    'txt_footnote_privacy' => 'Privacy Policy',
    'txt_footnote_copyright_end' => 'About This Site',
    'txt_footnote_contact' => 'Contact',
/* -----------------------------menu-----------------------------------*/
    'mn_home' => 'Home',
    'mn_do_you_remember_me' => 'Do You Remember Me?',
    'mn_campaign_report_2015' => 'Campaign Report 2015',
    'mn_about_wam' => 'About World Alzheimer&#39;s Month',
    'mn_about_dementia' => 'About Dementia',
    'mn_get_involved' => 'Get Involved',
/* --------------------------home----------------------------------*/
    'txt_main_menu_banner' => 'Welcome&#33;',
    'txt_main_text_banner' => 'September is World Alzheimer&#39;s Month&#8482;, an international campaign to raise awareness and challenge stigma.&nbsp;It&#39;s a time for action, a global movement united by its call for change, but also a time to reflect on the impact of dementia, a disease that will affect more and more people as the years pass.',
    'txt_main_title_impact' => 'The global impact of dementia',
    'txt_title_people_tweeting' => 'Spread the word on Twitter',
/* ---------------------experience---------------------------*/
    'txt_title_do_you' => 'Do you remember me?',
    'txt_desc_do_you' => 'Every person is unique and dementia affects people differently &#45; no two people will have symptoms that develop in exactly the same way,<br/>but there are broad similarities between them all. The most common signs are memory loss and the loss of practical abilities, which can <br/>lead to withdrawal from work or social activities. This game gives an idea of what it might be like to live with the symptoms of dementia,<br/>in this case, finding it difficult to recognise friends and family.',
    'txt_desc_do_you_alt' => 'Cicero, a famous roman philosopher once said &quot;Memory is the treasury and guardian of all things&quot;. But what if our memory slowly faded like Alzheimers patient? So let&#39;s test how well you remember your memory. Just follow this instruction below :',
    'txt_desc_do_you_alt2' => 'Let&#39;s see how well you do on our memory test.',
    'txt_desc_do_you_login' => 'Just login to start',
    'txt_coming_soon' => 'Coming Soon',
/*-------------------experience two---------------------------*/
    'txt_title_instruction' => 'Instructions',
    'txt_instruction_step_1' => 'Press start to begin the game.',
    'txt_instruction_step_2' => 'Name the faces from left to right.',
    'txt_instruction_step_3' => 'You must guess all of their names in 2 minutes.',
    'txt_instruction_step_4' => 'The difficulties will increase on each photo.',

    'txt_text_friends' => 'Type your friend&#39;s name here :',
    'txt_minutes' => 'Minutes',
/*------------------about_wam----------------------------------------*/
    'txt_title_facts' => 'The facts',
    'txt_desc_fact_1a' => 'World Alzheimer&#39;s Month was launched in September 2012.',
    'txt_desc_fact_1b' => 'World Alzheimer&#39;s Day is on 21 September each year.',
    'txt_desc_fact_2a' => 'There are several ways you can participate in World Alzheimer&#39;s Month. You could organise your own event or visit our',
    'txt_desc_fact_find_event' => 'Find an Event',
    'txt_desc_fact_2b' => 'page to join an activity organised by your national Alzheimer association. You can also spread the word on social media.',
    'txt_desc_fact_3' => 'World Alzheimer&#39;s Month unites opinion leaders, people with dementia, caregivers and family, medical professionals, researchers and the media from all around the world.',
    'txt_desc_fact_4' => 'World Alzheimer&#39;s Month is also an excellent opportunity to have fun!',
    'txt_desc_fact_5' => 'World Alzheimer&#39;s Month provides an opportunity for Alzheimer associations around the world to gain recognition and credibility for the work they do, placing themselves in a stronger position to influence opinion leaders and governments.',
    'txt_desc_fact_6' => 'Each year, more and more countries are participating in World Alzheimer&#39;s Month events and in many areas, dementia awareness is growing.',

    'txt_title_remember_me' => 'Remember Me',
    'txt_desc_remember_me_1' => 'The theme for this year&#39;s World Alzheimer&#39;s Month campaign is Remember Me. We&#39;re encouraging people all around the world to learn to spot the signs of dementia, but also not to forget about loved ones who are living with dementia, or who may have passed away. The impact of September&#39;s campaign is growing, but the stigmatisation and misinformation that surrounds dementia remains a global problem.',
    'txt_desc_remember_me_2' => 'Find out more about our previous campaigns',

    'txt_title_about_wam' => 'About Alzheimer&#39;s Disease International&nbsp;(ADI)',
    'txt_desc_about_wam_1' => 'World Alzheimer&#39;s Month is an initiative of Alzheimer&#39;s Disease International&nbsp;(ADI). Each year, ADI coordinates and organises the World Alzheimer&#39;s Month campaign.',
    'txt_desc_about_wam_2' => 'ADI is the umbrella organization of over 80 Alzheimer&#39;s associations around the globe. We aim to help establish and strengthen our associations through the world and to raise global awareness about Alzheimer&#39;s disease and all other causes of dementia. We believe that the key to winning the fight against dementia lies in a unique combination of global solutions and local knowledge.',
    'txt_desc_about_wam_3' => 'As such, we work locally, by empowering national Alzheimer association to promote and offer care and support for people with dementia and their caregivers, and globally to focus attention on the epidemic and campaign for policy change from governments and the World Health Organization, with whom we gained consultative status with in 2012. These are the pillars of our mission.',

    /*---------------------- get involved ------------------------------*/
    'txt_title_spread_word' => 'Spread the word',
    'txt_desc_spread_word_1' => 'Help us spread the word about World Alzheimer&#39;s Month with these materials.<br>You can also use our letter template to inform community leaders, politicians, businesses or other organisations about the campaign.<br>Follow the links below to download.',
    'txt_desc_spread_word_1a' => 'Help us spread the word about World Alzheimer&#39;s Month with these materials.',
    'txt_desc_spread_word_1b' => 'You can also use our letter template to inform community leaders, politicians, businesses or other organisations about the campaign. Follow the links below to download.',

    'txt_title_find_event' => 'Find an event',
    'txt_desc_find_event' => 'During the month of September there will be thousands of events taking place all around the world to mark World Alzheimer’s Month. To find an event in your country please use the drop down list below, or contact your local Alzheimer association',
    'txt_desc_find_event_2' => 'If you’re planning an event of your own, be sure to add it to our list.',
    'txt_subtitle_find_event' => 'World Alzheimer&#39;s Month events around you',

    'txt_region' => 'Select Region',
    'txt_country' => 'Select Country',
    'txt_city' => 'Select City',
    'txt_btn_add_event' => 'Add an event',
    'txt_title_support_us' => 'Support us',
    'txt_desc_support_us' => 'ADI relies on donations in order to carry out its work of <br/>strengthening Alzheimer associations worldwide <br/>and raising awareness about the global impact of dementia.',
    'txt_desc_support_us_2' => 'By making a donation today you can make a real difference <br/>to people around the world living with Alzheimer&#39;s disease and dementia.',
    'txt_desc_support_us_3' => 'if you would like to make a donation',

    'txt_add_event' => 'Add event',
    'txt_event_name'=> 'Event Name',
    'txt_email' => 'E-mail',
    'txt_event_desc' => 'Event Description',
    'txt_date' => 'Date',
    'txt_time' => 'Time',
    'txt_country' => 'Country',
    'txt_city' => 'City',
    'txt_location' => 'Location',
    'txt_organization' => 'Organisation',
    'txt_web_link' => 'Web Link',



    /*-------------------- about dementia -------------------------------*/
    'txt_title_faq' => 'Frequently Asked Questions (FAQ)',
    'txt_dementia_q1' => 'What is dementia?',
    'txt_dementia_a1' => 'Dementia is a collective name for progressive degenerative brain syndromes which affect memory, thinking, behaviour and emotion. Alzheimer&#39;s disease is the most common type of dementia, affecting up to 90% of people living with dementia. But there are a large number of conditions which cause the symptoms of dementia, as a result of changes that happen in the brain and the ultimate loss of nerve cells (neurons). Other types of dementia include vascular dementia, dementia with Lewy bodies and fronto-temporal dementia (including Pick&#39;s disease).',
    'txt_dementia_a1a' => 'Dementia is a collective name for progressive degenerative brain syndromes which affect memory, thinking, behaviour and emotion. Alzheimer&#39;s disease is the most common type of dementia, affecting up to 90% of people living with dementia. ',
    'txt_dementia_a1b' => 'But there are a large number of conditions which cause the symptoms of dementia, as a result of changes that happen in the brain and the ultimate loss of nerve cells (neurons). Other types of dementia include vascular dementia, dementia with Lewy bodies and fronto-temporal dementia (including Pick&#39;s disease).',
    'txt_dementia_q2' => 'What are the early symptoms of dementia?',
    'txt_dementia_a2' => 'Every person is unique and dementia affects people differently - no two people will have symptoms that develop in exactly the same way. An individual&#39;s personality, general health and social situation are all important factors in determining the impact of dementia on him or her. Symptoms vary between Alzheimer&#39;s disease and other types of dementia, but there are broad similarities between them all. The most common signs are memory loss and the loss of practical abilities, which can lead to withdrawal from work or social activities. If you think that these problems are affecting your daily life, or the life of someone you know, you should talk to your doctor.',
    'txt_dementia_a2a' => 'The most common signs are memory loss and the loss of practical abilities, which can lead to withdrawal from work or social activities. If you think that these problems are affecting your daily life, or the life of someone you know, you should talk to your doctor.',
    'txt_dementia_a2b' => 'Every person is unique and dementia affects people differently - no two people will have symptoms that develop in exactly the same way. An individual&#39;s personality, general health and social situation are all important factors in determining the impact of dementia on him or her.Symptoms vary between Alzheimer&#39;s disease and other types of dementia, but there are broad similarities between them all.',
    'txt_dementia_q3' => 'My mother has Alzheimer&#39;s disease. Will I get it?',
    'txt_dementia_a3' => 'There are a few very rare cases where Alzheimer&#39;s disease does run in families. In these cases there is a direct link between an inherited mutation in one gene and the onset of the disease. These tend to be cases of &#39;early onset&#39; Alzheimer&#39;s disease, which affects those under the age of 65. In these cases, the probability that close family members (brothers, sisters and children) will develop Alzheimer&#39;s disease is one in two. Most cases of Alzheimer&#39;s disease are not of the type that is passed on directly in this way. If a family member has a normal form of Alzheimer&#39;s disease, the risk to close relatives is around three times higher than the risk for a person of a similar age who has no family history of the disease. It is thought that in these cases a person&#39;s genes may contribute to the development of the disease but do not cause it directly.',
    'txt_dementia_a3a' => 'There are a few very rare cases where Alzheimer&#39;s disease does run in families. In these cases there is a direct link between an inherited mutation in one gene and the onset of the disease. These tend to be cases of &#39;early onset&#39; Alzheimer&#39;s disease, which affects those under the age of 65. In these cases, the probability that close family members (brothers, sisters and children) will develop Alzheimer&#39;s disease is one in two. Most cases of Alzheimer&#39;s disease are not of the type that is passed on directly in this way.',
    'txt_dementia_a3b' => 'If a family member has a normal form of Alzheimer&#39;s disease, the risk to close relatives is around three times higher than the risk for a person of a similar age who has no family history of the disease. It is thought that in these cases a person&#39;s genes may contribute to the development of the disease but do not cause it directly.',
    'txt_dementia_q4' => 'Is there a cure?',
    'txt_dementia_a4' => 'There is currently no cure for Alzheimer&#39;s disease or for most other causes of dementia, nor can a cure be expected in the near future. Researchers are still at the stage of developing drugs that will slow down the progression of the disease, at least in some cases. They still do not know how to prevent the disease from occurring, how to stop its progression, or how to reverse its effects. It is hoped that more research into the causes of dementia will make effective treatment possible.',
    'txt_dementia_a4a' => 'There is currently no cure for Alzheimer&#39;s disease or for most other causes of dementia, nor can a cure be expected in the near future. Researchers are still at the stage of developing drugs that will slow down the progression of the disease, at least in some cases.',
    'txt_dementia_a4b' => 'They still do not know how to prevent the disease from occurring, how to stop its progression, or how to reverse its effects. It is hoped that more research into the causes of dementia will make effective treatment possible.',
    'txt_dementia_q5' => 'Are there any drug treatments for Alzheimer&#39;s disease?',
    'txt_dementia_a5' => 'Although there are no drugs that can cure Alzheimer&#39;s disease, there are a number of drug treatments that can help some people with Alzheimer&#39;s disease. The currently available treatments can slow down the progression of the disease in some cases for periods between 6 and 18 months. The main class of such compounds is the cholinesterase inhibitors. Other kinds of drugs are occasionally useful for controlling some of the symptoms of Alzheimer&#39;s disease, such as sleeplessness and agitation. In general, however, the use of drugs such as sleeping pills or tranquillisers should be kept to a minimum if someone has Alzheimer&#39;s disease, as they can cause increased confusion. Non-drug treatments, including practical and emotional support, are important and effective in helping people with dementia and carers.',
    'txt_dementia_a5a' => 'Although there are no drugs that can cure Alzheimer&#39;s disease, there are a number of drug treatments that can help some people with Alzheimer&#39;s disease.',
    'txt_dementia_a5b' => 'The currently available treatments can slow down the progression of the disease in some cases for periods between 6 and 18 months. The main class of such compounds is the cholinesterase inhibitors. Other kinds of drugs are occasionally useful for controlling some of the symptoms of Alzheimer&#39;s disease, such as sleeplessness and agitation. In general, however, the use of drugs such as sleeping pills or tranquillisers should be kept to a minimum if someone has Alzheimer&#39;s disease, as they can cause increased confusion. Non-drug treatments, including practical and emotional support, are important and effective in helping people with dementia and carers.',
    'txt_dementia_q6' => 'Can Alzheimer&#39;s disease and other forms of dementia be prevented?',
    'txt_dementia_a6' => 'Not enough is known about the causes of Alzheimer&#39;s disease for any specific preventative measures to be recommended. Although Alzheimer&#39;s disease and other forms of dementia are more common with increasing age, the trigger for the characteristic changes that occur in the brain tissue of people with Alzheimer&#39;s disease is not known. Genes are thought to play a part in the development of most cases of Alzheimer&#39;s disease. In rare cases, abnormal genes actually cause the disease. Much more commonly, genes are believed only to contribute to a person&#39;s susceptibility to the disease. It seems that, at least in some cases, factors in the environment may be necessary to trigger the illness. Although there are no specific preventative measures, what can be recommended is a healthy lifestyle - eating a healthy diet and staying physically, mentally and socially active. There is increasing research evidence to suggest that having a healthy lifestyle helps to reduce an individual&#39;s risk.',
    'txt_dementia_a6a' => 'Not enough is known about the causes of Alzheimer&#39;s disease for any specific preventative measures to be recommended. Although Alzheimer&#39;s disease and other forms of dementia are more common with increasing age, the trigger for the characteristic changes that occur in the brain tissue of people with Alzheimer&#39;s disease is not known. Genes are thought to play a part in the development of most cases of Alzheimer&#39;s disease. In rare cases, abnormal genes actually cause the disease.',
    'txt_dementia_a6b' => 'Much more commonly, genes are believed only to contribute to a person&#39;s susceptibility to the disease. It seems that, at least in some cases, factors in the environment may be necessary to trigger the illness. Although there are no specific preventative measures, what can be recommended is a healthy lifestyle - eating a healthy diet and staying physically, mentally and socially active. There is increasing research evidence to suggest that having a healthy lifestyle helps to reduce an individual&#39;s risk.',

    'txt_title_signs' => '10 warning signs of dementia',
    'txt_subtitle_sign1' => '1. Memory loss that disrupts daily life',
    'txt_subtitle_sign2' => '2. Challenges in planning or solving problems',
    'txt_subtitle_sign3' => '3. Difficulty completing familiar tasks at home, at work or at leisure',
    'txt_subtitle_sign4' => '4. Confusion with time or place',
    'txt_subtitle_sign5' => '5. Trouble understanding visual images and spatial relationships',
    'txt_subtitle_sign6' => '6. New problems with words in speaking or writing',
    'txt_subtitle_sign7' => '7. Misplacing things and losing the ability to retrace steps',
    'txt_subtitle_sign8' => '8. Decreased or poor judgment',
    'txt_subtitle_sign9' => '9. Withdrawal from work or social activities',
    'txt_subtitle_sign10' => '10. Changes in mood and personality',
    'txt_desc_sign1' => 'One of the most common signs of Alzheimer&#39;s is memory loss, especially forgetting recently learned information. Others include forgetting important dates or events; asking for the same information over and over; increasingly needing to rely on memory aids (e.g., reminder notes or electronic devices) or family members for things they used to handle on their own.',
    'txt_desc_sign2' => 'Some people may experience changes in their ability to develop and follow a plan or work with numbers. They may have trouble following a familiar recipe or keeping track of monthly bills. They may have difficulty concentrating and take much longer to do things than they did before.',
    'txt_desc_sign3' => 'People with Alzheimer&#39;s often find it hard to complete daily tasks. Sometimes, people may have trouble driving to a familiar location, managing a budget at work or remembering the rules of a favorite game.',
    'txt_desc_sign4' => 'People with Alzheimer&#39;s can lose track of dates, seasons and the passage of time. They may have trouble understanding something if it is not happening immediately. Sometimes they may forget where they are or how they got there. ',
    'txt_desc_sign5' => 'For some people, having vision problems is a sign of Alzheimer&#39;s. They may have difficulty reading, judging distance and determining color or contrast, which may cause problems with driving.',
    'txt_desc_sign6' => 'People with Alzheimer&#39;s may have trouble following or joining a conversation. They may stop in the middle of a conversation and have no idea how to continue or they may repeat themselves. They may struggle with vocabulary, have problems finding the right word or call things by the wrong name (e.g. calling a "watch" a "hand-clock").',
    'txt_desc_sign7' => 'A person with Alzheimer&#39;s disease may put things in unusual places. They may lose things and be unable to go back over their steps to find them again. Sometimes, they may accuse others of stealing. This may occur more frequently over time.',
    'txt_desc_sign8' => 'People with Alzheimer&#39;s may experience changes in judgment or decision-making. For example, they may use poor judgment when dealing with money, giving large amounts to telemarketers. They may pay less attention to grooming or keeping themselves clean. ',
    'txt_desc_sign9' => 'A person with Alzheimer&#39;s may start to remove themselves from hobbies, social activities, work projects or sports. They may have trouble keeping up with a favorite sports team or remembering how to complete a favorite hobby. They may also avoid being social because of the changes they have experienced. ',
    'txt_desc_sign10' => 'The mood and personalities of people with Alzheimer&#39;s can change. They can become confused, suspicious, depressed, fearful or anxious. They may be easily upset at home, at work, with friends or in places where they are out of their comfort zone.',
    'txt_source' => 'Source',

    'txt_title_find_help' => 'Finding help',
    'txt_desc_finding_help' => 'If you or a loved one is living with dementia, you are not alone. National Alzheimer associations offer help and support to people with dementia and their carers. Contact the Alzheimer association in your country, or if there is no association in your country, you can try contacting one in a neighbouring country. These organisations offer friendship, support and advice, and are a lifeline for many people.',
    'txt_desc_finding_help1' => 'to find your national Alzheimer association.',
    'txt_desc_finding_help2' => 'If you or a loved one is living with dementia, you are not alone. National Alzheimer associations offer help and support to people with dementia and their carers.',
    'txt_desc_finding_help3' => 'Contact the Alzheimer association in your country, or if there is no association in your country, you can try contacting one in a neighbouring country.',
    'txt_desc_finding_help4' => 'These organisations offer friendship, support and advice, and are a lifeline for many people.',
    'txt_desc_finding_help5' => 'to find your national Alzheimer association.',

    /*---------------------------privacy policy -----------------------------------------*/
    'txt_title_privacy' => 'Privacy Policy',
    'txt_desc_privacy1' => 'Alzheimer&#39;s Disease International (ADI) will only use any personal information you send us for the purposes for which you provide it. For example, we may collect your information, where you choose to provide it to us, for administration, fundraising, and development of the web site, products and services. All employees and agents who have access to your personal data and are associated with the handling of that data are obliged to respect the confidentiality of your personal data. ADI tries to keep the information we have about you accurate and up to date. If, however, you find errors or inaccuracies in your data, we will erase, complete or amend that information upon request.',
    'txt_desc_privacy2' => 'The internet (IP) address, browser type, and any referral information provided by your browser when you use this site are logged. This information will be used by ADI to collect broad usage statistics, to help us improve the site and diagnose technical problems.',
    'txt_desc_privacy3' => 'If you use the &quot;Do you remember me&quot; activity and log in to your Facebook account, you are granting this website access to your photos and Facebook profile information. This information will only to be used for the experience on this website and will not be retained. Basic information about your use of this website will also be transmitted to and stored by Facebook.&nbsp',
    'txt_desc_privacy3_link' => 'Facebook&#39;s policies',
    'txt_desc_privacy3a' => 'apply to the data collected.',
    'txt_desc_privacy4' => 'This website uses Google Analytics, a web analytics service provided by Google, Inc. Google Analytics uses &quot;cookies&quot;, which are text files placed on your computer, to help the website analyze how users use the site. The information generated by the cookie about your use of the website (including your IP address) will be transmitted to and stored by Google on servers in the United States. Google will use this information for the purpose of evaluating your use of the website, compiling reports on website activity for website operators and providing other services relating to website activity and internet usage. Google may also transfer this information to third parties where required to do so by law, or where such third parties process the information on Google&#39;s behalf. Google will not associate your IP address with any other data held by Google. You may refuse the use of cookies by selecting the appropriate settings on your browser. By using this website, you consent to the processing of data about you by Google in the manner and for the purposes set out above. Site search is also handled by Google.&nbsp;',
    'txt_desc_privacy4_link' => 'Google&#39;s privacy policy',
    'txt_desc_privacy4a' => 'applies to the data collected.',
    'txt_desc_privacy5' => 'If you have any concerns about the use of your personal data, please',
    'txt_contact_us' => 'contact us',

    /*-----------------------------new experience----------------------------------------*/
    'txt_experience_new_1_content_1' => 'Every person is unique and dementia affects people differently<br/>- no two people will have symptoms that develop in exactly<br/> the same way, but there are broad similarities between them <br/>all.',
    'txt_experience_new_1_content_2' => 'The most common signs are memory loss and the loss of<br/> practical abilities, which can lead to withdrawal from work or<br/> social activities. ',
    'txt_experience_new_1_content_3' => 'This game gives an idea of what it might be like to experience<br/> the symptoms of dementia, in this case, finding it difficult to<br/> recognise friends and family or to recall memories. ',

    'txt_experience_new_1_alt_content_1' => 'Every person is unique and dementia affects people differently - no two people will have symptoms that develop in exactly the same way,<br/> but there are broad similarities between them all. The most common signs are memory loss and the loss of practical abilities, which can<br/> lead to withdrawal from work or social activities.',
    'txt_experience_new_1_alt_content_2' => 'This game gives an idea of what it might be like to live with the symptoms of dementia, in this case,<br/><br/>
        finding it difficult to recognise friends and family or to recall memories.  ',

    'txt_experience_new_2_content_1' => 'Common symptoms in the early stages of dementia include <b>forgetfulness</b>,<br/>
            <b>communication difficulties</b> and <b>changes in mood and behaviour.</b> ',
    'txt_experience_new_2_content_2' => 'The initial stages of dementia are often difficult to recognise,<br/>
            as symptoms like <b>memory loss</b> and <b>confusion</b> can be temporary and transient.',
    'txt_experience_new_2_content_3' => 'Once diagnosed, people living with dementia can be better supported to help plan their future care,<br/>
            but our research shows that as many as <b>3 out of 4 people</b> living with dementia worldwide<br/>
            have never received a formal diagnosis.',

    'txt_experience_new_3_content_1' => 'Do you remember when this picture<br/>was taken?',
    'txt_experience_new_3_content_2' => 'YES',
    'txt_experience_new_3_content_3' => 'NO',

    'txt_experience_new_4_content_1' => 'How about this one?',
    'txt_experience_new_4_content_2' => 'I REMEMBER',
    'txt_experience_new_4_content_3' => 'I DON&#39;T<br/> REMEMBER',

    'txt_experience_new_5_content_1' => 'A person living in the middle stages of dementia will be <b>aware of their condition</b>, but will need support<br/>
            to help them manage their day-to-day living as <b>their memory continues to deteriorate.</b>',
    'txt_experience_new_5_content_2' => 'They are likely to become increasingly forgetful and may also <b>fail to recognise people</b> or<br/>
            confuse them with others. The middle stages of dementia are typically the longest and can <b>last for many years.</b>',
    'txt_experience_new_5_content_3' => 'These changes can be difficult for families and loved ones, but it’s important to remember that there is support<br/>
            available through your <a href="http://www.alz.co.uk/associations" target="_blank" class="text-blue underline">national Alzheimer association</a>',

    'txt_experience_new_6_content_1' => 'Do you remember where this picture<br/>was taken?',

    'txt_experience_new_8_content_1' => 'During the late stages of dementia the needs of the person will change and they will usually require full time care.<br/>
            The <b>progressive loss of memory</b> can be a distressing time as the person living with dementia<br/><b>may fail to recognise close family members.</b> ',
    'txt_experience_new_8_content_2' => 'Throughout the process a person living with dementia may have increasing <b>difficulty understanding what is happening</b><br/>
            around them and <b>may progressively lose their speech.</b> ',
    'txt_experience_new_8_content_3' => 'It’s important to remember that although many abilities may be lost or diminished, <b>some<br/>
            - such as the senses and ability to respond to emotion - remain.</b> ',

    'txt_experience_new_9_content_1' => 'Do you remember who is <br/>in this this picture?',

    'txt_experience_new_11_content_1' => 'Memory loss is just one of the many ways dementia will affect someone. A person with dementia will also experience poor or decreased judgement, changes in mood and personality and loss of initiative. If you think that these problems are affecting your daily life, or the life of someone you know, you should talk to your doctor, or encourage them to talk to theirs.',
    'txt_experience_new_11_content_2' => 'Think about how difficult it may have been to recognise these photographs and you will have had a small glimpse into what it’s like to live with dementia, especially in the middle and later stages when familiar faces and memories will fade away. Learning about the different experiences of dementia is a great way to help support loved ones. Spread the word, so the world can see what you’ve seen.',
    'txt_experience_new_11_content_3' => 'For more information on the early, middle and later stages of dementia, take a look at the <a href="http://www.alzheimer.ca/en/Living-with-dementia/Caring-for-someone/Early-stage" class=" underline text-blue" target="_blank">Alzheimer’s Society of Canada</a>, <a href="https://fightdementia.org.au/support-and-services/i-care-for-someone-with-dementia/later-stages-of-dementia" class="underline text-blue" target="_blank">Alzheimer’s Australia</a> and the <a href="http://www.alz.org/care/alzheimers-early-mild-stage-caregiving.asp" class="underline text-blue" target="_blank">Alzheimer’s Association</a> guides.',
    'txt_continue' => 'Continue',
);