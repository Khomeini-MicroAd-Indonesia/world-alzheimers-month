<?php
/**
 * Created by PhpStorm.
 * User: Albert
 * Date: 5/18/15
 * Time: 11:04 AM
 */
return array(
/*------------------------------default--------------------------------*/
    'txt_here' => 'aqu&#237;',
    'txt_click' => 'Click',
    'txt_donate' => 'Donar',
    'txt_time' => 'Time',
    'txt_download' => 'Descargue nuestros materiales',
    'txt_button_find_your_national' => 'Encuentre su asociaci&#243;n nacional de Alzheimer',
/*---------------------------template---------------------------------*/
    'txt_footnote_campaign' => 'Una campa&#241;a de',
    'txt_footnote_supported' => 'Orgullosamente patrocinada por',
    'txt_footnote_copyright' => 'Derechos de autor &copy; World Alzheimer&#39;s Month 2015',
    'txt_footnote_privacy' => 'Pol&#237;tica de Privacidad',
    'txt_footnote_copyright_end' => 'Sobre Este Sitio',
    'txt_footnote_contact' => 'Contacto',
/* -----------------------------menu-----------------------------------*/
    'mn_home' => 'Inicio',
    'mn_do_you_remember_me' => '&#191;Se acuerda de m&#237;?',
    'mn_campaign_report_2015' => 'Campaign Report 2015',
    'mn_about_wam' => 'Mes Mundial Del Alzheimer',
    'mn_about_dementia' => 'Acerca de la Demencia',
    'mn_get_involved' => ' Invol&#250;crese',
/* --------------------------home----------------------------------*/
    'txt_main_menu_banner' => '&#161;Bienvenido!',
    'txt_main_text_banner' => 'Septiembre es el Mes Mundial Del Alzheimer,&nbsp;(World Alzheimer&#39;s Month™) una campa&#241;a internacional para crear conciencia y desafiar al estigma. Es tiempo de tomar acci&#243;n, un movimiento global unido por su llamamiento para el cambio, pero tambi&#233;n un momento para reflexionar sobre el impacto de la demencia, una enfermedad que afectar&#225; a m&#225;s y m&#225;s gente con el paso de los a&#241;os.',
    'txt_main_title_impact' => 'El impacto global de la demencia',
    'txt_title_people_tweeting' => 'Spread the word on Twitter',
/* ---------------------experience---------------------------*/
    'txt_title_do_you' => 'Do you remember me?',
    'txt_desc_do_you' => 'Every person is unique and dementia affects people differently &#45; no two people will have symptoms that develop in exactly the same way,<br/>but there are broad similarities between them all. The most common signs are memory loss and the loss of practical abilities, which can <br/>lead to withdrawal from work or social activities. This game gives an idea of what it might be like to live with the symptoms of dementia,<br/>in this case, finding it difficult to recognise friends and family.',
    'txt_desc_do_you_alt' => 'Cicero, a famous roman philosopher once said &quot;Memory is the treasury and guardian of all things&quot;. But what if our memory slowly faded like Alzheimers patient? So let&#39;s test how well you remember your memory. Just follow this instruction below :',
    'txt_desc_do_you_alt2' => 'Let&#39;s see how well you do on our memory test.',
    'txt_desc_do_you_login' => 'Just login to start',
    'txt_coming_soon' => 'Coming Soon',
/*-------------------experience two---------------------------*/
    'txt_title_instruction' => 'Instructions',
    'txt_instruction_step_1' => 'Press start to begin the game.',
    'txt_instruction_step_2' => 'Name the faces from left to right.',
    'txt_instruction_step_3' => 'You must guess all of their names in 2 minutes.',
    'txt_instruction_step_4' => 'The difficulties will increase on each photo.',

    'txt_text_friends' => 'Type your friend&#39;s name here :',
    'txt_minutes' => 'Minutes',
/*------------------about_wam----------------------------------------*/
    'txt_title_facts' => 'Los Hechos',
    'txt_desc_fact_1a' => 'El Mes Mundial del Alzheimer se puso en marcha en septiembre de 2012.',
    'txt_desc_fact_1b' => 'El D&#237;a Mundial del Alzheimer es el 21 de septiembre de cada a&#241;o.',
    'txt_desc_fact_2a' => 'Hay varias maneras en las que usted puede participar en el Mes Mundial del Alzheimer. Podr&#237;a organizar su propio',
    'txt_desc_fact_find_event' => ' evento',
    'txt_desc_fact_2b' => 'o visitar nuestra página bajo &quot;encontrar un evento&quot; para unirse a una actividad organizada por su asociaci&#243;n del Alzheimer nacional. Tambi&#233;n puede difundirlo en las redes sociales.',
    'txt_desc_fact_3' => 'El Mes Mundial del Alzheimer une l&#237;deres de opini&#243;n, personas con demencia, cuidadores y familiares, profesionales m&#233;dicos, investigadores y medios de comunicaci&#243;n de todo el mundo.',
    'txt_desc_fact_4' => '&#161;El Mes Mundial del Alzheimer es tambi&#233;n una excelente oportunidad para divertirse!',
    'txt_desc_fact_5' => 'El Mes Mundial del Alzheimer ofrece una oportunidad para que las asociaciones de Alzheimer alrededor del mundo obtengan el reconocimiento y credibilidad por el trabajo que realizan, coloc&#225;ndose en una posición m&#225;s fuerte para influir en los l&#237;deres de opini&#243;n y los gobiernos.',
    'txt_desc_fact_6' => 'Cada a&#241;o, m&#225;s y m&#225;s pa&#237;ses est&#225;n participando en eventos del Mes Mundial del Alzheimer y en muchas &#225;reas, la conciencia sobre la demencia est&#225; aumentando.',

    'txt_title_remember_me' => 'Recu&#233;rdame',
    'txt_desc_remember_me_1' => 'El tema de la campa&#241;a de este a&#241;o para el Mes Mundial del Alzheimer es &quot;Recu&#233;rdame&quot;. Estamos animando a la gente de todo el mundo a aprender a detectar los signos de demencia, pero tambi&#233;n a no olvidarse de sus seres queridos que viven con demencia, o que hayan fallecido. El impacto de la campa&#241;a de septiembre est&#225; creciendo, pero la estigmatizaci&#243;n y la desinformaci&#243;n que rodea la demencia sigue siendo un problema mundial.',
    'txt_desc_remember_me_2' => 'Descubra m&#225;s sobre nuestras campa&#241;as anteriores',

    'txt_title_about_wam' => 'Acerca de Alzheimer&#39;s Disease International (ADI)',
    'txt_desc_about_wam_1' => 'El Mes Mundial del Alzheimer es una iniciativa de Alzheimer&#39;s Disease International (ADI). Cada a&#241;o, ADI coordina y organiza la campa&#241;a del Mes Mundial del Alzheimer.',
    'txt_desc_about_wam_2' => 'ADI es la organizaci&#243;n que agrupa a m&#225;s de 80 asociaciones de Alzheimer en todo el mundo. Nuestro objetivo es ayudar a establecer y fortalecer nuestras asociaciones a través del mundo y aumentar la conciencia mundial sobre la enfermedad del Alzheimer y otras causas de demencia. Creemos que la clave para ganar la lucha contra la demencia radica en una combinaci&#243;n &#250;nica de soluciones globales y del conocimiento local.',
    'txt_desc_about_wam_3' => 'Como tal, trabajamos a nivel local, mediante la potenciaci&#243;n de la asociaci&#243;n nacional de Alzheimer para promover y ofrecer atenci&#243;n y apoyo a las personas con demencia y sus cuidadores, y en el mundo para centrar la atenci&#243;n en la epidemia y en la campaña para el cambio de pol&#237;tica de los gobiernos y la Organizaci&#243;n Mundial de la Salud, con quien hemos ganado como entidades consultivas en el a&#241;o 2012. Estos son los pilares de nuestra misi&#243;n.',

    /*---------------------- get involved ------------------------------*/
    'txt_title_spread_word' => 'Difunda la informaci&#243;n',
    'txt_desc_spread_word_1' => 'Ay&#250;denos a difundir la informaci&#243;n sobre el Mes Mundial del Alzheimer con estos materiales. Tambi&#233;n puede utilizar nuestra plantilla de carta para informar a los dirigentes comunitarios, pol&#237;ticos, empresas u otras organizaciones acerca de la campa&#241;a. Siga los siguientes enlaces para descargar.',
    'txt_desc_spread_word_1a' => 'Ay&#250;denos a difundir la informaci&#243;n sobre el Mes Mundial del Alzheimer con estos materiales.',
    'txt_desc_spread_word_1b' => 'Tambi&#233;n puede utilizar nuestra plantilla de carta para informar a los dirigentes comunitarios, pol&#237;ticos, empresas u otras organizaciones acerca de la campa&#241;a. Siga los siguientes enlaces para descargar.',

    'txt_title_find_event' => 'Encuentre un evento',
    'txt_desc_find_event' => 'Durante el mes de septiembre habr&#225; miles de eventos que tienen lugar en todo el mundo para conmemorar el Mes Mundial del Alzheimer. Para encontrar un evento en su pa&#237;s, por favor utilice la lista desplegable a continuaci&#243;n o p&#243;ngase en contacto con su asociaci&#243;n local del Alzheimer',
    'txt_desc_find_event_2' => 'Si usted est&#225; planeando un evento por su cuenta, aseg&#250;rese de a&#241;adirla a nuestra lista.',
    'txt_subtitle_find_event' => 'Eventos del Mes del Alzheimer cerca de usted',

    'txt_region' => 'Seleccionar Regi&#243;n',
    'txt_country' => 'Seleccionar Pa&#237;s',
    'txt_city' => 'Seleccionar Ciudad',
    'txt_btn_add_event' => 'Add an event',
    'txt_title_support_us' => 'Ap&#243;yenos',
    'txt_desc_support_us' => 'ADI depende de las donaciones con el fin de llevar a cabo su labor de fortalecer las asociaciones de Alzheimer en <br/>todo el mundoy crear conciencia sobre el impacto global de la demencia.',
    'txt_desc_support_us_2' => 'Al hacer una donaci&#243;n hoy, usted puede marcar una diferencia <br/>real para personas de todo el mundo que viven con la enfermedad de Alzheimer y la demencia.',
    'txt_desc_support_us_3' => 'if you would like to make a donation',

    'txt_add_event' => 'A&#241;adir Evento',
    'txt_event_name'=> 'Nombre del Evento',
    'txt_email' => 'E-mail',
    'txt_event_desc' => 'Descripci&#243;n del Evento',
    'txt_date' => 'Fecha',
    'txt_time' => 'Horario',
    'txt_country' => 'Pa&#237;s',
    'txt_city' => 'Ciudad',
    'txt_location' => 'Ubicaci&#243;n',
    'txt_organization' => 'Organizaci&#243;n',
    'txt_web_link' => 'Enlace Web',
    /*-------------------- about dementia -------------------------------*/
    'txt_title_faq' => 'Preguntas frecuentes (FAQ)',
    'txt_dementia_q1' => '&#191;Qu&#233; es la demencia?',
    'txt_dementia_a1' => 'La demencia es un nombre colectivo para los s&#237;ndromes cerebrales degenerativos progresivos que afectan a la memoria, el pensamiento, el comportamiento y la emoción. La enfermedad de Alzheimer es el tipo m&#225;s com&#250;n de demencia, afectando hasta el 90% de las personas que viven con demencia. Pero hay un gran número de condiciones que causan los s&#237;ntomas de la demencia, como resultado de los cambios que ocurren en el cerebro y la p&#233;rdida final de las c&#233;lulas nerviosas (neuronas). Otros tipos de demencia incluyen la demencia vascular, la demencia de cuerpos de Lewy y la frontotemporal (incluyendo la enfermedad de Pick).',
    'txt_dementia_a1a' => 'La demencia es un nombre colectivo para los s&#237;ndromes cerebrales degenerativos progresivos que afectan a la memoria, el pensamiento, el comportamiento y la emoción. La enfermedad de Alzheimer es el tipo m&#225;s com&#250;n de demencia, afectando hasta el 90% de las personas que viven con demencia. ',
    'txt_dementia_a1b' => 'Pero hay un gran número de condiciones que causan los s&#237;ntomas de la demencia, como resultado de los cambios que ocurren en el cerebro y la p&#233;rdida final de las c&#233;lulas nerviosas (neuronas). Otros tipos de demencia incluyen la demencia vascular, la demencia de cuerpos de Lewy y la frontotemporal (incluyendo la enfermedad de Pick).',
    'txt_dementia_q2' => '&#191;Cu&#225;les son los primeros síntomas de la demencia?',
    'txt_dementia_a2' => 'Cada persona es &#250;nica y la demencia afecta a las personas de manera diferente – no hay dos personas que desarrollar&#225;n los s&#237;ntomas exactamente de la misma manera. La personalidad de un individuo, la salud general y la situaci&#243;n social son factores importantes para determinar el impacto de la demencia en él o ella. Los síntomas varían entre la enfermedad de Alzheimer y otros tipos de demencia, pero hay similitudes generales entre todos ellos. Los signos más comunes son la pérdida de memoria y la pérdida de habilidades prácticas, que pueden conducir a la retirada del trabajo o de actividades sociales. Si usted piensa que estos problemas están afectando a su vida diaria, o la vida de alguien que usted conoce, debería hablar con su médico.',
    'txt_dementia_a2a' => 'Los signos m&#225;s comunes son la p&#233;rdida de memoria y la p&#233;rdida de habilidades pr&#225;cticas, que pueden conducir a la retirada del trabajo o de actividades sociales. Si usted piensa que estos problemas est&#225;n afectando a su vida diaria, o la vida de alguien que usted conoce, deber&#237;a hablar con su m&#233;dico.',
    'txt_dementia_a2b' => 'Cada persona es &#250;nica y la demencia afecta a las personas de manera diferente – no hay dos personas que desarrollar&#225;n los s&#237;ntomas exactamente de la misma manera. La personalidad de un individuo, la salud general y la situaci&#243;n social son factores importantes para determinar el impacto de la demencia en &#233;l o ella. Los s&#237;ntomas var&#237;an entre la enfermedad de Alzheimer y otros tipos de demencia, pero hay similitudes generales entre todos ellos.',
    'txt_dementia_q3' => 'Mi madre tiene la enfermedad de Alzheimer. &#191;Voy a tenerla?',
    'txt_dementia_a3' => 'Hay unos pocos casos muy raros en los que la enfermedad de Alzheimer es hereditaria. En estos casos hay un vínculo directo entre una mutaci&#243;n heredada en un gen y la aparici&#243;n de la enfermedad. Estos tienden a ser los casos de "inicio temprano" de la enfermedad de Alzheimer, que afecta a los menores de 65 a&#241;os. En estos casos, la probabilidad de que la enfermedad de Alzheimer se desarrolle en los familiares (hermanos, hermanas e hijos) es uno de cada dos. La mayor&#237;a de casos de la enfermedad de Alzheimer no son del tipo que se transmite directamente de esta manera. Si un miembro de la familia tiene una forma normal de la enfermedad de Alzheimer, el riesgo para los parientes cercanos es de alrededor de tres veces m&#225;s que el riesgo de una persona de una edad similar que no tiene antecedentes familiares de la enfermedad. Se cree que en estos casos los genes de una persona pueden contribuir al desarrollo de la enfermedad pero no causarlo directamente.',
    'txt_dementia_a3a' => 'Hay unos pocos casos muy raros en los que la enfermedad de Alzheimer es hereditaria. En estos casos hay un v&#237;nculo directo entre una mutaci&#243;n heredada en un gen y la aparici&#243;n de la enfermedad. Estos tienden a ser los casos de &quot;inicio temprano&quot; de la enfermedad de Alzheimer, que afecta a los menores de 65 a&#241;os. En estos casos, la probabilidad de que la enfermedad de Alzheimer se desarrolle en los familiares (hermanos, hermanas e hijos) es uno de cada dos. La mayor&#237;a de casos de la enfermedad de Alzheimer no son del tipo que se transmite directamente de esta manera.',
    'txt_dementia_a3b' => 'Si un miembro de la familia tiene una forma normal de la enfermedad de Alzheimer, el riesgo para los parientes cercanos es de alrededor de tres veces m&#225;s que el riesgo de una persona de una edad similar que no tiene antecedentes familiares de la enfermedad. Se cree que en estos casos los genes de una persona pueden contribuir al desarrollo de la enfermedad pero no causarlo directamente.',
    'txt_dementia_q4' => '&#191;Existe una cura?',
    'txt_dementia_a4' => 'Actualmente no existe cura para la enfermedad de Alzheimer o de la mayor&#237;a de otras causas de demencia, ni se puede esperar una cura en el futuro cercano. Los investigadores todav&#237;a est&#225;n en la etapa del desarrollo de f&#225;rmacos para desacelerar el progreso de la enfermedad, al menos en algunos casos. Ellos todav&#237;a no saben c&#243;mo prevenir que la enfermedad no ocurra, ni c&#243;mo detener su progreso o la manera de revertir sus efectos. Se espera que m&#225;s investigaci&#243;n sobre las causas de la demencia haga posible un tratamiento eficaz.',
    'txt_dementia_a4a' => 'Actualmente no existe cura para la enfermedad de Alzheimer o de la mayor&#237;a de otras causas de demencia, ni se puede esperar una cura en el futuro cercano. Los investigadores todav&#237;a est&#225;n en la etapa del desarrollo de f&#225;rmacos para desacelerar el progreso de la enfermedad, al menos en algunos casos.',
    'txt_dementia_a4b' => 'Ellos todav&#237;a no saben c&#243;mo prevenir que la enfermedad no ocurra, ni c&#243;mo detener su progreso o la manera de revertir sus efectos. Se espera que m&#225;s investigaci&#243;n sobre las causas de la demencia haga posible un tratamiento eficaz.',
    'txt_dementia_q5' => '&#191;Existen tratamientos farmacol&#243;gicos para la enfermedad de Alzheimer?',
    'txt_dementia_a5' => 'Aunque no existen medicamentos que pueden curar la enfermedad de Alzheimer, hay una serie de tratamientos farmacol&#243;gicos que pueden ayudar a algunas personas con la enfermedad de Alzheimer. Los tratamientos actualmente disponibles pueden desacelerar el progreso de la enfermedad en algunos casos durante per&#237;odos de entre 6 y 18 meses. La clase principal de tales compuestos son los inhibidores de la colinesterasa. Otros tipos de medicamentos son &#250;tiles de vez en cuando para controlar algunos de los s&#237;ntomas de la enfermedad de Alzheimer, tales como el insomnio y agitaci&#243;n. En general, sin embargo, el uso de medicamentos como las p&#237;ldoras para dormir o tranquilizantes debe mantenerse al m&#237;nimo si alguien tiene la enfermedad de Alzheimer, ya que pueden causar aumento de la  confusi&#243;n. Tratamientos no farmacol&#243;gicos, incluyendo el apoyo pr&#225;ctico y emocional, son importantes y eficaces para ayudar a las personas con demencia y a cuidadores.',
    'txt_dementia_a5a' => 'Aunque no existen medicamentos que pueden curar la enfermedad de Alzheimer, hay una serie de tratamientos farmacol&#243;gicos que pueden ayudar a algunas personas con la enfermedad de Alzheimer.',
    'txt_dementia_a5b' => 'Los tratamientos actualmente disponibles pueden desacelerar el progreso de la enfermedad en algunos casos durante per&#237;odos de entre 6 y 18 meses. La clase principal de tales compuestos son los inhibidores de la colinesterasa. Otros tipos de medicamentos son &#250;tiles de vez en cuando para controlar algunos de los s&#237;ntomas de la enfermedad de Alzheimer, tales como el insomnio y agitaci&#243;n. En general, sin embargo, el uso de medicamentos como las p&#237;ldoras para dormir o tranquilizantes debe mantenerse al m&#237;nimo si alguien tiene la enfermedad de Alzheimer, ya que pueden causar aumento de la  confusi&#243;n. Tratamientos no farmacol&#243;gicos, incluyendo el apoyo pr&#225;ctico y emocional, son importantes y eficaces para ayudar a las personas con demencia y a cuidadores.',
    'txt_dementia_q6' => '&#191;Se puede prevenir la enfermedad de Alzheimer y otras formas de demencia?',
    'txt_dementia_a6' => 'No se sabe lo suficiente acerca de las causas de la enfermedad de Alzheimer ni de las medidas preventivas espec&#237;ficas para ser recomendadas. Aunque la enfermedad de Alzheimer y otras formas de demencia son m&#225;s comunes con el avance de la edad, no se conoce el detonante de los cambios caracter&#237;sticos que se producen en el tejido cerebral de personas con enfermedad de Alzheimer. Se cree que los genes desempe&#241;an un papel en el desarrollo de la mayor&#237;a de los casos de la enfermedad de Alzheimer. En casos raros, los genes anormales son los que en realidad causan la enfermedad. Mucho m&#225;s com&#250;nmente, se cree que los genes s&#243;lo contribuyen a la susceptibilidad de una persona a la enfermedad. Parece que, al menos en algunos casos, los factores en el medio ambiente pueden ser necesarios para desencadenar la enfermedad. Aunque no hay medidas preventivas espec&#237;ficas, lo que puede recomendarse es un estilo de vida saludable - llevar una dieta saludable y mantenerse f&#237;sica, mental y socialmente activo. Cada vez hay m&#225;s evidencia de que tener un estilo de vida saludable ayuda a reducir el riesgo de un individuo.',
    'txt_dementia_a6a' => 'No se sabe lo suficiente acerca de las causas de la enfermedad de Alzheimer ni de las medidas preventivas espec&#237;ficas para ser recomendadas. Aunque la enfermedad de Alzheimer y otras formas de demencia son m&#225;s comunes con el avance de la edad, no se conoce el detonante de los cambios caracter&#237;sticos que se producen en el tejido cerebral de personas con enfermedad de Alzheimer. Se cree que los genes desempe&#241;an un papel en el desarrollo de la mayor&#237;a de los casos de la enfermedad de Alzheimer.',
    'txt_dementia_a6b' => 'En casos raros, los genes anormales son los que en realidad causan la enfermedad. Mucho m&#225;s com&#250;nmente, se cree que los genes s&#243;lo contribuyen a la susceptibilidad de una persona a la enfermedad. Parece que, al menos en algunos casos, los factores en el medio ambiente pueden ser necesarios para desencadenar la enfermedad. Aunque no hay medidas preventivas espec&#237;ficas, lo que puede recomendarse es un estilo de vida saludable - llevar una dieta saludable y mantenerse f&#237;sica, mental y socialmente activo. Cada vez hay m&#225;s evidencia de que tener un estilo de vida saludable ayuda a reducir el riesgo de un individuo.',

    'txt_title_signs' => '10 se&#241;ales de advertencia de la demencia',
    'txt_subtitle_sign1' => '1. P&#233;rdida de la memoria que altera la vida cotidiana',
    'txt_subtitle_sign2' => '2. Desaf&#237;os en la planificaci&#243;n o resoluci&#243;n de problemas',
    'txt_subtitle_sign3' => '3. Dificultad para completar tareas familiares en el hogar, en el trabajo o en el ocio',
    'txt_subtitle_sign4' => '4. Confusi&#243;n con el tiempo o lugar',
    'txt_subtitle_sign5' => '5. Dificultad para comprender im&#225;genes visuales y relaciones espaciales',
    'txt_subtitle_sign6' => '6. Nuevos problemas con palabras al hablar o escribir',
    'txt_subtitle_sign7' => '7. Perder las cosas y perder la capacidad de volver sobre los pasos',
    'txt_subtitle_sign8' => '8. Disminuci&#243;n o falta de juicio',
    'txt_subtitle_sign9' => '9. Abandono de las actividades laborales o sociales',
    'txt_subtitle_sign10' => '10. Cambios en el estado de &#225;nimo y en la personalidad',
    'txt_desc_sign1' => 'Uno de los signos m&#225;s comunes de la enfermedad de Alzheimer es la p&#233;rdida de memoria, especialmente olvidar informaci&#243;n aprendida recientemente. Otros incluyen olvidar fechas o eventos importantes; pedir la misma informaci&#243;n una y otra vez; tener la necesidad de depender cada vez m&#225;s de ayudas para la memoria (por ejemplo, notas de recordatorio o dispositivos) o de miembros de la familia para las cosas que sol&#237;an manejar por su cuenta.',
    'txt_desc_sign2' => 'Algunas personas pueden experimentar cambios en su capacidad para desarrollar y seguir un plan o para trabajar con n&#250;meros. Pueden tener problemas para seguir una receta familiar o hacer seguimiento de las facturas mensuales. Pueden tener dificultad para concentrarse y necesitar m&#225;s tiempo que el que necesitaban antes para hacer las cosas.',
    'txt_desc_sign3' => 'Las personas con la enfermedad de Alzheimer a menudo tienen dificultades para completar las tareas diarias. A veces, pueden tener problemas para conducir a un lugar familiar, manejar la gesti&#243;n de un presupuesto en el trabajo o recordar las reglas de un juego favorito.',
    'txt_desc_sign4' => 'Las personas con Alzheimer pueden perder la noci&#243;n de las fechas, temporadas y del paso del tiempo. Pueden tener problemas para entender algo si no est&#225; sucediendo inmediatamente. A veces pueden olvidar d&#243;nde est&#225;n o c&#243;mo llegaron all&#237;.',
    'txt_desc_sign5' => 'Para algunas personas, tener problemas de la vista es un signo de la enfermedad de Alzheimer. Pueden tener dificultad para leer, juzgar distancias y determinar el color o el contraste, lo que puede causar problemas al conducir.',
    'txt_desc_sign6' => 'Las personas con Alzheimer pueden tener problemas para seguir o unirse a una conversaci&#243;n. Pueden parar en medio de una conversaci&#243;n y no tienen idea de c&#243;mo continuar o pueden repetirse. Pueden tener dificultades con el vocabulario, tener problemas para encontrar la palabra correcta o llamar a las cosas por el nombre equivocado (por ejemplo, llamar a un &quot;reloj&quot;, &quot;reloj de mano&quot;).',
    'txt_desc_sign7' => 'Una persona con enfermedad de Alzheimer puede poner las cosas en lugares inusuales. Pueden perder las cosas y ser incapaces de volver sobre sus pasos para encontrarlas de nuevo. A veces, pueden acusar a otros de robar. Con el tiempo, esto puede ocurrir con m&#225;s frecuencia.',
    'txt_desc_sign8' => 'Las personas con Alzheimer pueden experimentar cambios en su juicio o en la toma de decisiones. Por ejemplo, pueden utilizar la falta de juicio cuando se trata de dinero, dando grandes cantidades de dinero a agentes de venta telef&#243;nica. Puede que presten menos atenci&#243;n a mantenerse limpios o al aseo personal. ',
    'txt_desc_sign9' => 'Una persona con Alzheimer puede que comience a retirarse de pasatiempos, actividades sociales, proyectos de trabajo o deportes. Puede que tenga problemas para mantenerse al d&#237;a con su equipo deportivo favorito o para recordar c&#243;mo completar un pasatiempo favorito. Puede que tambi&#233;n eviten ser sociables, debido a los cambios que han experimentado. ',
    'txt_desc_sign10' => 'El estado de &#225;nimo y la personalidad de las personas con Alzheimer pueden cambiar. Se pueden volver confusos, sospechosos, deprimidos, temerosos o ansiosos. Se pueden volver molestos f&#225;cilmente en casa, en el trabajo, con amigos o en lugares en los que est&#225;n fuera de su zona de comodidad.',
    'txt_source' => 'Fuente',

    'txt_title_find_help' => 'Encontrar ayuda',
    'txt_desc_finding_help' => 'Si usted o un ser querido vive con demencia, usted no est&#225; solo. Las Asociaciones Nacionales de Alzheimer ofrecen ayuda y apoyo para las personas con demencia y sus cuidadores. P&#243;ngase en contacto con la asociaci&#243;n de Alzheimer en su pa&#237;s, o si no existe una asociaci&#243;n en su pa&#237;s, puede intentar ponerse en contacto con uno de un pa&#237;s vecino. Estas organizaciones ofrecen amistad, apoyo y asesoramiento, y son un salvavidas para muchas personas.',
    'txt_desc_finding_help1' => 'to find your national Alzheimer association.',
    'txt_desc_finding_help2' => 'Si usted o un ser querido vive con demencia, usted no est&#225; solo. Las Asociaciones Nacionales de Alzheimer ofrecen ayuda y apoyo para las personas con demencia y sus cuidadores.',
    'txt_desc_finding_help3' => 'P&#243;ngase en contacto con la asociaci&#243;n de Alzheimer en su pa&#237;s, o si no existe una asociaci&#243;n en su pa&#237;s, puede intentar ponerse en contacto con uno de un pa&#237;s vecino.',
    'txt_desc_finding_help4' => 'Estas organizaciones ofrecen amistad, apoyo y asesoramiento, y son un salvavidas para muchas personas.',
    'txt_desc_finding_help5' => 'to find your national Alzheimer association.',

    /*---------------------------privacy policy -----------------------------------------*/
    'txt_title_privacy' => 'Pol&#237;tica de Privacidad',
    'txt_desc_privacy1' => 'Alzheimer&#39;s Disease International (ADI) s&#243;lo utilizar&#225; cualquier informaci&#243;n personal que usted nos env&#237;e a los fines para los que se proporcionen. Por ejemplo, podemos recopilar su informaci&#243;n, donde usted elija prove&#233;rnosla, para la administraci&#243;n, recaudaci&#243;n de fondos y desarrollo del sitio web, productos y servicios. Todos los empleados y agentes que tienen acceso a sus datos personales y est&#225;n asociados con el manejo de esos datos est&#225;n obligados a respetar la confidencialidad de sus datos personales. ADI trata de mantener la informaci&#243;n que tenemos sobre usted de manera precisa y actualizada. Sin embargo, si usted encuentra errores o inexactitudes en los datos, borraremos, completaremos o modificaremos dicha informaci&#243;n bajo petici&#243;n.',
    'txt_desc_privacy2' => 'La direcci&#243;n de Internet (IP), tipo de navegador, y cualquier informaci&#243;n de referencia proporcionada por su navegador se registran cuando usted utiliza este sitio. Esta informaci&#243;n ser&#225; utilizada por ADI para compilar estad&#237;sticas de uso general, para ayudarnos a mejorar el sitio y para diagnosticar problemas t&#233;cnicos.',
    'txt_desc_privacy3' => 'Si usted utiliza la actividad &quot;&#191;Se acuerda de m&#237;?&quot; y accede a su cuenta de Facebook, usted estar&#225; concediendo a este sitio web el acceso a sus fotos y a su informaci&#243;n de perfil en Facebook. Esta informaci&#243;n s&#243;lo ser&#225; utilizada para la experiencia en este sitio web y no ser&#225; retenida. La informaci&#243;n b&#225;sica sobre el uso de este sitio web tambi&#233;n ser&#225; transmitida y archivada por Facebook.&nbsp;',
    'txt_desc_privacy3_link' => 'Las pol&#237;ticas de Facebook&nbsp;',
    'txt_desc_privacy3a' => 'se aplican a los datos recogidos.',
    'txt_desc_privacy4' => 'Esta p&#225;gina web utiliza Google Analytics, un servicio anal&#237;tico de web prestado por Google, Inc. Google Analytics utiliza &quot;cookies&quot;, que son archivos de texto ubicados en su ordenador, para ayudar a la p&#225;gina web a analizar c&#243;mo los usuarios utilizan el sitio. La informaci&#243;n generada por la cookie acerca de su uso de la p&#225;gina web (incluyendo su direcci&#243;n IP) ser&#225; directamente transmitida y archivada por Google en los servidores de Estados Unidos. Google usar&#225;  esta informaci&#243;n con el prop&#243;sito de evaluar su uso de la p&#225;gina web, recopilando informes de la actividad de la p&#225;gina web y prestando otros servicios relacionados con la actividad de la p&#225;gina web y el uso de Internet. Google podr&#225; transmitir dicha informaci&#243;n a terceros donde as&#237; se lo requiera la ley, o donde dichos terceros procesen la informaci&#243;n por cuenta de Google. Google no asociar&#225; su direcci&#243;n IP con ning&#250;n otro dato del que disponga Google. Usted puede rechazar el uso de cookies seleccionando la configuración apropiada en su navegador. Al utilizar esta página web, usted consiente el tratamiento de informaci&#243;n acerca de usted por Google en la forma y para los fines arriba indicados. Las b&#250;squedas en el sitio tambi&#233;n est&#225;n a cargo de Google. &nbsp;',
    'txt_desc_privacy4_link' => 'La Pol&#237;tica de privacidad de Google ',
    'txt_desc_privacy4a' => 'se aplica a los datos recopilados. ',
    'txt_desc_privacy5' => 'Si usted tiene alguna preocupación sobre el uso de sus datos personales, por favor',
    'txt_contact_us' => 'cont&#225;ctenos',

    /*-----------------------------new experience----------------------------------------*/
    'txt_experience_new_1_content_1' => 'Every person is unique and dementia affects people differently<br/>- no two people will have symptoms that develop in exactly<br/> the same way, but there are broad similarities between them <br/>all.',
    'txt_experience_new_1_content_2' => 'The most common signs are memory loss and the loss of<br/> practical abilities, which can lead to withdrawal from work or<br/> social activities. ',
    'txt_experience_new_1_content_3' => 'This game gives an idea of what it might be like to experience<br/> the symptoms of dementia, in this case, finding it difficult to<br/> recognise friends and family or to recall memories. ',

    'txt_experience_new_1_alt_content_1' => 'Every person is unique and dementia affects people differently - no two people will have symptoms that develop in exactly the same way,<br/> but there are broad similarities between them all. The most common signs are memory loss and the loss of practical abilities, which can<br/> lead to withdrawal from work or social activities.',
    'txt_experience_new_1_alt_content_2' => 'This game gives an idea of what it might be like to live with the symptoms of dementia, in this case,<br/><br/>
        finding it difficult to recognise friends and family or to recall memories.  ',

    'txt_experience_new_2_content_1' => 'Common symptoms in the early stages of dementia include <b>forgetfulness</b>,<br/>
            <b>communication difficulties</b> and <b>changes in mood and behaviour.</b> ',
    'txt_experience_new_2_content_2' => 'The initial stages of dementia are often difficult to recognise,<br/>
            as symptoms like <b>memory loss</b> and <b>confusion</b> can be temporary and transient.',
    'txt_experience_new_2_content_3' => 'Once diagnosed, people living with dementia can be better supported to help plan their future care,<br/>
            but our research shows that as many as <b>3 out of 4 people</b> living with dementia worldwide<br/>
            have never received a formal diagnosis.',

    'txt_experience_new_3_content_1' => 'Do you remember when this picture<br/>was taken?',
    'txt_experience_new_3_content_2' => 'YES',
    'txt_experience_new_3_content_3' => 'NO',

    'txt_experience_new_5_content_1' => 'A person living in the middle stages of dementia will be <b>aware of their condition</b>, but will need support<br/>
            to help them manage their day-to-day living as <b>their memory continues to deteriorate.</b>',
    'txt_experience_new_5_content_2' => 'They are likely to become increasingly forgetful and may also <b>fail to recognise people</b> or<br/>
            confuse them with others. The middle stages of dementia are typically the longest and can <b>last for many years.</b>',
    'txt_experience_new_5_content_3' => 'These changes can be difficult for families and loved ones, but it’s important to remember that there is support<br/>
            available through <a href="http://www.alz.co.uk/associations" target="_blank" class="text-blue">http://www.alz.co.uk/associations</a>',

    'txt_experience_new_8_content_1' => 'During the late stages of dementia the needs of the person will change and they will usually require full time care.<br/>
            The <b>progressive loss of memory</b> can be a distressing time as the person living with dementia<br/><b>may fail to recognise close family members.</b> ',
    'txt_experience_new_8_content_2' => 'Throughout the process a person living with dementia may have increasing <b>difficulty understanding what is happening</b><br/>
            around them and <b>may progressively lose their speech.</b> ',
    'txt_experience_new_8_content_3' => 'It’s important to remember that although many abilities may be lost or diminished, <b>some<br/>
            - such as the senses and ability to respond to emotion - remain.</b> ',

    'txt_experience_new_11_content_1' => 'Memory loss is just one of the many ways dementia will affect someone. A person with dementia will also experience poor or decreased judgement, changes in mood and personality and loss of initiative. If you think that these problems are affecting your daily life, or the life of someone you know, you should talk to your doctor, or encourage them to talk to theirs.',
    'txt_experience_new_11_content_2' => 'Think about how difficult it may have been to recognise these photographs and you will have had a small glimpse into what it’s like to live with dementia, especially in the middle and later stages when familiar faces and memories will fade away. Learning about the different experiences of dementia is a great way to help support loved ones. Spread the word, so the world can see what you’ve seen.',
    'txt_experience_new_11_content_3' => 'For more information on the early, middle and later stages of dementia, take a look at the <a href="#" class=" underline text-blue">Alzheimer’s Society of Canada</a>, <a href="#" class="underline text-blue">Alzheimer’s Australia</a> and the <a href="#" class="underline text-blue">Alzheimer’s Association</a> guides.',


);