<?php
/**
 * Created by PhpStorm.
 * User: Albert
 * Date: 5/18/15
 * Time: 11:04 AM
 */
return array(
    /*------------------------------default--------------------------------*/
    'txt_here' => 'disini',
    'txt_click' => 'Klik',
    'txt_donate' => 'Donasi',
    'txt_download' => 'Unduh materi kami',
    'txt_button_find_your_national' => 'Temui asosiasi Alzheimer national anda',

    /*------------------------template------------------------------------*/
    'txt_footnote_campaign' => 'Kampanye oleh',
    'txt_footnote_supported' => 'Dengan bangga didukung oleh',
    'txt_footnote_copyright' => 'Hak cipta &copy; World Alzheimer Month 2015',
    'txt_footnote_privacy' => 'Kebijakan Privasi',
    'txt_footnote_copyright_end' => 'Tentang Situs ini',
    'txt_footnote_contact' => 'Kontak',

    /* -----------------------------menu-----------------------------------*/
    'mn_home' => 'Halaman Utama',
    'mn_do_you_remember_me' => 'Apakah Kamu Ingat Saya?',
    'mn_campaign_report_2015' => 'Report Kampanye 2015',
    'mn_about_wam' => 'Tentang World Alzheimer&#39;s Month',
    'mn_about_dementia' => 'Tentang Dementia',
    'mn_get_involved' => 'Ikut Terlibat',
    /* --------------------------home----------------------------------*/
    'txt_main_menu_banner' => 'Selamat Datang&#33;',
    'txt_main_text_banner' => 'September diperingati sebagai Bulan Alzheimer Dunia, sebuah kampanye internasional yang bertujuan untuk meningkatkan kesadaran dan tantangan merubah stigma Alzheimer pada masyarakat. Kini saatnya kita berkontribusi dan terlibat dalam sebuah gerakan global yang disatukan oleh seruan perubahan. Bulan Alzheimer Dunia  adalah momentum untuk merenungkan dampak demensia Alzheimer, penyakit yang dari tahun ke tahun semakin banyak penderitanya.',
    'txt_main_title_impact' => 'Dampak Global dari Demensia',
    'txt_title_people_tweeting' => 'Sebarkan pesan ini di Twitter',
    /* ------------------do you remember me? ------------------------*/
    'txt_title_do_you' => 'Apakah kamu ingat saya?',
    'txt_desc_do_you' => 'Setiap orang adalah berbeda dan demensia bisa mempengaruhi orang dengan cara yang berbeda-beda. Tidak ada orang yang memiliki gejala demensia yang sama persis dengan orang lain, namun  banyak juga yang mempunyai kesamaan diantara mereka. Tanda-tanda yang paling umum adalah hilangnya memori dan hilangnya kemampuan praktis, yang dapat menyebabkan kemunduran dalam pekerjaan atau kehidupan sosial penderita. Game ini memberikan gambaran tentang apa yang mungkin terjadi jika kita hidup dengan gejala demensia, dalam hal ini kesulitan mengenali teman-teman dan keluarga.',
    'txt_desc_do_you_mobile' => 'Setiap orang adalah berbeda dan demensia bisa mempengaruhi orang dengan cara yang berbeda-beda. Tidak ada orang yang memiliki gejala demensia yang sama persis dengan orang lain, namun  banyak juga yang mempunyai kesamaan diantara mereka. Tanda-tanda yang paling umum adalah hilangnya memori dan hilangnya kemampuan praktis, yang dapat menyebabkan kemunduran dalam pekerjaan atau kehidupan sosial penderita. Game ini memberikan gambaran tentang apa yang mungkin terjadi jika kita hidup dengan gejala demensia, dalam hal ini kesulitan mengenali teman-teman dan keluarga.',
    'txt_desc_do_you_alt' => 'Cicero, a famous roman philosopher once said &quot;Memory is the treasury and guardian of all things&quot;. But what if our memory slowly faded like Alzheimers patient? So let&#39;s test how well you remember your memory. Just follow this instruction below :',
    'txt_desc_do_you_alt2' => 'Mari kita lihat seberapa baik Anda dalam melakukan tes memori kami.',
    'txt_desc_do_you_login' => 'Silahkan Login untuk memulai',
    'txt_coming_soon' => 'Segera Hadir',
    /*-------------------experience two---------------------------*/
    'txt_title_instruction' => 'Instructions',
    'txt_instruction_step_1' => 'Press start to begin the game.',
    'txt_instruction_step_2' => 'Name the faces from left to right.',
    'txt_instruction_step_3' => 'You must guess all of their names in 2 minutes.',
    'txt_instruction_step_4' => 'The difficulties will increase on each photo.',
    /*------------------about_wam----------------------------------------*/
    'txt_title_facts' => 'Fakta',
    'txt_desc_fact_1a' => 'Bulan Alzheimer Dunia diluncurkan pada bulan September.',
    'txt_desc_fact_1b' => 'Hari Alzheimer Dunia diperingati setiap tanggal 21 September setiap tahunnya.',
    'txt_desc_fact_2a' => 'Ada beberapa cara untuk Anda dapat berpartisipasi dalam Bulan Alzheimer Dunia ini. Anda dapat mengorganisir acara atau kunjungi website kami untuk bergabung dengan',
    'txt_desc_fact_find_event' => 'kegiatan',
    'txt_desc_fact_2b' => 'yang diselenggarakan oleh asosiasi Alzheimer di negara Anda, atau menyebarkan informasi di media social.',
    'txt_desc_fact_3' => 'Bulan Alzheimer Dunia menyatukan para pembuat kebajikan, Orang Dengan Demensia (ODD), pendamping dan keluarga ODD, tenaga kesehatan profesional, peneliti dan media di seluruh dunia.',
    'txt_desc_fact_4' => 'Bulan Alzheimer Dunia juga merupakan kesempatan yang baik untuk kita bergembira!',
    'txt_desc_fact_5' => 'Bulan Alzheimer Dunia memberikan kesempatan bagi seluruh yayasan dan asosiasi Alzheimer di seluruh dunia untuk mendapatkan penghargaan dan pengakuan untuk apa yang mereka perjuangkan serta menempatkan diri dalam posisi yang lebih kuat untuk mempengaruhi para pembuat kebijakan dan pemerintah.',
    'txt_desc_fact_6' => 'Setiap tahun, semakin banyak negara yang berpartisipasi dalam acara World Alzheimer\'s Month dan di banyak daerah, kesadaran demensia berkembang.',

    'txt_title_remember_me' => 'Ingat Saya',
    'txt_desc_remember_me_1' => 'Tema Bulan Alzheimer Dunia tahun ini adalah Ingat Saya. Kami mendorong public di seluruh dunia untuk belajar memahami tanda-tanda Demensia, tanpa melupakan orang-orang terkasih yang hidup dengan Demensia, atau yang mungkin telah wafat. Dampak dari kampanye bulan September selalu berkembang setiap tahunnya, tetapi stigma dan kesalahan informasi mengenai Demensia masih menjadi masalah global.',
    'txt_desc_remember_me_2' => 'Cari tahu tentang kampanye tahun sebelumnya',

    'txt_title_about_wam' => 'Tentang Alzheimer&#39;s Disease International&nbsp;(ADI)',
    'txt_desc_about_wam_1' => 'Bulan Alzheimer Dunia merupakan kampanye inisiatif dari Alzheimer&#39;s Disease International(ADI). Setiap tahunnya, ADI berkoordinasi dalam melaksanakan kampanye Bulan Alzheimer Dunia di seluruh dunia.',
    'txt_desc_about_wam_2' => 'ADI adalah sebuah organisasi yang menaungi lebih dari 80 asosiasi Alzheimer di seluruh dunia. ADI bertujuan untuk membangun dan memperkuat asosiasi Alzheimer di seluruh dunia dan meningkatkan kesadaran global tentang penyakit Alzheimer serta penyebab lain dari Demensia. ADI percaya bahwa kunci dari peran kita semua dalam kepedulian tentang Demensia terletak pada suatu kombinasi unik perpaduan solusi global dan pengetahuan lokal.',
    'txt_desc_about_wam_3' => 'Kami bekerja secara lokal, dengan memberdayakan asosiasi Alzheimer nasional untuk mempromosikan dan menawarkan perawatan dan dukungan bagi orang-orang dengan Demensia dan karir mereka, dan secara global memusatkan perhatian untuk kampanye pemahaman public dan perubahan kebijakan dari pemerintah dan organisasi kesehatan dunia, yang mana pada tahun 2012 kami memperoleh status konsultatif.Ini merupakan pilar dari misi kami.Semua pencapaian ini berkat kerja keras dan dedikasi seluruh anggota ADI yang membuat dampak World Alzheimer’s Month dapat dirasakan pada tingkat nasional dan global. Setiap tahunnya, semakin banyak negara yang berpartisipasi dalam acara Bulan Alzheimer Dunia dan di banyak daerah, kesadaran mengenai Demensia terus berkembang.',

    /*---------------------- get involved ------------------------------*/
    'txt_title_spread_word' => 'Sebarkan pesan ini',
    'txt_desc_spread_word_1' => 'Bantu kami menyebarkan informasi tentang Bulan Alzheimer Dunia dengan berbagai materi ini.<br>Anda juga dapat menggunakan template surat kami untuk menginformasikan tokoh masyarakat, politisi, bisnis atau organisasi lain tentang kampanye Bulan Alzheimer Dunia ini. Ikuti Link di bawah ini untuk mengunduhnya.',
    'txt_desc_spread_word_1a' => 'Bantu kami menyebarkan informasi tentang Bulan Alzheimer Dunia dengan berbagai materi ini',
    'txt_desc_spread_word_1b' => 'Anda juga dapat menggunakan template surat kami untuk menginformasikan tokoh masyarakat, politisi, bisnis atau organisasi lain tentang kampanye Bulan Alzheimer Dunia ini. Ikuti Link di bawah ini untuk mengunduhnya.',

    'txt_title_find_event' => 'Event',
    'txt_desc_find_event' => 'Selama bulan September akan ada beberapa event / acara di seluruh dunia. Selama Bulan Alzheimer Dunia 2014, termasuk peluncuran Laporan Alzheimer Dunia 2015. Untuk menemukan acara tersebut di negara anda, silahkan hubungi asosiasi Alzheimer setempat. Asosiasi Alzheimer adalah organisasi non-profit yang mendukung penderita demensia dan keluarganya. Temukan lebih lanjut tentang peran asosiasi. Pilih negara atau wilayah untuk menemukan asosiasi Alzheimer’s yang terdekat dari anda.',
    'txt_desc_find_event_2' => 'Jika tidak terdaftar Anda dapat melihat negara tetangga anda.',
    'txt_subtitle_find_event' => 'Cari Acara / Event Bulan Alzheimer Dunia disekitar anda',

    'txt_region' => 'Pilih Wilayah',
    'txt_country' => 'Pilih Negara',
    'txt_city' => 'Pilih Kota',
    'txt_btn_add_event' => 'Mengunggah event',
    'txt__title_support_us' => 'Dukung kami',
    'txt_desc_support_us' => 'ADI sangat bergantung pada donasi Anda demi menjalankan tugasnya <br/>dalam memperkuat asosiasi Alzheimers di seluruh dunia <br/>dan meningkatkan kesadaran masyarakat tentang dampak global Demensia.',
    'txt_desc_support_us_2' => 'Dengan berdonasi Anda berkontribusi dalam mewujudkan dukungan nyata yang mempengaruhi<br/>orang-orang yang hidup dengan penyakit Alzheimer dan Demensia di seluruh dunia.',
    'txt_desc_support_us_3' => 'untuk berdonasi',

    'txt_add_event' => 'Tambah Event',
    'txt_event_name'=> 'Nama Event',
    'txt_email' => 'E-mail',
    'txt_event_desc' => 'Deskripsi Event',
    'txt_date' => 'Tanggal',
    'txt_time' => 'Waktu',
    'txt_country' => 'Negara',
    'txt_city' => 'kota',
    'txt_location' => 'Lokasi',
    'txt_organization' => 'Organisasi',
    'txt_web_link' => 'Link Web',

    /*-------------------- about dementia -------------------------------*/
    'txt_title_faq' => 'Pertanyaan yang sering ditanyakan',
    'txt_dementia_q1' => 'Apa itu Demensia?',
    'txt_dementia_a1' => 'Demensia adalah suatu sindrom degeneratif progresif pada otak yang mempengaruhi penurunan fungsi pada daya ingat, berpikir, perilaku dan emosi. Penyakit Alzheimer adalah tipe yang paling umum dari Demensia, yang mempengaruhi hingga 90% kemampuan otak manusia. Sebagian besar adalah akibat dari perubahan yang terjadi dalam otak dan hilangnya utama sel-sel saraf (neuron). Tipe lain dari Demensia termasuk Demensia Vaskular, Demensia Lewy Bodies dan Demensia Fronto-temporal (termasuk Pick&#39;s disease).',
    'txt_dementia_a1a' =>'Demensia adalah suatu sindrom degeneratif progresif pada otak yang mempengaruhi penurunan fungsi pada daya ingat, berpikir, perilaku dan emosi. Penyakit Alzheimer adalah tipe yang paling umum dari Demensia, yang mempengaruhi hingga 90% kemampuan otak manusia. ',
    'txt_dementia_a1b' =>'Sebagian besar adalah akibat dari perubahan yang terjadi dalam otak dan hilangnya utama sel-sel saraf (neuron). Tipe lain dari Demensia termasuk Demensia Vaskular, Demensia Lewy Bodies dan Demensia Fronto-temporal (termasuk Pick&#39;s disease).',
    'txt_dementia_q2' => 'Apa saja gejala awal demensia?',
    'txt_dementia_a2' => 'Setiap orang berbeda dan Demensia bisa mempengaruhi orang dengan cara yang berbeda-beda. Tidak ada orang yang memiliki gejala Demensia yang sama persis dengan orang lain. Kepribadian tingkat kesehatan atau kondisi sosial seseorang merupakan faktor-faktor penting dalam menentukan dampak Demensia pada diri mereka. Terdapat gejala yang bervariasi antara penyakit Alzheimer dan jenis Demensia, tetapi diantara mereka terdapat banyak kesamaan. Gejala yang paling umum adalah hilangnya memori dan hilangnya kemampuan praktis, yang dapat menyebabkan kemunduran dalam pekerjaan atau kehidupan sosial penderita. Jika menurut anda masalah ini mempengaruhi kehidupan sehari-hari anda atau seseorang yang Anda kenal, untuk segera periksakan dan hubungi dokter.',
    'txt_dementia_a2a' => 'Setiap orang berbeda dan Demensia bisa mempengaruhi orang dengan cara yang berbeda-beda. Tidak ada orang yang memiliki gejala Demensia yang sama persis dengan orang lain. Kepribadian tingkat kesehatan atau kondisi sosial seseorang merupakan faktor-faktor penting dalam menentukan dampak Demensia pada diri mereka. Terdapat gejala yang bervariasi antara penyakit Alzheimer dan jenis Demensia, tetapi diantara mereka terdapat banyak kesamaan. Gejala yang paling umum adalah hilangnya memori dan hilangnya kemampuan praktis, yang dapat menyebabkan kemunduran dalam pekerjaan atau kehidupan sosial penderita.',
    'txt_dementia_a2b' => 'Jika menurut anda masalah ini mempengaruhi kehidupan sehari-hari anda atau seseorang yang Anda kenal, untuk segera periksakan dan hubungi dokter.',
    'txt_dementia_q3' => 'Ibu saya memiliki penyakit Alzheimer. Apakah saya berpeluang untuk terkena juga?',
    'txt_dementia_a3' => 'Ada beberapa kasus yang sangat langka di mana penyakit Alzheimer diwariskan dalam keluarga. Dalam kasus ini ada hubungan langsung antara mutasi diwariskan dalam satu gen dan timbulnya penyakit. Ini cenderung dari kasus awal penyakit Alzheimer, yang mempengaruhi orang-orang di bawah usia 65. Dalam kasus ini, ada kemungkinan satu dari dua anggota keluarga dekat (saudara, saudara perempuan dan anak-anak) mempunyai peluang mewariskan penyakit Alzheimer. Sebagian besar kasus Alzheimer yang terjadi adalah yang bukan dari jenis yang disampaikan diatas. Jika salah satu anggota keluarga memiliki penyakit Alzheimer, risiko penyakit tersebut menyerang kerabat sekitar tiga kali lebih tinggi daripada risiko untuk orang dari usia yang sama tetapi tidak memiliki keluarga dengan riwayat Alzheimer. Diperkirakan bahwa dalam kasus ini gen seseorang dapat berkontribusi terhadap perkembangan penyakit, tetapi tidak menyebabkan secara langsung.',
    'txt_dementia_a3a' => 'Ada beberapa kasus yang sangat langka di mana penyakit Alzheimer diwariskan dalam keluarga. Dalam kasus ini ada hubungan langsung antara mutasi diwariskan dalam satu gen dan timbulnya penyakit. Ini cenderung dari kasus awal penyakit Alzheimer, yang mempengaruhi orang-orang di bawah usia 65. Dalam kasus ini, ada kemungkinan satu dari dua anggota keluarga dekat (saudara, saudara perempuan dan anak-anak) mempunyai peluang mewariskan penyakit Alzheimer.',
    'txt_dementia_a3b' => 'Sebagian besar kasus Alzheimer yang terjadi adalah yang bukan dari jenis yang disampaikan diatas. Jika salah satu anggota keluarga memiliki penyakit Alzheimer, risiko penyakit tersebut menyerang kerabat sekitar tiga kali lebih tinggi daripada risiko untuk orang dari usia yang sama tetapi tidak memiliki keluarga dengan riwayat Alzheimer. Diperkirakan bahwa dalam kasus ini gen seseorang dapat berkontribusi terhadap perkembangan penyakit, tetapi tidak menyebabkan secara langsung.',
    'txt_dementia_q4' => 'Apakah ada obatnya?',
    'txt_dementia_a4' => 'Sampai saat ini belum ada obat untuk penyakit Alzheimer, begitu juga dengan sebagian besar Demensia juga belum bisa disembuhkan dalam waktu dekat. Para peneliti masih pada tahap pengembangan obat-obatan yang akan memperlambat perkembangan penyakit, setidaknya dalam beberapa kasus. Mereka masih belum tahu bagaimana cara untuk mencegah penyakit tersebut sampai terjadi, bagaimana menghentikan perkembangannya, atau bagaimana untuk membalikkan efeknya. Diharapkan dengan lebih banyaknya penelitian tentang Demensia akan mempercepat ditemukannya obatnya.',
    'txt_dementia_a4a' => 'Sampai saat ini belum ada obat untuk penyakit Alzheimer, begitu juga dengan sebagian besar Demensia juga belum bisa disembuhkan dalam waktu dekat. Para peneliti masih pada tahap pengembangan obat-obatan yang akan memperlambat perkembangan penyakit, setidaknya dalam beberapa kasus.',
    'txt_dementia_a4b' => 'Mereka masih belum tahu bagaimana cara untuk mencegah penyakit tersebut sampai terjadi, bagaimana menghentikan perkembangannya, atau bagaimana untuk membalikkan efeknya. Diharapkan dengan lebih banyaknya penelitian tentang Demensia akan mempercepat ditemukannya obatnya.',
    'txt_dementia_q5' => 'Apakah ada perawatan khusus terhadap para pasien Alzheimer?',
    'txt_dementia_a5' => 'Meskipun tidak ada obat yang dapat menyembuhkan penyakit Alzheimer, ada sejumlah perawatan obat yang dapat membantu beberapa orang dengan penyakit Alzheimer. Perawatan yang tersedia saat ini dapat memperlambat perkembangan penyakit dalam beberapa kasus, antara 6 sampai dan 18 bulan. Kelas utama dalam proses perawatan tersebut dinamakan cholinesterase inhibitor. Perawatan/pengobatan ini juga terkadang berguna untuk mengendalikan beberapa gejala penyakit Alzheimer, seperti sulit tidur dan agitasi. Penggunaan obat-obatan seperti pil atau obat tidur tidak boleh diberikan secara berlebihan atau harus jauh dari jangkauan orang yang memiliki penyakit Alzheimer, karena obat-obat tersebut dapat menyebabkan peningkatan kebingungan dalam diri mereka. Perawatan non-obat, termasuk dukungan praktis dan emosional sangat penting dan efektif dalam membantu orang dengan Demensia dan perawat mereka.',
    'txt_dementia_a5a' => 'Meskipun tidak ada obat yang dapat menyembuhkan penyakit Alzheimer, ada sejumlah perawatan obat yang dapat membantu beberapa orang dengan penyakit Alzheimer. Perawatan yang tersedia saat ini dapat memperlambat perkembangan penyakit dalam beberapa kasus, antara 6 sampai dan 18 bulan. Kelas utama dalam proses perawatan tersebut dinamakan cholinesterase inhibitor. Perawatan/pengobatan ini juga terkadang berguna untuk mengendalikan beberapa gejala penyakit Alzheimer, seperti sulit tidur dan agitasi.',
    'txt_dementia_a5b' => 'Penggunaan obat-obatan seperti pil atau obat tidur tidak boleh diberikan secara berlebihan atau harus jauh dari jangkauan orang yang memiliki penyakit Alzheimer, karena obat-obat tersebut dapat menyebabkan peningkatan kebingungan dalam diri mereka. Perawatan non-obat, termasuk dukungan praktis dan emosional sangat penting dan efektif dalam membantu orang dengan Demensia dan perawat mereka.',
    'txt_dementia_q6' => 'Dapatkah penyakit Alzheimer dan Demensia lainnya dicegah?',
    'txt_dementia_a6' => ' Tidak banyak informasi tentang penyebab penyakit Alzheimer sehingga dibutuhkan pencegahan spesifik yang dapat direkomendasikan. Meskipun penyakit Alzheimer dan penyakit Demensia lainnya sering dialami sejalan dengan bertambahnya usia, namun pemicu perubahan karakteristik yang terjadi pada jaringan otak penderita penyakit Alzheimer belum diketahui sampai sekarang. Pada kebanyakan kasus, gen memainkan peran penting dalam pengembangan penyakit Alzheimer. Ada juga dibeberapa kasus, gen yang abnormal sangat berkontribusi dalam penyebaran penyakit. Tapi secara umum, menurut para peneliti, gen diyakini hanya berkontribusi dalam faktor kerentanan seseorang terhadap penyakit. Setidaknya dalam beberapa kasus, faktor lingkungan juga ikut mempengaruhi dalam memicu penyakit. Meskipun belum ada tindakan pencegahan secara khusus, tetapi menurut banyak penelitian, yang dapat Anda lakukan dalam mengurangi risiko terjangkit Alzheimer adalah dengan cara menerapkan gaya hidup sehat, memakan makanan yang sehat dan menjaga kesehatan fisik dan mental, serta aktif dalam kehidupan sosial Anda.',
    'txt_dementia_a6a' => 'Tidak banyak informasi tentang penyebab penyakit Alzheimer sehingga dibutuhkan pencegahan spesifik yang dapat direkomendasikan. Meskipun penyakit Alzheimer dan penyakit Demensia lainnya sering dialami sejalan dengan bertambahnya usia, namun pemicu perubahan karakteristik yang terjadi pada jaringan otak penderita penyakit Alzheimer belum diketahui sampai sekarang. Pada kebanyakan kasus, gen memainkan peran penting dalam pengembangan penyakit Alzheimer.',
    'txt_dementia_a6b' => 'Ada juga dibeberapa kasus, gen yang abnormal sangat berkontribusi dalam penyebaran penyakit. Tapi secara umum, menurut para peneliti, Gen diyakini hanya berkontribusi dalam faktor kerentanan seseorang terhadap penyakit. Setidaknya dalam beberapa kasus, faktor lingkungan juga ikut mempengaruhi dalam memicu penyakit. Meskipun belum ada tindakan pencegahan secara khusus, tetapi menurut banyak penelitian, yang dapat Anda lakukan dalam mengurangi risiko terjangkit Alzheimer adalah dengan cara menerapkan gaya hidup sehat, memakan makanan yang sehat dan menjaga kesehatan fisik dan mental, serta aktif dalam kehidupan sosial Anda.',

    'txt_title_signs' => 'Kenali 10 Gejala Demensia',
    'txt_subtitle_sign1' => '1. Gangguan daya ingat yang mengganggu kehidupan sehari-hari',
    'txt_subtitle_sign2' => '2. Kesulitan dalam merencanakan sesuatu atau memecahkan masalah/Sulit Fokus',
    'txt_subtitle_sign3' => '3. Sulit melakukan kegiatan familiar sehari-hari di rumah , di tempat kerja atau di waktu luang',
    'txt_subtitle_sign4' => '4. Kebingungan dengan waktu atau tempat (Disorientasi)',
    'txt_subtitle_sign5' => '5. Kesulitan memahami gambar dan tata ruang',
    'txt_subtitle_sign6' => '6. Gangguan berkomunikasi (Bermasalah dalam berbicara atau menulis',
    'txt_subtitle_sign7' => '7. Menaruh barang tidak pada tempatnya dan kehilangan kemampuan menelusuri kembali langkah sebelumnya ',
    'txt_subtitle_sign8' => '8. Salah membuat keputusan',
    'txt_subtitle_sign9' => '9. Menarik diri dari pekerjaan atau pergaulan sosial',
    'txt_subtitle_sign10' => '10. Perubahan suasana hati, perilaku dan kepribadian',
    'txt_desc_sign1' => 'Salah satu tanda yang paling umum pada pengidap Alzheimers adalah gangguan daya ingat, terutama melupakan informasi yang baru saja dipahami. Gejala lainnya termasuk melupakan tanggal dari peristiwa penting dalam hidupnya, selalu meminta informasi yang sama berulang-ulang, semakin bergantung pada alat bantu pengingat (misalnya, catatan pengingat atau perangkat elektronik) atau anggota keluarga untuk hal-hal yang sebenarnya bisa mereka tangani sendiri.',
    'txt_desc_sign2' => 'Beberapa orang mungkin akan mengalami perubahan kemampuan untuk mengembangkan dan mengikuti rencana atau saat bekerja berhubungan dengan angka. Mereka juga mungkin sering memiliki masalah dengan mengaplikasikan resep masakan atau tagihan bulanan. Selain itu mereka mungkin memiliki kesulitan berkonsentrasi dan memakan waktu lebih lama dalam mengerjakan sesuatu daripada biasanya.',
    'txt_desc_sign3' => 'Orang yang hidup dengan Alzheimer sering merasa sulit untuk menyelesaikan tugas-tugas sehari-hari. Terkadang mereka mengalami kesulitan mengemudi ke lokasi yang sering mereka lalui, mengelola anggaran di tempat kerja atau mengingat aturan permainan/game favorit mereka',
    'txt_desc_sign4' => 'Orang dengan Alzheimer dapat kehilangan daya ingat tentang tanggal, musim dan berlalunya waktu. Mereka mungkin memiliki kesulitan memahami sesuatu jika tidak terjadi dengan segera. Kadang-kadang mereka mungkin lupa di mana mereka berada atau bagaimana mereka sampai di sana (disorientasi).',
    'txt_desc_sign5' => 'Memiliki masalah penglihatan adalah salah satu gejala Alzheimer. Orang tersebut mungkin mengalami kesulitan membaca, menilai jarak dan menentukan warna atau kontras, yang dapat menyebabkan masalah dalam mengemudi.',
    'txt_desc_sign6' => 'Orang dengan Alzheimer kesulitan mengikuti atau bergabung dalam sebuah percakapan. Mereka mungkin berhenti di tengah-tengah percakapan dan tidak tahu bagaimana untuk melanjutkannya atau mengulang percakapan dengan sendirinya. Mereka kesulitan dengan kosa kata, memiliki masalah menemukan kata yang tepat atau menyebutkan benda/sesuatu dengan nama yang salah (misalnya, memanggil &quot;Jam tangan&quot; dengan &quot;Jam dinding&quot;)',
    'txt_desc_sign7' => 'Seseorang dengan penyakit Alzheimer dapat meletakkan segala sesuatu di tempat-tempat yang tidak biasa. Mereka mungkin kehilangan sesuatu dan tidak dapat menelusuri kembali langkah-langkah mereka sebelumnya untuk menemukan benda tersebut. Kadang-kadang, mereka menuduh orang lain mencuri. Hal ini dapat terjadi lebih sering dari waktu ke waktu.',
    'txt_desc_sign8' => 'Orang dengan Alzheimer mungkin mengalami perubahan penilaian atau keputusan-keputusan. Misalnya, mereka dapat menggunakan penilaian buruk ketika berhadapan dengan uang, memberikan jumlah besar untuk telemarketer, tetapi kurang memperhatikan perawatan atau menjaga diri mereka sendiri.',
    'txt_desc_sign9' => 'Seseorang dengan penyakit Alzheimer akan mulai untuk menarik diri dari hobi, kegiatan sosial, pekerjaan atau olahraga yang mereka sukai. Mereka mungkin mengalami kesulitan mengikuti tim olahraga favorit mereka atau mengingat bagaimana menjalani hobi favoritnya. Mereka juga akan mengisolasi diri karena perubahan yang mereka alami.',
    'txt_desc_sign10' => 'Perilaku dan kepribadian orang dengan Alzheimer dapat berubah. Mereka bisa menjadi bingung, curiga, depresi, takut atau cemas. Mereka mungkin mudah marah di rumah, di tempat kerja, dengan teman-teman atau di tempat-tempat di mana mereka berada di luar zona nyaman mereka.',
    'txt_source' => 'Sumber',

    'txt_title_find_help' => 'Menemukan bantuan',
    'txt_desc_finding_help' => 'Jika Anda atau orang terdekat anda hidup dengan Demensia, Anda tidak sendirian. Banyak orang dan organisasi, baik profesional dan sukarela, yang siap membantu Anda. Organisasi Alzheimer yang ada di dunia memberikan bantuan dan dukungan kepada orang-orang dengan Demensia dan keluarganya. Segera hubungi asosiasi/yayasan Alzheimer di negara Anda, atau jika tidak ada silahkan hubungi asosiasi Alzheimer Negara lain yang terdekat dari Anda. Banyak asosiasi memiliki helplines telepon, serta cabang di seluruh negeri, dan Komunitas lokal yang mengerti tentang Alzheimer di wilayah Anda. Asosiasi Alzheimer juga akan dapat menghubungkan Anda dengan keluarga penderita Alzheimer lainnya yang memahami kekhawatiran dan masalah Anda serta dapat membantu Anda. Organisasi-organisasi ini menawarkan persahabatan, dukungan dan saran, serta berkomitmen untuk membantu kelangsungan hidup bagi banyak orang.',
    'txt_desc_finding_help1' => 'Menemukan bantuan',
    'txt_desc_finding_help2' => 'Jika Anda atau orang terdekat anda hidup dengan Demensia, Anda tidak sendirian. Banyak orang dan organisasi, baik profesional dan sukarela, yang siap membantu Anda.',
    'txt_desc_finding_help3' => ' Organisasi Alzheimer yang ada di dunia memberikan bantuan dan dukungan kepada orang-orang dengan Demensia dan keluarganya.',
    'txt_desc_finding_help4' => 'Organisasi-organisasi ini menawarkan persahabatan, dukungan dan saran, serta berkomitmen untuk membantu kelangsungan hidup bagi banyak orang.',
    'txt_desc_finding_help5' => 'Menemukan bantuan',

    /*---------------------------privacy policy -----------------------------------------*/
    'txt_title_privacy' => 'Privacy Policy',
    'txt_desc_privacy1' => 'Alzheimer Disease International (ADI) hanya akan menggunakan informasi pribadi yang Anda kirimkan pribadi Anda, untuk keperluan administrasi, penggalangan dana, dan pengembangan situs web, produk dan jasa. Semua karyawan dan agen yang memiliki akses ke data pribadi Anda dan berkaitan dengan penanganan data, wajib menghormati kerahasiaan data pribadi Anda. ADI akan menjaga informasi yang kami miliki tentang Anda secara akurat dan up to date. Namun, jika Anda menemukan kesalahan atau ketidakakuratan dalam data Anda, kami akan segera menghapus, atau mengubah informasi atas permintaan Anda. ',
    'txt_desc_privacy2' => 'IP Address, jenis browser, dan informasi rujukan adakn disediakan oleh browser Anda ketika Anda menggunakan masuk situs ini. Informasi ini akan digunakan oleh ADI mengumpulkan statistik penggunaan, untuk membantu kami dalam mengembangkan situs ini dan mendiagnosa masalah teknis.',
    'txt_desc_privacy3' => 'Jika Anda mengikuti aktifitas "Do You Remember Me" dan masuk ke akun Facebook Anda, ini berarti Anda memberikan izin situs ini untuk mengakses foto dan informasi profil Facebook Anda. Informasi ini hanya akan digunakan untuk merasakan pengalaman masuk kedalam di situs ini dan tidak akan kami pertahankan. Informasi dasar tentang penggunaan website ini juga akan ditransmisikan dan disimpan oleh Facebook.',
    'txt_desc_privacy3_link' => 'Kebijakan Facebook',
    'txt_desc_privacy3a' => 'juga berlaku untuk data yang dikumpulkan.',
    'txt_desc_privacy4' => 'Website ini menggunakan Google Analytics, layanan analisis web yang disediakan oleh Google, Inc. Google Analytics menggunakan "cookies". “Cookies” sendiri adalah file teks yang ditempatkan pada komputer Anda, yang berguna untuk membantu website menganalisis bagaimana pengguna menggunakan situs ini. Informasi yang dihasilkan oleh cookie tentang penggunaan situs (termasuk alamat IP Anda) akan ditransmisikan dan disimpan oleh Google pada server di Amerika Serikat. Google akan menggunakan informasi ini untuk tujuan mengevaluasi penggunaan situs web, menyusun laporan  kegiatan website untuk operator website dan memberikan layanan lain yang berkaitan dengan kegiatan. Google juga dapat mentransfer informasi ini kepada pihak ketiga untuk keperluan hukum, atau pihak ketiga tersebut memproses informasi atas nama Google. Google tidak akan mengasosiasikan alamat IP anda dengan data lain yang dimiliki oleh Google. Anda dapat menolak penggunaan cookies dengan memilih pengaturan yang sesuai pada browser Anda. Dengan menggunakan situs ini, Anda setuju untuk  pengolahan data pribadi Anda oleh Google dengan cara dan untuk tujuan ditetapkan di atas. Situs pencarian juga ditangani oleh Google. ',
    'txt_desc_privacy4_link' => 'Kebijakan privasi Google',
    'txt_desc_privacy4a' => 'berlaku untuk data dikumpulkan.',
    'txt_desc_privacy5' => 'Jika Anda mempunyai kekhawatiran apapun tentang penggunaan data pribadi Anda, silahkan hubungi kami.',
    'txt_contact_us' => 'kontak kami',

    /*-----------------------------new experience----------------------------------------*/
    'txt_experience_new_1_content_1' => 'Setiap orang adalah unik dan demensia mempengaruhi mereka dengan cara yang berbeda-beda, tetapi terdapat kesamaan diatara mereka.',
    'txt_experience_new_1_content_2' => 'Tanda-tanda yang paling umum adalah kehilangan memori dan hilangnya kemampuan praktis, yang dapat menyebabkan penarikan dari pekerjaan atau kegiatan sosial.',
    'txt_experience_new_1_content_3' => 'Game ini memberikan gambaran tentang apa yang mungkin terjadi saat hidup dengan gejala demensia. Seperti kesulitan untuk mengenali teman-teman dan keluarga atau untuk mengingat kenangan. Silahkan Login dengan Facebook untuk memulai.',

    'txt_experience_new_1_alt_content_1' => 'Setiap orang adalah unik dan demensia mempengaruhi mereka dengan cara yang berbeda-beda, tetapi terdapat kesamaan diatara mereka. Tanda-tanda yang paling umum adalah kehilangan memori dan hilangnya kemampuan praktis, yang dapat menyebabkan penarikan dari pekerjaan atau kegiatan sosial.',
    'txt_experience_new_1_alt_content_2' => 'Game ini memberikan gambaran tentang apa yang mungkin terjadi saat hidup dengan gejala demensia.<br/><br/>
                                             Seperti kesulitan untuk mengenali teman-teman dan keluarga atau untuk mengingat kenangan.  ',

    'txt_experience_new_2_content_1' => 'Common symptoms in the early stages of dementia include <b>forgetfulness</b>,<br/>
            <b>communication difficulties</b> and <b>changes in mood and behaviour.</b> ',
    'txt_experience_new_2_content_2' => 'The initial stages of dementia are often difficult to recognise,<br/>
            as symptoms like <b>memory loss</b> and <b>confusion</b> can be temporary and transient.',
    'txt_experience_new_2_content_3' => 'Once diagnosed, people living with dementia can be better supported to help plan their future care,<br/>
            but our research shows that as many as <b>3 out of 4 people</b> living with dementia worldwide<br/>
            have never received a formal diagnosis.',

    'txt_experience_new_3_content_1' => 'Do you remember when this picture<br/>was taken?',
    'txt_experience_new_3_content_2' => 'YES',
    'txt_experience_new_3_content_3' => 'NO',

    'txt_experience_new_5_content_1' => 'A person living in the middle stages of dementia will be <b>aware of their condition</b>, but will need support<br/>
            to help them manage their day-to-day living as <b>their memory continues to deteriorate.</b>',
    'txt_experience_new_5_content_2' => 'They are likely to become increasingly forgetful and may also <b>fail to recognise people</b> or<br/>
            confuse them with others. The middle stages of dementia are typically the longest and can <b>last for many years.</b>',
    'txt_experience_new_5_content_3' => 'These changes can be difficult for families and loved ones, but it’s important to remember that there is support<br/>
            available through <a href="http://www.alz.co.uk/associations" target="_blank" class="text-blue">http://www.alz.co.uk/associations</a>',

    'txt_experience_new_8_content_1' => 'Selama tahap akhir demensia kebutuhan penderita akan berubah dan biasanya memerlukan waktu perawatan penuh.<br/> <b>Hilangnya progresif memori</b> dapat menjadi waktu yang menyedihkan bagi orang yang hidup dengan demensia<br/><b>yang mungkin gagal untuk mengenali anggota keluarga dekat.</b>',
    'txt_experience_new_8_content_2' => 'Selama proses tersebut orang akan <b>semakin kesulitan memahami apa yang terjadi</b> di sekitar mereka dan dapat semakin kehilangan kemampuan bicara mereka.',
    'txt_experience_new_8_content_3' => 'Sangat penting untuk diingat bahwa meskipun banyak kemampuan yang hilang atau berkurang, <b>beberapa<br/> - seperti indra dan kemampuan untuk menanggapi emosi  mungkin masih dapat bekerja dengan normal. </b>',

    'txt_experience_new_11_content_1' => 'Hilangnya ingatan adalah salah satu dari banyak cara demensia akan mempengaruhi seseorang. Selain itu orang tersebut juga akan mengalami menurunnya kemampuan penilaian mereka, perubahan suasana hati dan kepribadian dan hilangnya inisiatif. Jika Anda atau seseorang yang Anda kenal memiliki masalah semacam ini, Anda harus berbicara dengan dokter Anda, atau mendorong mereka untuk berbicara dengan mereka.',
    'txt_experience_new_11_content_2' => 'Pikirkan tentang betapa sulitnya mungkin untuk mengenali foto-foto ini dan Anda akan memiliki pengalaman sekilas tentang apa rasanya hidup dengan demensia, terutama pada tahap menengah dan kemudian ketika wajah-wajah dan kenangan akan memudar. Belajar tentang pengalaman yang berbeda dari demensia adalah cara yang bagus untuk membantu dan memberikan dukungan kepada orang-orang terkasih. Menyebarkan berita, sehingga dunia bisa melihat apa yang telah Anda lihat.',
    'txt_experience_new_11_content_3' => 'Untuk informasi lebih lanjut tentang tahap awal, tengah dan kemudian demensia, lihatlah <a href="#" class="text-blue">Alzheimer’s Society of Canada</a>, <a href="#" class="text-blue">Alzheimer’s Australia</a> dan <a href="#" class="text-blue">Alzheimer’s Association</a>.',




);
